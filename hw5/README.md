## HW5

To run this program, simply open HW5.html in the Firefox browser. This is only tested to be working in Firefox.

The code is adapted from the ex08 code, mostly because I have never even touched javascript in my life.

The lighting is mostly working, but gets a little wonky based on the view. I did not have time to fix this.

### Time
This took me approximately 4 hours to complete, mostly because I have zero experience with Javascript, and only some experience with HTML.