precision highp float;

attribute vec3 PositionIn;
attribute vec3 NormalIn;
attribute vec2 TexCoordIn;

uniform mat4 ProjectionMatrix;
uniform mat4 ModelviewMatrix;
uniform mat4 NormalMatrix;
uniform vec3 LightPosition;

varying vec3 NormalOut;
varying vec2 TexCoordOut;
varying vec3 View;
varying vec3 Light;

void main(void)
{
	NormalOut = vec3(vec4(NormalIn, 1.0) * NormalMatrix);
	TexCoordOut = TexCoordIn;
	
	vec3 P = mat3(ModelviewMatrix) * PositionIn;
	View = -P;
	Light = LightPosition - P;
	gl_Position = ProjectionMatrix * vec4(P, 1.0);
}