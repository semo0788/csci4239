precision highp float;

varying vec3 NormalOut;
varying vec2 TexCoordOut;
varying vec3 View;
varying vec3 Light;

uniform sampler2D Texture;

vec4 phong()
{
   //  N is the object normal
   vec3 N = normalize(NormalOut);
   //  L is the light vector
   vec3 L = normalize(Light);

   //  Emission and ambient color
   vec4 color = vec4(0.1, 0.1, 0.1, 1.0);

   //  Diffuse light is cosine of light and normal vectors
   float Id = dot(L,N);
   if (Id>0.0)
   {
      //  Add diffuse
      color += Id;
      //  R is the reflected light vector R = 2(L.N)N - L
      vec3 R = reflect(-L,N);
      //  V is the view vector (eye vector)
      vec3 V = normalize(View);
      //  Specular is cosine of reflected and view vectors
      float Is = dot(R,V);
	  if (Is>0.0) color += pow(Is, 6.0);
   }

   //  Return sum of color components
   return color;
}

void main(void)
{
   gl_FragColor = phong() * texture2D(Texture, TexCoordOut);
}