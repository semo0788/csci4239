function loadTextFile(url, callback) {
  var request = new XMLHttpRequest();
  request.open('POST', url, false);
  request.addEventListener('load', function() {
     callback(request.responseText);
  });
  request.send();
}

//
//  Compile a shader (edited for HW 5)
//
function CompileShader(gl,path,type)
{
   //  Create shader based on type setting
   var shader = gl.createShader(type);

   // Load in shader source
   loadTextFile(path, function(text){
	  	//  Supply shader source
		gl.shaderSource(shader, text);
		//  Compile the shader
		gl.compileShader(shader);
   });

   //  Check for errors
	if (gl.getShaderParameter(shader, gl.COMPILE_STATUS) == 0)
		alert("Compile Error: " + gl.getShaderInfoLog(shader)); 
   //  Return shader
   return shader;
}

//
//  Compile shader program (edited for HW 5)
//
function CompileShaderProg(gl,vert,frag)
{
   //  Compile the program
   var prog  = gl.createProgram();
   gl.attachShader(prog , CompileShader(gl, vert, gl.VERTEX_SHADER));
   gl.attachShader(prog , CompileShader(gl, frag, gl.FRAGMENT_SHADER));
   gl.linkProgram(prog);
   //  Check for errors
   if (gl.getProgramParameter(prog, gl.LINK_STATUS) == 0)
      alert("Linker Error: " + gl.getProgramInfoLog(prog));
   //  Return program
   return prog;
}

var gl,canvas;
function webGLStart()
{
   //  Set canvas
   canvas = document.getElementById("canvas");
   //  Select canvas size
   var size = Math.min(window.innerWidth,window.innerHeight)-10;
   canvas.width  = size;
   canvas.height = size;
   //  Start WebGL
   if (!window.WebGLRenderingContext)
   {
      alert("Your browser does not support WebGL. See http://get.webgl.org");
      return;
   }
   try
   {
      gl = canvas.getContext("experimental-webgl");
   }
   catch(e)
   {}
   if (!gl)
   {
      alert("Can't get WebGL");
      return;
   }

   //  Set viewport to entire canvas
   gl.viewport(0,0,size,size);

   //  Load Shader
   var prog = CompileShaderProg(gl,"src/hw5.vert","src/hw5.frag");

   //  Set program
   gl.useProgram(prog);

   //  Set projection
   var ProjectionMatrix = new CanvasMatrix4();
   ProjectionMatrix.ortho(-2.5,+2.5,-2.5,+2.5,-2.5,+2.5);

	// New shiny stuff
	var cruiserObj;
	OBJ.downloadMeshes({"cruiser": "res/cruiser.obj"}, function(meshes) { 
		cruiserObj = meshes.cruiser; 
	});

	// Load buffers (this happens behind the scenes, but only creates and populates VBOs, nothing more)
	OBJ.initMeshBuffers(gl, cruiserObj);

	// Locate the shader attributes and uniforms
	prog.posLoc = gl.getAttribLocation(prog, "PositionIn");
	prog.normalLoc = gl.getAttribLocation(prog, "NormalIn");
	prog.texLoc = gl.getAttribLocation(prog, "TexCoordIn");
	prog.projLoc = gl.getUniformLocation(prog, "ProjectionMatrix");
	prog.mviewLoc = gl.getUniformLocation(prog, "ModelviewMatrix");
	prog.normLoc = gl.getUniformLocation(prog, "NormalMatrix");
	prog.textureLoc = gl.getUniformLocation(prog, "Texture");
	prog.lightLoc = gl.getUniformLocation(prog, "LightPosition");

	// Create a webgl texture
	var cruiserTex = gl.createTexture();
	gl.bindTexture(gl.TEXTURE_2D, cruiserTex);

	// Fill the texture with temporary white texture until the actual image can be loaded
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE, 
		new Uint8Array([255, 255, 255, 255]));
	//gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
	//gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
	//gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

	// Asynchronously load an image
	var cruiserImage = new Image();
	cruiserImage.src = "res/crate.png";
	cruiserImage.addEventListener('load', function() {
		gl.bindTexture(gl.TEXTURE_2D, cruiserTex);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, cruiserImage);
		gl.generateMipmap(gl.TEXTURE_2D);
	});


   //  Set state to draw scene
   gl.enable(gl.DEPTH_TEST);
   gl.clearColor(0,0,0,1);
   //  Mouse control variables
   var x0 = y0 = move  = 0;
   //  Rotation angles
   var th = ph = 15;

   //
   //  Display the scene
   //
   var lightTime = 0;
   function Display()
   {
      //  Clear the screen and Z buffer
      gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

      // Compute modelview matrix
      var ModelviewMatrix = new CanvasMatrix4();
      ModelviewMatrix.makeIdentity();
      ModelviewMatrix.rotate(ph,0,1,0);
      ModelviewMatrix.rotate(th,1,0,0);

	  // Compute normal matrix
	  var NormalMatrix = new CanvasMatrix4();
	  NormalMatrix.makeIdentity();
	  NormalMatrix.rotate(ph, 0, 1, 0);
	  NormalMatrix.rotate(th, 1, 0, 0);
	  NormalMatrix.invert();
	  NormalMatrix.transpose();

	  // Precalculate matrix arrays
	  var projMat = new Float32Array(ProjectionMatrix.getAsArray());
	  var mviewMat = new Float32Array(ModelviewMatrix.getAsArray());
	  var normMat = new Float32Array(NormalMatrix.getAsArray());

      // Set shader
      gl.useProgram(prog);

      //  Set projection and modelview matrixes
      gl.uniformMatrix4fv(prog.projLoc, false, projMat);
      gl.uniformMatrix4fv(prog.mviewLoc, false, mviewMat);
	  gl.uniformMatrix4fv(prog.normLoc, false, normMat);

      //  Set up 3D vertex array
      gl.bindBuffer(gl.ARRAY_BUFFER, cruiserObj.vertexBuffer);
      gl.enableVertexAttribArray(prog.posLoc);
      gl.vertexAttribPointer(prog.posLoc, cruiserObj.vertexBuffer.itemSize, gl.FLOAT, false, 0, 0);

      //  Set up normal array
      gl.bindBuffer(gl.ARRAY_BUFFER, cruiserObj.normalBuffer);
      gl.enableVertexAttribArray(prog.normalLoc);
      gl.vertexAttribPointer(prog.normalLoc, cruiserObj.normalBuffer.itemSize, gl.FLOAT, false, 0, 0);

	  // Set up TexCoord array
	  gl.bindBuffer(gl.ARRAY_BUFFER, cruiserObj.textureBuffer);
	  gl.enableVertexAttribArray(prog.texLoc);
	  gl.vertexAttribPointer(prog.texLoc, cruiserObj.textureBuffer.itemSize, gl.FLOAT, false, 0, 0);

	  // Setup the index buffer
	  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, cruiserObj.indexBuffer);

	  // Bind the texture
	  gl.activeTexture(gl.TEXTURE0);
	  gl.bindTexture(gl.TEXTURE_2D, cruiserTex);
	  gl.uniform1i(prog.textureLoc, 0);

	  // Set the lighting
	  lightTime += (33.33 / 1000);
	  var lightArr = new Float32Array([10.0 * Math.cos(lightTime * 2), 5.0, 10.0 * Math.sin(lightTime * 2)]);
	  gl.uniform3fv(prog.lightLoc, lightArr);

      //  Draw all vertexes
      gl.drawElements(gl.TRIANGLES, cruiserObj.indexBuffer.numItems, gl.UNSIGNED_SHORT, 0);

      //  Disable vertex arrays
      gl.disableVertexAttribArray(prog.texLoc);
      gl.disableVertexAttribArray(prog.normalLoc);
	  gl.disableVertexAttribArray(prog.posLoc);

      //  Flush
      gl.flush ();
   }

   //
   //  Resize canvas
   //
   canvas.resize = function ()
   {
      var size = Math.min(window.innerWidth, window.innerHeight)-10;
      canvas.width  = size;
      canvas.height = size;
      gl.viewport(0,0,size,size);
   }

   //
   //  Mouse button pressed
   //
   canvas.onmousedown = function (ev)
   {
      move  = 1;
      x0 = ev.clientX;
      y0 = ev.clientY;
   }

   //
   //  Mouse button released
   //
   canvas.onmouseup = function (ev)
   {
      move  = 0;
   }

   //
   //  Mouse movement
   //
   canvas.onmousemove = function (ev)
   {
      if (move==0) return;
      //  Update angles
      ph -= ev.clientX-x0;
      th += ev.clientY-y0;
      //  Store location
      x0 = ev.clientX;
      y0 = ev.clientY;
   }

   // Set the scene to render at 30fps
   setInterval(Display, 33.33);
}