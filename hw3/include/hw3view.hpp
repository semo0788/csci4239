/*
 *	HW3 Header File for Sean Moss.
 */

#ifndef hw3view_hpp_
#define hw3view_hpp_

#include <QWidget>
#include <QSlider>
#include <QDoubleSpinBox>
#include <QGroupBox>
#include "hw3opengl.hpp"

class HW3View : public QWidget
{
Q_OBJECT
private:
   QSlider*     Lpos;
   QSlider*     Zpos;
   QPushButton* light;
   HW3OpenGL*  ogl;
private slots:
   void lmove();        //  Light movement
private:
    QGroupBox* prepareSceneControls(QComboBox *object, QComboBox *projection, QLabel *angles, QLabel *frames);
    QGroupBox* prepareLightControls();
    QGroupBox* prepareShaderControls(QComboBox *shader);
public:
    HW3View();
};

#endif // hw3view_hpp_
