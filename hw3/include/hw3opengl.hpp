/*
 *	HW3 Header File for Sean Moss.
 */

#ifndef hw3opengl_hpp_
#define hw3opengl_hpp_

#include "CUgl.h"

class HW3OpenGL : public CUgl
{
Q_OBJECT
private:
   int frame; //  Frame counter
   int t0;    //  Frame seconds
public:
   HW3OpenGL(QWidget* parent=0);                   // Constructor
   QSize sizeHint() const {return QSize(400,400);}  // Default size of widget
protected:
    void initializeGL();  // Initialize widget
    void paintGL();       // Draw widget
signals:
    void fps(QString);    // Frames per second
};

#endif // hw3opengl_hpp_
