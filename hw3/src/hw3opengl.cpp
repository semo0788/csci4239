/*
 *	HW3 Source File for Sean Moss.
 */
#include "hw3opengl.hpp"
#include "Cube.h"
#include "WaveOBJ.h"
#include "Teapot.h"

//
//  Constructor
//
HW3OpenGL::HW3OpenGL(QWidget* parent)
    : CUgl(parent)
{
   t0 = frame = 0;
}

//
//  Initialize
//
void HW3OpenGL::initializeGL()
{
   //  Load shaders
   addShader("",":/saved.frag"); // Saved
   addShader(":/vertex_phong.vert",":/vertex_phong.frag"); // Vertex Phong
   addShader(":/pixel_phong.vert",":/pixel_phong.frag"); // Pixel Phong
   addShader(":/pixel_phong.vert", ":/light_if.frag"); // If (Light)
   addShader(":/pixel_phong.vert", ":/heavy_if.frag"); // If (Heavy)
   addShader(":/pixel_phong.vert", ":/float_arith.frag"); // Float Arith
   addShader(":/pixel_phong.vert", ":/int_arith.frag"); // Int Arith
   addShader(":/pixel_phong.vert", ":/trans_arith.frag"); // Trans Arith
   addShader(":/pixel_phong.vert", ":/function_call.frag"); // Function Calls
   addShader(":/pixel_phong.vert", ":/inline_call.frag"); // Inline Calls

   //  Load objects
   addObject(new Cube(":/crate.png"));
   addObject(new Teapot(8,":/pi.png",0.5));
   addObject(new WaveOBJ("cruiser.obj",":/"));
   setObject(0);

   //  Set max frame rate
   maxFPS(1);
}

//
//  Draw the window
//
void HW3OpenGL::paintGL()
{
   //  Clear screen and Z-buffer
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
   glEnable(GL_DEPTH_TEST);

   //  Set view
   doView();

   //  Apply lighting
   doLight();

   //  Apply shader
   if (mode) shader[mode]->bind();

   //  Draw scene
   doScene();

   //  Release shader
   if (mode) shader[mode]->release();
   glDisable(GL_LIGHTING);
   glDisable(GL_DEPTH_TEST);

   //  Frame counter
   frame++;
   int t = time.elapsed()/1000;
   if (t>t0)
   {
      emit fps(QString::number(frame));
      t0 = t;
      frame = 0;
   }
}
