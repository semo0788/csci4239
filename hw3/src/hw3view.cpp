/*
 *	HW3 Source File for Sean Moss.
 */

#include <QComboBox>
#include <QLabel>
#include <QGridLayout>
#include <QGroupBox>
#include "hw3view.hpp"

//
//  Constructor
//
HW3View::HW3View()
{
   //  Set window title
   setWindowTitle(tr("Assignment 3: Sean Moss"));

   //  Create new OpenGL widget
   ogl = new HW3OpenGL;

   //  Select shader
   QComboBox* shader = new QComboBox();
   shader->addItem("None");
   shader->addItem("Saved");
   shader->addItem("Vertex Phong");
   shader->addItem("Pixel Phong");
   shader->addItem("If (Light)");
   shader->addItem("If (Heavy)");
   shader->addItem("Float Arith");
   shader->addItem("Int Arith");
   shader->addItem("Trans Arith");
   shader->addItem("Function Calls");
   shader->addItem("Inline Calls");

   //  Select projection
   QComboBox* projection = new QComboBox();
   projection->addItem("Orthogonal");
   projection->addItem("Perspective");
   projection->setCurrentIndex(1);

   //  Select object
   QComboBox* object = new QComboBox();
   object->addItem("Cube");
   object->addItem("Teapot");
   object->addItem("Cruiser");

   //  Center position
   Lpos = new QSlider(Qt::Horizontal);
   Zpos = new QSlider(Qt::Horizontal);
   Lpos->setRange(0,360);
   Zpos->setRange(-100,100);

   //  View angle and zoom
   QLabel* angles = new QLabel();
   //  Frames per second
   QLabel* frames = new QLabel();

   //  Pause/resume button
   light = new QPushButton("Pause");

   //  Reset
   QPushButton* rst = new QPushButton("Reset");
   //  Quit
   QPushButton* quit = new QPushButton("Quit");

   //  Set layout of child widgets
   QGridLayout* layout = new QGridLayout;
   layout->addWidget(ogl,0,0,8,1);
   QGroupBox *sceneBox = prepareSceneControls(object, projection, angles, frames);
   layout->addWidget(sceneBox,0,1,1,2);
   QGroupBox *lightBox = prepareLightControls();
   layout->addWidget(lightBox,1,1,1,2);
   QGroupBox *shaderBox = prepareShaderControls(shader);
   layout->addWidget(shaderBox,2,1,1,2);
   layout->addWidget(rst,4,1);
   layout->addWidget(quit,4,2);
   //  Manage resizing
   layout->setColumnStretch(0,100);
   layout->setColumnMinimumWidth(0,100);
   layout->setRowStretch(2,100);
   setLayout(layout);

   //  Connect valueChanged() signals to ogl
   connect(shader,SIGNAL(currentIndexChanged(int))     , ogl,SLOT(setShader(int)));
   connect(object,SIGNAL(currentIndexChanged(int))     , ogl,SLOT(setObject(int)));
   connect(projection,SIGNAL(currentIndexChanged(int)) , ogl,SLOT(setPerspective(int)));
   connect(Lpos,SIGNAL(valueChanged(int)) , ogl,SLOT(setLightAngle(int)));
   connect(Zpos,SIGNAL(valueChanged(int)) , ogl,SLOT(setLightElevation(int)));
   //  Connect angles() and zoom() signal to labels
   connect(ogl,SIGNAL(angles(QString)) , angles,SLOT(setText(QString)));
   connect(ogl,SIGNAL(fps(QString))    , frames,SLOT(setText(QString)));
   connect(ogl,SIGNAL(light(int))      , Lpos,SLOT(setValue(int)));
   //  Connect reset() and lmove() signals
   connect(rst  ,SIGNAL(pressed()),ogl,SLOT(reset()));
   connect(light,SIGNAL(pressed()),this,SLOT(lmove()));
   //  Connect quit() signal to qApp::quit()
   connect(quit,SIGNAL(pressed()) , qApp,SLOT(quit()));

   //  Settings
   Zpos->setValue(100);
}

//
//  Light pause/move
//
void HW3View::lmove()
{
   bool pause = (light->text()=="Pause");
   if (pause)
      light->setText("Animate");
   else
      light->setText("Pause");
   ogl->setLightMove(!pause);
}

// ================================================================================================
QGroupBox* HW3View::prepareSceneControls(QComboBox *object, QComboBox *projection, QLabel *angles, QLabel *frames)
{
    QGroupBox *box = new QGroupBox(tr("Scene Controls"));

    QGridLayout *layout = new QGridLayout;
    layout->addWidget(new QLabel("Projection"),0,0);
    layout->addWidget(projection,0,1);
    layout->addWidget(new QLabel("Object"),1,0);
    layout->addWidget(object,1,1);
    layout->addWidget(new QLabel("Angles"),2,0);
    layout->addWidget(angles,2,1);  
    layout->addWidget(new QLabel("FPS"),3,0);
    layout->addWidget(frames,3,1);

    box->setLayout(layout);
    return box;
}

// ================================================================================================
QGroupBox* HW3View::prepareLightControls()
{
    QGroupBox *box = new QGroupBox(tr("Light Controls"));

    QGridLayout *layout = new QGridLayout;
    layout->addWidget(new QLabel("Light Position"),0,0);
    layout->addWidget(Lpos,0,1);
    layout->addWidget(new QLabel("Light Elevation"),1,0);
    layout->addWidget(Zpos,1,1);
    layout->addWidget(new QLabel("Light"),2,0);
    layout->addWidget(light,2,1);

    box->setLayout(layout);
    return box;
}

// ================================================================================================
QGroupBox* HW3View::prepareShaderControls(QComboBox *shader)
{
    QGroupBox *box = new QGroupBox(tr("Shader Controls"));

    QGridLayout *layout = new QGridLayout;
    layout->addWidget(new QLabel("Shader"),0,0);
    layout->addWidget(shader,0,1);
    layout->setRowStretch(1, 100);
    
    box->setLayout(layout);
    return box;
}