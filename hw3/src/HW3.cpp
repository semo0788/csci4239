/*
 *	HW3 Source File for Sean Moss.
 */

#include <QApplication>
#include "hw3view.hpp"

int main(int argc, char *argv[])
{
   QApplication app(argc,argv);

   HW3View view;
   view.show();

   return app.exec();
}
