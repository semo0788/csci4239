## HW3

To run this program, simply run `qmake` and then `make` in the directory. The resulting executable is called "hw3".

The interface is adapted from the ex04 code, as well as the code from previous homeworks.

### Shaders
These are the shaders that are included in this program. All custom shaders use per-pixel phong lighting.

1) None 			- Fixed pipeline
2) Saved 			- Original "Saved" shader written by Willem
3) Vertex Phong 	- Original "Vertex Phong" shader written by Willem
4) Pixel Phong 		- Original "Pixel Phong" shader written by Willem
5) If (Light) 		- Shader that uses 3 level simple nested if statements
6) If (Heavy) 		- Shader that uses up to 11 level simple nested if statements
7) Float Arith 		- Shader that performs many pointless floating point operations per pixel
8) Int Arith 		- Shader that performs as many integer operations per pixel as the Float Arith shader
9) Trans Arith 		- Shader that performs as many transcendental operations per pixel as the Float Arith shader
10) Function Calls 	- Shader that calls the phong() function 100 times per pixel
11) Inline Calls 	- Shader that inlines the phong() function 100 times per pixel


### Results
For this homework assignment, I created 4 basic tests to measure performance of various features. The four tests are:

1) If Construct - I provided a simple nested `if` shader, and a very complex nested `if` shader, and compared performance.
2) Float vs. Int Arithmetic - I provided two shaders that perform the save heavy calculations per pixel, one using ints and one using floats.
3) Transcendental Cost - I adapted the heavy float calculations shader to use many transcendental functions (`sin`, `cos`, `exp`, and `log`), effectively every time that a float was used.
4) Function Calls vs. Inlining - For this, I had each shader perform 100 phong calculations, one shader using a `phong()` function, and the other inlining the code.

Each test was captured on an Nvidia GTX 660 card, the the program fullscreen at 1920x1080 resolution. The "cruiser" object was used, looking from the top down and zoomed in to fill the entire viewport. For comparison, the simple per-vertex phong lighting shader averaged aroung **2000 FPS**. The results are:

1) The simple nested if shader averaged **1820 - 1840 FPS**. The complex nested shader averaged a much more unstable **1690 - 1800 FPS**.
2) The floating point arithmetic shader averaged **55 - 57 FPS**. The integer arithmetic shader averaged **59 - 61 FPS**.
3) The transcendental shader, which was being compared against the floating point arithmetic shader, averaged **54 - 57 FPS**.
4) The function call shader and inline call shaders both averaged a steady **64 FPS**.

A discussion of the results:

1) The shader with only a few if statements managed a very steady and very high FPS. The complex if shader not only ran under a noticeably reduced FPS, but also saw a much higher fluctuation in the FPS range while it was active. This does point towards some measureable reduction of performance caused by these control structures. While the inclusion of even a decent number of if statements seems to not be too much an issue for better cards, they should still be avoided when possible.
2) The difference between the floating point and integer arithmetic shaders was much less substantial than with the previous test. While the FPS ranges were different, they were still too close to confidently call a higher speed for either of the arithmetic types. I believe that this points to either an issue with my code, where agressive optimization may have made the shaders very similar, or to perhaps the lack of integer units on the GPU. There is a chance that certain graphics systems may opt to have only floating point units. However, since graphics cannot be done properly without widespread use of floating point numbers, this discussion seems to be a moot point anyway, at least substantially less important than with the CPU.
3) The use of the transcendental functions does not seem to have noticeably slowed down the processing at all, except for maybe the costs of repeated function calls. While this was initially surprising, I realized that, in many systems, these transcendental functions are either very basic approximations, interpolated lookup tables of precomputed values, or even have direct firmware/hardware support. This would change the cost of calculating these values from multiple floating point operations to a few simple comparisons and substantially fewer operations. Because these sorts of functions are so important to graphics, it makes sense that the backend of the functions could be some of the most highly optimized parts of the GPU.
4) There was no measureable FPS difference between function calls and inlined code. This makes sense, since the GLSL compiler implements function calls by inlining the code to begin with. This was basically just us taking a single optimization step away from the compiler.

All of these results point towards the incredible optimizability of GLSL, and the power of SIMD-style computing systems.


### Time
This assignment took me about 7 hours to complete, including adapting the example code, writing the shaders, and compiling results.

*Disclaimer: this homework made heavy use of the helper classes provided by Willem in the "ex01" code set. That code belongs to him.*