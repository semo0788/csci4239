HEADERS = include/Cube.h \
          include/CUgl.h \
          include/hw3opengl.hpp \
          include/hw3view.hpp \
          include/Object.h \
          include/Sphere.h \
          include/Teapot.h \
          include/Type.h \
          include/WaveOBJ.h
SOURCES = src/Cube.cpp \
          src/CUgl.cpp \
          src/HW3.cpp \
          src/hw3opengl.cpp \
          src/hw3view.cpp \
          src/Object.cpp \
          src/Sphere.cpp \
          src/Teapot.cpp \
          src/WaveOBJ.cpp
QT += opengl
RESOURCES = res/hw3.qrc
INCLUDEPATH += $$PWD/include
QMAKE_CXXFLAGS += -std=c++11
