//  Per-pixel Phong lighting
//  Fragment shader

varying vec3 View;
varying vec3 Light;
varying vec3 Normal;
varying vec4 Ambient;
uniform sampler2D tex;

uniform int loopCounter = 500;
uniform int if1 = 1;
uniform int if2 = 2;
uniform int if3 = 4;
uniform int if4 = 8;
uniform int if5 = 16;

vec4 phong()
{
	//  N is the object normal
	vec3 N = normalize(Normal);
	//  L is the light vector
	vec3 L = normalize(Light);

	//  Emission and ambient color
	vec4 color = Ambient;

	//  Diffuse light is cosine of light and normal vectors
	float Id = dot(L,N);
	if (Id>0.0)
	{
		//  Add diffuse
		color += Id*gl_FrontLightProduct[0].diffuse;
		//  R is the reflected light vector R = 2(L.N)N - L
		vec3 R = reflect(-L,N);
		//  V is the view vector (eye vector)
		vec3 V = normalize(View);
		//  Specular is cosine of reflected and view vectors
		float Is = dot(R,V);
		if (Is>0.0) color += pow(Is,gl_FrontMaterial.shininess)*gl_FrontLightProduct[0].specular;
	}

	// A great deal of pointless floating point operations (MAIN TEST FOR THIS SHADER)
	int f1, f2, f3, f4, f5;
	vec4 savedColor = color;
	for (int count = 0; count < loopCounter; ++count) {
		f1 = if1 * if2 - (if3 / if4) + if5;
		f2 = (if1 / if2 + (if3 * if4) - if5) / f1;
		f3 = (if1 * if2 - (if3 / if4) + if5) * f2;
		f4 = (if1 / if2 - (if3 * if4) + if5) / f3;
		f5 = (if1 * if2 + (if3 * if4) - if5) * f4;
		color *= (float) (f1 + f2 + f3 * f4 / f5);
	}

	//  Return sum of color components
	return color;
}

void main()
{
	gl_FragColor = phong() * texture2D(tex,gl_TexCoord[0].xy);
}
