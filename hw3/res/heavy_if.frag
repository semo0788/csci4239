//  Per-pixel Phong lighting
//  Fragment shader

varying vec3 View;
varying vec3 Light;
varying vec3 Normal;
varying vec4 Ambient;
uniform sampler2D tex;

vec4 phong()
{
	//  N is the object normal
	vec3 N = normalize(Normal);
	//  L is the light vector
	vec3 L = normalize(Light);

	//  Emission and ambient color
	vec4 color = Ambient;

	//  Diffuse light is cosine of light and normal vectors
	float Id = dot(L,N);
	if (Id>0.0)
	{
		//  Add diffuse
		color += Id*gl_FrontLightProduct[0].diffuse;
		//  R is the reflected light vector R = 2(L.N)N - L
		vec3 R = reflect(-L,N);
		//  V is the view vector (eye vector)
		vec3 V = normalize(View);
		//  Specular is cosine of reflected and view vectors
		float Is = dot(R,V);
		if (Is>0.0) color += pow(Is,gl_FrontMaterial.shininess)*gl_FrontLightProduct[0].specular;
	}

	// Multiple nested if statements (MAIN TEST FOR THIS SHADER)
	vec2 tex = gl_TexCoord[0].xy;
	if (tex.x > 0.334048496138) {
		if (tex.x > 0.967942124721) {
			if (tex.x > 0.967660693867) {
				if (tex.x > 0.154623883638) {
					if (tex.x > 0.994529463236) {
						if (tex.x > 0.986360384882) {
							if (tex.x > 0.675172460428) {
								if (tex.x > 0.0893284642762) {
									if (tex.x > 0.342547641308) {
										if (tex.x > 0.837008930031) {
											if (tex.y > 0.776760495369) {
												discard;
											}
										} else if (tex.x < 0.644974138545) {
											if (tex.y < 0.12064989134) {
												discard;
											}
										}
									} else if (tex.x < 0.00161473862231) {
										if (tex.x > 0.383782731478) {
											if (tex.y > 0.780160612004) {
												discard;
											}
										} else if (tex.x < 0.309455626612) {
											if (tex.y < 0.472570084567) {
												discard;
											}
										}
									}
								} else if (tex.x < 0.506815900747) {
									if (tex.y < 0.332873917183) {
										discard;
									}
								}
							} else if (tex.x < 0.805737717908) {
								if (tex.x > 0.605343252171) {
									if (tex.y > 0.621724939808) {
										discard;
									}
								} else if (tex.x < 0.740725197781) {
									if (tex.y < 0.0249541103881) {
										discard;
									}
								}
							}
							if (tex.x > 0.725954577994) {
								if (tex.x > 0.554179589974) {
									if (tex.y > 0.451604284088) {
										discard;
									}
								} else if (tex.x < 0.749268631236) {
									if (tex.y < 0.544089611994) {
										discard;
									}
								}
							} else if (tex.x < 0.215066960311) {
								if (tex.x > 0.264655010586) {
									if (tex.y > 0.328614485022) {
										discard;
									}
								} else if (tex.x < 0.329475598498) {
									if (tex.y < 0.383063298324) {
										discard;
									}
								}
							}
						} else if (tex.x < 0.912711982818) {
							if (tex.x > 0.896203930461) {
								if (tex.x > 0.199282026003) {
									if (tex.y > 0.432988490856) {
										discard;
									}
								} else if (tex.x < 0.0354780401191) {
									if (tex.y < 0.599903147583) {
										discard;
									}
								}
							} else if (tex.x < 0.0463190329015) {
								if (tex.x > 0.995484063151) {
									if (tex.y > 0.253208784836) {
										discard;
									}
								} else if (tex.x < 0.175878502072) {
									if (tex.y < 0.0351091102347) {
										discard;
									}
								}
							}
							if (tex.x > 0.42203262441) {
								if (tex.x > 0.474729995187) {
									if (tex.y > 0.550765074191) {
										discard;
									}
								} else if (tex.x < 0.418540203992) {
									if (tex.y < 0.776474039901) {
										discard;
									}
								}
							} else if (tex.x < 0.685218928853) {
								if (tex.x > 0.860192384734) {
									if (tex.y > 0.841301454048) {
										discard;
									}
								} else if (tex.x < 0.684753254574) {
									if (tex.y < 0.0328428824215) {
										discard;
									}
								}
							}
						}
					}
				} else if (tex.x < 0.953963006037) {
					if (tex.y < 0.597881905055) {
						discard;
					}
				}
			} else if (tex.x < 0.684067229793) {
				if (tex.x > 0.835946556392) {
					if (tex.y > 0.662683789363) {
						discard;
					}
				} else if (tex.x < 0.3429201728) {
					if (tex.y < 0.326111754475) {
						discard;
					}
				}
			}
			if (tex.x > 0.622079705227) {
				if (tex.x > 0.608260952898) {
					if (tex.x > 0.262844744119) {
						if (tex.x > 0.55503307119) {
							if (tex.x > 0.601508361784) {
								if (tex.x > 0.221749194319) {
									if (tex.y > 0.761449315845) {
										discard;
									}
								} else if (tex.x < 0.351047499444) {
									if (tex.y < 0.741080004025) {
										discard;
									}
								}
							} else if (tex.x < 0.41929825316) {
								if (tex.x > 0.893828189211) {
									if (tex.y > 0.336928507984) {
										discard;
									}
								} else if (tex.x < 0.608098515066) {
									if (tex.y < 0.772501356047) {
										discard;
									}
								}
							}
							if (tex.x > 0.520048850799) {
								if (tex.x > 0.910212782728) {
									if (tex.y > 0.729416014296) {
										discard;
									}
								} else if (tex.x < 0.237944467082) {
									if (tex.y < 0.444278210669) {
										discard;
									}
								}
							} else if (tex.x < 0.285949988486) {
								if (tex.x > 0.746934561608) {
									if (tex.y > 0.586907956728) {
										discard;
									}
								} else if (tex.x < 0.271725769846) {
									if (tex.y < 0.567285927305) {
										discard;
									}
								}
							}
						} else if (tex.x < 0.599126597397) {
							if (tex.x > 0.32889051525) {
								if (tex.x > 0.086319206452) {
									if (tex.y > 0.204205697795) {
										discard;
									}
								} else if (tex.x < 0.413367408922) {
									if (tex.y < 0.607878240481) {
										discard;
									}
								}
							} else if (tex.x < 0.129833863636) {
								if (tex.x > 0.590500801681) {
									if (tex.y > 0.663534051457) {
										discard;
									}
								} else if (tex.x < 0.15059118457) {
									if (tex.y < 0.720107372356) {
										discard;
									}
								}
							}
							if (tex.x > 0.112592020723) {
								if (tex.x > 0.711249448204) {
									if (tex.y > 0.208695714496) {
										discard;
									}
								} else if (tex.x < 0.0466800387207) {
									if (tex.y < 0.41016355566) {
										discard;
									}
								}
							} else if (tex.x < 0.283165955673) {
								if (tex.x > 0.397093905832) {
									if (tex.y > 0.0763407540189) {
										discard;
									}
								} else if (tex.x < 0.321031315886) {
									if (tex.y < 0.389602785836) {
										discard;
									}
								}
							}
						}
					}
				} else if (tex.x < 0.408018343343) {
					if (tex.y < 0.141850806703) {
						discard;
					}
				}
			} else if (tex.x < 0.363384459569) {
				if (tex.x > 0.107607971831) {
					if (tex.y > 0.871899653161) {
						discard;
					}
				} else if (tex.x < 0.422462240564) {
					if (tex.y < 0.772134543134) {
						discard;
					}
				}
			}
		} else if (tex.x < 0.798770401787) {
			if (tex.x > 0.490835938413) {
				if (tex.x > 0.0629593482625) {
					if (tex.y > 0.0846401764959) {
						discard;
					}
				} else if (tex.x < 0.781444367788) {
					if (tex.y < 0.877972603217) {
						discard;
					}
				}
			} else if (tex.x < 0.761338829041) {
				if (tex.x > 0.999008872967) {
					if (tex.x > 0.265847815914) {
						if (tex.x > 0.200957999528) {
							if (tex.x > 0.359409094323) {
								if (tex.y > 0.75280573998) {
									discard;
								}
							} else if (tex.x < 0.467995786694) {
								if (tex.y < 0.724311660158) {
									discard;
								}
							}
						} else if (tex.x < 0.935019705598) {
							if (tex.x > 0.526440328461) {
								if (tex.y > 0.613651860431) {
									discard;
								}
							} else if (tex.x < 0.817672282689) {
								if (tex.y < 0.190449304184) {
									discard;
								}
							}
						}
						if (tex.x > 0.750912989707) {
							if (tex.x > 0.111482614395) {
								if (tex.y > 0.597335052553) {
									discard;
								}
							} else if (tex.x < 0.908860631867) {
								if (tex.y < 0.0113624850585) {
									discard;
								}
							}
						} else if (tex.x < 0.254807229949) {
							if (tex.x > 0.464907013016) {
								if (tex.y > 0.703455193768) {
									discard;
								}
							} else if (tex.x < 0.911109626285) {
								if (tex.y < 0.854699788615) {
									discard;
								}
							}
						}
					} else if (tex.x < 0.556007712057) {
						if (tex.x > 0.981304099831) {
							if (tex.x > 0.943675139499) {
								if (tex.y > 0.155479133924) {
									discard;
								}
							} else if (tex.x < 0.551402475803) {
								if (tex.y < 0.0620077236698) {
									discard;
								}
							}
						} else if (tex.x < 0.956410968069) {
							if (tex.x > 0.220805048266) {
								if (tex.y > 0.0604685050179) {
									discard;
								}
							} else if (tex.x < 0.539622884758) {
								if (tex.y < 0.955600411302) {
									discard;
								}
							}
						}
						if (tex.x > 0.824735380052) {
							if (tex.x > 0.419662790617) {
								if (tex.y > 0.189182213863) {
									discard;
								}
							} else if (tex.x < 0.767957806457) {
								if (tex.y < 0.86762916332) {
									discard;
								}
							}
						} else if (tex.x < 0.648132336345) {
							if (tex.x > 0.885166057756) {
								if (tex.y > 0.500962791111) {
									discard;
								}
							} else if (tex.x < 0.553077693549) {
								if (tex.y < 0.00310154121583) {
									discard;
								}
							}
						}
					}
				}
			}
			if (tex.x > 0.252751463213) {
				if (tex.x > 0.493479618497) {
					if (tex.y > 0.792038871862) {
						discard;
					}
				} else if (tex.x < 0.554183381945) {
					if (tex.y < 0.285079957444) {
						discard;
					}
				}
			} else if (tex.x < 0.780463218758) {
				if (tex.x > 0.0911409078671) {
					if (tex.y > 0.860535761696) {
						discard;
					}
				} else if (tex.x < 0.953311511942) {
					if (tex.x > 0.657459527709) {
						if (tex.x > 0.843634250156) {
							if (tex.y > 0.966472408221) {
								discard;
							}
						} else if (tex.x < 0.772096125835) {
							if (tex.y < 0.276924678121) {
								discard;
							}
						}
					} else if (tex.x < 0.530121769041) {
						if (tex.x > 0.454588539911) {
							if (tex.y > 0.336416296483) {
								discard;
							}
						} else if (tex.x < 0.883647609733) {
							if (tex.y < 0.860412387237) {
								discard;
							}
						}
					}
				}
			}
		}
	}

	//  Return sum of color components
	return color;
}

void main()
{
	gl_FragColor = phong() * texture2D(tex,gl_TexCoord[0].xy);
}
