# Simple python script for replacing numbers with random numbers in text files

import sys
import re
import random

filename = sys.argv[1]
with open(filename, 'r') as infile:
	with open(filename + '.tmp', 'w') as outfile:
		TEX_PATTERN = re.compile(r'([<|>|=]{1,2}) \d*\.\d*', re.IGNORECASE)
		for line in infile:
			val = random.random()
			outfile.write(TEX_PATTERN.sub(r'\1 ' + str(val), line))
