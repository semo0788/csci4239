//  Per-pixel Phong lighting
//  Fragment shader

varying vec3 View;
varying vec3 Light;
varying vec3 Normal;
varying vec4 Ambient;
uniform sampler2D tex;

uniform int loopCounter = 500;
uniform float if1 = 1.0;
uniform float if2 = 2.0;
uniform float if3 = 4.0;
uniform float if4 = 8.0;
uniform float if5 = 16.0;

vec4 phong()
{
	//  N is the object normal
	vec3 N = normalize(Normal);
	//  L is the light vector
	vec3 L = normalize(Light);

	//  Emission and ambient color
	vec4 color = Ambient;

	//  Diffuse light is cosine of light and normal vectors
	float Id = dot(L,N);
	if (Id>0.0)
	{
		//  Add diffuse
		color += Id*gl_FrontLightProduct[0].diffuse;
		//  R is the reflected light vector R = 2(L.N)N - L
		vec3 R = reflect(-L,N);
		//  V is the view vector (eye vector)
		vec3 V = normalize(View);
		//  Specular is cosine of reflected and view vectors
		float Is = dot(R,V);
		if (Is>0.0) color += pow(Is,gl_FrontMaterial.shininess)*gl_FrontLightProduct[0].specular;
	}

	// A great deal of pointless floating point operations (MAIN TEST FOR THIS SHADER)
	float f1, f2, f3, f4, f5;
	vec4 savedColor = color;
	for (int count = 0; count < loopCounter; ++count) {
		f1 =  cos(if1) * sin(if2) - (cos(if3) / sin(if4)) + cos(if5);
		f2 = (sin(if1) / cos(if2) + (sin(if3) * cos(if4)) - sin(if5)) / cos(f1);
		f3 = (cos(if1) * sin(if2) - (cos(if3) / sin(if4)) + cos(if5)) * sin(f2);
		f4 = (sin(if1) / cos(if2) - (sin(if3) * cos(if4)) + sin(if5)) / cos(f3);
		f5 = (cos(if1) * sin(if2) + (cos(if3) * sin(if4)) - cos(if5)) * sin(f4);
		color *= (exp(f1) + exp(f2) + log(f3) * log(f4) / f5);
	}

	//  Return sum of color components
	return color;
}

void main()
{
	gl_FragColor = phong() * texture2D(tex,gl_TexCoord[0].xy);
}
