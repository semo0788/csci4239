/*
 *	HW2 Source File for Sean Moss.
 */

#include "FullscreenQuad.hpp"

//=================================================================================================
FullscreenQuad::FullscreenQuad(const QString tex)
{
	if (tex.length() > 0)
		setTexture(tex);
}

//=================================================================================================
void FullscreenQuad::display()
{
	glPushMatrix();
	EnableTex();

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	float height = Object::WidgetHeight, width = Object::WidgetWidth;
	float asp = width / height;

	glOrtho(-asp, +asp, -1, 1, -1, 1);

	glBegin(GL_QUADS);
	useColor(Color(1,0,0));
    glNormal3f( 0, 0, 1);
    glTexCoord2f(0,0); glVertex3f(-asp,-1, 0);
    glTexCoord2f(1,0); glVertex3f(asp,-1, 0);
    glTexCoord2f(1,1); glVertex3f(asp,+1, 0);
    glTexCoord2f(0,1); glVertex3f(-asp,+1, 0);
    glEnd();

	DisableTex();
	glPopMatrix();
}