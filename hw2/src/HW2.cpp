/*
 *	HW2 Source File for Sean Moss.
 */

#include <QApplication>
#include "hw2view.hpp"

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);

	// Create and display the assignment widget
	HW2View view;
	view.show();

	return app.exec();
}
