/*
 *	HW2 Source File for Sean Moss.
 */

#include "hw2opengl.hpp"
#include "Cube.h"
#include "WaveOBJ.h"
#include "Teapot.h"
#include "FullscreenQuad.hpp"

// ================================================================================================
HW2OpenGL::HW2OpenGL(QWidget* parent)
    : CUgl(parent)
{
   x0 = y0 = 0;
   z0 = 1;
}

// ================================================================================================
void HW2OpenGL::setX(double X)
{
   x0 = X;
   //  Request redisplay
   update();
}

// ================================================================================================
void HW2OpenGL::setY(double Y)
{
   y0 = Y;
   //  Request redisplay
   update();
}

// ================================================================================================
void HW2OpenGL::setZoom(double Z)
{
   z0 = Z;
   //  Request redisplay
   update();
}

// ================================================================================================
void HW2OpenGL::initializeGL()
{
   //  Load shaders
   //addShader(":/ex03a.vert",":/ex03a.frag");
   //addShader(":/ex03b.vert",":/ex03b.frag");
   //addShader(":/ex03b.vert",":/ex03c.frag");
   //addShader(":/ex03b.vert",":/ex03d.frag");
   addShader(":/basic.vert", ":/sine.frag");
   addShader(":/basic.vert", ":/sinediscard.frag");
   addShader(":/basic.vert", ":/waves.frag");
   addShader(":/basic.vert", ":/radial.frag");

   // Load objects
   addObject(new Cube(":/pi.png"));
   addObject(new Teapot(8,":/pi.png",0.5,Color(0,1,1)));
   addObject(new WaveOBJ("suzanne.obj",":/",1.0));
   addObject(new FullscreenQuad(":/pi.png"));
   setObject(0);
}

// ================================================================================================
void HW2OpenGL::paintGL()
{
   //  Clear screen and Z-buffer
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
   glEnable(GL_DEPTH_TEST);

   //  Set view
   doView();

   //  Enable lighting
   doLight();

   //  Apply shader
   if (mode)
   {
      shader[mode]->bind();
      //  Dimensions
      QVector3D loc(x0,y0,1/z0);
      shader[mode]->setUniformValue("loc",loc);
      //  Time
      float t = getTime();
      shader[mode]->setUniformValue("time",t);
   }

   //  Draw scene
   doScene();

   //  Release shader
   if (mode) shader[mode]->release();
   glDisable(GL_LIGHTING);
   glDisable(GL_DEPTH_TEST);
   
   //  Emit centers to display
   emit zoom(QString::number((int)z0));
}
