/*
 *	HW2 Source File for Sean Moss.
 */

#include <QComboBox>
#include <QLabel>
#include <QGridLayout>
#include "hw2view.hpp"

// ================================================================================================
HW2View::HW2View()
{
   //  Set window title
   setWindowTitle(tr("Assignment 2: Sean Moss"));

   //  Create new OpenGL widget
   ogl = new HW2OpenGL;

   //  Select shader
   QComboBox* shader = new QComboBox();
   shader->addItem("None");
   //shader->addItem("Stored");
   //shader->addItem("Brick");
   //shader->addItem("Mandelbrot");
   //shader->addItem("Mandelhole");
   shader->addItem("Sine Pattern");
   shader->addItem("Sine Pattern (Discard)");
   shader->addItem("Waves");
   shader->addItem("Radial Pattern");

   //  Select projection
   QComboBox* projection = new QComboBox();
   projection->addItem("Ortho.");
   projection->addItem("Persp.");

   //  Select object
   QComboBox* object = new QComboBox();
   object->addItem("Cube");
   object->addItem("Teapot");
   object->addItem("Suzanne");
   object->addItem("Fullscreen Quad");

   //  Center position
   Xpos = new QDoubleSpinBox();
   Ypos = new QDoubleSpinBox();
   Zpos = new QSlider(Qt::Horizontal);
   Xpos->setRange(-100,100);Xpos->setDecimals(6);Xpos->setSingleStep(0.1);
   Ypos->setRange(-100,100);Ypos->setDecimals(6);Ypos->setSingleStep(0.1);
   Zpos->setRange(0,100);

   //  View angle and zoom
   QLabel* angles = new QLabel();
   QLabel* zoom  = new QLabel();

   //  Pause/resume button
   light = new QPushButton("Pause");

   //  Reset
   QPushButton* rst = new QPushButton("Reset");
   //  Quit
   QPushButton* quit = new QPushButton("Quit");

   //  Set layout of child widgets
   QGridLayout* layout = new QGridLayout;
   layout->addWidget(ogl,0,0,4,1);

   // Add the scene control
   QGroupBox *sceneCtrl = prepareSceneControls(shader, projection, object, angles);
   layout->addWidget(sceneCtrl, 0, 1, 1, 2); 

   // Add the texture control
   QGroupBox *texCtrl = prepareTextureControls(zoom);
   layout->addWidget(texCtrl, 1, 1, 1, 2);  

   layout->addWidget(rst,3,1);
   layout->addWidget(quit,3,2);
   //  Manage resizing
   layout->setColumnStretch(0,100);
   layout->setColumnMinimumWidth(0,125);
   layout->setColumnMinimumWidth(1, 110);
   layout->setColumnMinimumWidth(2, 110);
   layout->setRowStretch(2,100);
   setLayout(layout);

   //  Connect valueChanged() signals to ogl
   connect(shader,SIGNAL(currentIndexChanged(int))     , ogl,SLOT(setShader(int)));
   connect(object,SIGNAL(currentIndexChanged(int))     , ogl,SLOT(setObject(int)));
   connect(projection,SIGNAL(currentIndexChanged(int)) , ogl,SLOT(setPerspective(int)));
   connect(Xpos,SIGNAL(valueChanged(double)) , ogl,SLOT(setX(double)));
   connect(Ypos,SIGNAL(valueChanged(double)) , ogl,SLOT(setY(double)));
   connect(Zpos,SIGNAL(valueChanged(int)) , this,SLOT(izoom(int)));
   //  Connect angles() and zoom() signal to labels
   //  Connect angles() and zoom() signal to labels
   connect(ogl,SIGNAL(angles(QString)) , angles,SLOT(setText(QString)));
   connect(ogl,SIGNAL(zoom(QString))   , zoom,SLOT(setText(QString)));
   //  Connect reset() and lmove() signals
   connect(rst  ,SIGNAL(pressed()),this,SLOT(reset()));
   connect(light,SIGNAL(pressed()),this,SLOT(lmove()));
   //  Connect quit() signal to qApp::quit()
   connect(quit,SIGNAL(pressed()) , qApp,SLOT(quit()));
}

// ================================================================================================
void HW2View::reset()
{
   Xpos->setValue(0);
   Ypos->setValue(0);
   Zpos->setValue(0);
   ogl->reset();
}

// ================================================================================================
void HW2View::izoom(int iz)
{
   //  Map coordinates
   float z = powf(2.0,0.2*iz);
   ogl->setZoom(z);
   //  Set Increments
   int dec = iz/16+1;
   float step = powf(10.0,-dec);
   Xpos->setSingleStep(step);
   Ypos->setSingleStep(step);
}

// ================================================================================================
void HW2View::lmove()
{
   bool pause = (light->text()=="Pause");
   if (pause)
      light->setText("Animate");
   else
      light->setText("Pause");
   ogl->setLightMove(!pause);
}

// ================================================================================================
QGroupBox* HW2View::prepareSceneControls(QComboBox *shader, QComboBox *proj, QComboBox *obj, QLabel *angles)
{
    QGroupBox *group = new QGroupBox(tr("Scene Controls"));

    QGridLayout *layout = new QGridLayout;
    layout->addWidget(new QLabel("Shader"), 0, 0);
    layout->addWidget(shader, 0, 1);
    layout->addWidget(new QLabel("Projection"), 1, 0);
    layout->addWidget(proj, 2, 0);
    layout->addWidget(new QLabel("Object"), 1, 1);
    layout->addWidget(obj, 2, 1);
    layout->addWidget(new QLabel("Light"), 3, 0);
    layout->addWidget(light, 3, 1);
    layout->addWidget(new QLabel("Angles"), 4, 0);
    layout->addWidget(angles, 4, 1);

    group->setLayout(layout);
    return group;
}

// ================================================================================================
QGroupBox* HW2View::prepareTextureControls(QLabel *zoom)
{
    QGroupBox *group = new QGroupBox(tr("Texture Controls"));

    QGridLayout *layout = new QGridLayout;
    layout->addWidget(new QLabel("X Position"), 0, 0);
    layout->addWidget(Xpos, 1, 0);
    layout->addWidget(new QLabel("Y Position"), 0, 1);
    layout->addWidget(Ypos, 1, 1);
    layout->addWidget(new QLabel("Zoom Level"), 2, 0);
    layout->addWidget(Zpos, 2, 1);
    layout->addWidget(new QLabel("Zoom"), 3, 0);
    layout->addWidget(zoom, 3, 1);

    group->setLayout(layout);
    return group;
}