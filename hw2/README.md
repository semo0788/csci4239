## HW2

To run this program, simply run `qmake` and then `make` in the directory. The resulting executable is called "hw2".

The interface is adapted from the ex03 code, as well as the code from hw1.

I changed the layout of the controls to be a bit better grouped. This also opened up space for a possible future feature of adding shader controls in another QGroupBox. I also added a FullscreenQuad object, which helps to see the details of the textures across the entire screen.

I included 4 procedural shaders:
1. Sine (`sine.frag`) - Creates a sinusoidal pattern that alternates with time.
2. Sine Discard (`sinediscard.frag`) - Same as Sine, but discards above a certain threshold.
3. Wave (`wave.frag`) - Creates a moving wave that alternates between green and purple transversely.
4. Radial (`radial.frag`) - Creates a radial pattern that makes use of the fract and floor functions. Adapted from code found at `glslsandbox.com`. This one looks particularly cool on the teapot.


#### Time
This assignment took me about 7 hours to complete, including creating a new Object subclass, fixing lighting for this new Object, and writing shaders.

*Disclaimer: this homework made heavy use of the helper classes provided by Willem in the "ex01" code set. That code belongs to him.*