/*
 *	HW2 Header File for Sean Moss.
 */

#ifndef hw2opengl_hpp_
#define hw2opengl_hpp_

#include "CUgl.h"

class HW2OpenGL : 
    public CUgl
{
    Q_OBJECT

private:
   float x0,y0; // Model center
   float z0;    // Zoom level

public:
   HW2OpenGL(QWidget* parent=0);                  // Constructor
   QSize sizeHint() const {return QSize(500,500);} // Default size of widget

public slots:
    void setX(double X);     // Set Model X position
    void setY(double Y);     // Set Model Y position
    void setZoom(double Z);  // Set Zoom level
signals:
    void zoom(QString text); // Zoom level
    
protected:
    void initializeGL();     // Initialize widget
    void paintGL();          // Draw widget
};

#endif // hw2opengl_hpp_
