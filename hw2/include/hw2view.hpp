/*
 *	HW2 Header File for Sean Moss.
 */

#ifndef hw2view_hpp_
#define hw2view_hpp_

#include <QWidget>
#include <QSlider>
#include <QDoubleSpinBox>
#include <QGroupBox>
#include "hw2opengl.hpp"

class HW2View : 
    public QWidget
{
    Q_OBJECT
    
private:
   QDoubleSpinBox* Xpos;
   QDoubleSpinBox* Ypos;
   QSlider*        Zpos;
   QPushButton*    light;
   HW2OpenGL*     ogl;

private slots:
   void reset();        //  Reset angles
   void lmove();        //  Pause/animate light
   void izoom(int iz);  //  Zoom level (percent)

public:
    HW2View();

private:
    QGroupBox* prepareSceneControls(QComboBox *shader, QComboBox *proj, QComboBox *obj, QLabel *angles);
    QGroupBox* prepareTextureControls(QLabel *zoom);
    
};

#endif // hw2view_hpp_
