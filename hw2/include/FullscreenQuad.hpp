/*
 *	HW2 Header File for Sean Moss.
 */

#ifndef fullscreen_quad_hpp_
#define fullscreen_quad_hpp_

#include "Object.h"

class FullscreenQuad : 
	public Object
{
public:
   FullscreenQuad(const QString tex = "");
   void display();
};

#endif // fullscreen_quad_hpp_