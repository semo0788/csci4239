/*
 * Fragment Shader by Sean Moss
 * This generates slow moving waves of two colors.
 */

// Colors
const vec3 COLOR_1 = vec3(1.0, 0.0, 1.0);

// Model coordinates and light from vertex shader
varying float LightIntensity;
varying vec2  ModelPos;
uniform float time;

vec3 pattern(float x, float y)
{
	return vec3(sin(x * y), sin(x * y), sin(x * y));
}

void main()
{
	vec2 texpos = ModelPos;

	vec3 color = COLOR_1;
	float phase = time + (texpos.y / 7.0);

	color *= pattern(cos(phase), sin(phase));
	phase += (texpos.x * 7.0);
	color += pattern(sin(phase), cos(phase));

	gl_FragColor = vec4(color, 1.0) * LightIntensity;
}