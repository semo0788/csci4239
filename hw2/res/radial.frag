/*
 * Fragment Shader by Sean Moss
 * This generates a very interesting radial pattern.
 * Code for this shader was adapted from code found on glslsandbox.com.
 */

#define M_PI 3.1415926535

// Colors
const vec3 COLOR_1 = vec3(1.0, 0.3, 0.0);
const vec3 COLOR_2 = vec3(0.3, 0.2, 0.5);

// Model coordinates and light from vertex shader
varying float LightIntensity;
varying vec2  ModelPos;
uniform float time;

void main()
{
	vec2 texpos = ModelPos;

	vec2 adjusted = vec2(
		atan(texpos.x, texpos.y) * 3.184 + time,
		length(3.0 * texpos) * (2.0 + sin(time))
	);

	float p = floor(adjusted.y) / 5.0;
	vec2 ar = abs(fract(adjusted) - 0.5);

	gl_FragColor = vec4(mix(COLOR_1, COLOR_2, p) * 0.1 / dot(ar, ar), 1.0);
}