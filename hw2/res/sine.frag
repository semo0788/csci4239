/*
 * Fragment Shader by Sean Moss
 * Simply generates a two dimensional sine pattern, which varies in position and order over time.
 */

#define M_PI 3.1415926535

// Colors
const vec3 COLOR_1 = vec3(1.0, 0.2, 0.2);
const vec3 COLOR_2 = vec3(0.2, 0.2, 1.0);

// Model coordinates and light from vertex shader
varying float LightIntensity;
varying vec2  ModelPos;
uniform float time;

void main()
{
	float order = 24.0 + (sin(time / 1.5) * 3.0);
	float dx = time / 2.0;
	float rot = (M_PI / 4.0) * sin(time / 5.0);

	vec2 texpos = ModelPos;
	texpos.x *= cos(rot);
	texpos.y *= sin(rot);
	texpos.x += dx;

	float amt = (sin(texpos.x * order) + sin(texpos.y * order) + 2.0) / 4.0;

	vec3 color = mix(COLOR_1, COLOR_2, amt) * LightIntensity;

	gl_FragColor = vec4(color, 1.0);
}