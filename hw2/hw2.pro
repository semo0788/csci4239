HEADERS = include/Cube.h \
          include/CUgl.h \
          include/FullscreenQuad.hpp \
          include/hw2opengl.hpp \
          include/hw2view.hpp \
          include/Object.h \
          include/Sphere.h \
          include/Teapot.h \
          include/Type.h \
          include/WaveOBJ.h
SOURCES = src/Cube.cpp \
          src/CUgl.cpp \
          src/FullscreenQuad.cpp \
          src/HW2.cpp \
          src/hw2opengl.cpp \
          src/hw2view.cpp \
          src/Object.cpp \
          src/Sphere.cpp \
          src/Teapot.cpp \
          src/WaveOBJ.cpp
QT += opengl
RESOURCES = res/hw2.qrc
INCLUDEPATH += $$PWD/include
QMAKE_CXXFLAGS += -std=c++11
