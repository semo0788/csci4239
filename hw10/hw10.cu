/*
 *  CUDA square matrix multiplier
 *
 *  The size of the matrix is width*blocks
 *
 *  Parameters:
 *  -v      Verbose - show hardware detila
 *  width   Block width (width squared <= max threads/block)
 *  blocks  Number of blocks
 */

#ifdef _WIN32
#include <windows.h>
#else
#include <sys/time.h>
#endif

#include "funcs.cuh"

/*
 *  Return elapsed wall time since last call (seconds)
 */
static double t0=0;
double Elapsed(void)
{
#ifdef _WIN32
	//  Windows version of wall time
	LARGE_INTEGER tv,freq;
	QueryPerformanceCounter((LARGE_INTEGER*)&tv);
	QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
	double t = tv.QuadPart/(double)freq.QuadPart;
#else
	//  Unix/Linux/OSX version of wall time
	struct timeval tv;
	gettimeofday(&tv,NULL);
	double t = tv.tv_sec+1e-6*tv.tv_usec;
#endif
	double s = t-t0;
	t0 = t;
	return s;
}

void Fatal(const char* format , ...)
{
   va_list args;
   va_start(args,format);
   vfprintf(stderr,format,args);
   va_end(args);
   exit(1);
}

/*
 *  Initialize matrix with random values
 */
void RandomInit(float x[])
{
	for (unsigned int i = 0; i < MATRIX_COUNT; i++)
		x[i] = rand() / (float)RAND_MAX;
}

/*
 *  Initialize fastest GPU device
 */
void InitGPU()
{
	//  Get number of CUDA devices
	int num;
	cudaError_t err;
	if (err = cudaGetDeviceCount(&num)) Fatal("Cannot get number of CUDA devices: %s\n", cudaGetErrorString(err));
	if (num<1) Fatal("No CUDA devices found\n");

	//  Get fastest device
	cudaDeviceProp prop;
	int MaxDevice = -1;
	int MaxGflops = -1;
	for (int dev=0;dev<num;dev++)
	{
		if (cudaGetDeviceProperties(&prop,dev)) Fatal("Error getting device %d properties\n",dev);
		int Gflops = prop.multiProcessorCount * prop.clockRate;
		if(Gflops > MaxGflops)
		{
			MaxGflops = Gflops;
			MaxDevice = dev;
		}
	}

	//  Print and set device
	if (cudaGetDeviceProperties(&prop,MaxDevice)) Fatal("Error getting device %d properties\n",MaxDevice);
	printf("Fastest CUDA Device %d: %s\n",MaxDevice,prop.name);
	cudaSetDevice(MaxDevice);
}

void test_matmul(float *Ch, float *Ah, float *Bh, float *Rh, float *Cd, float *Ad, float *Bd)
{
	RandomInit(Ah);
	RandomInit(Bh);

	if (cudaMemcpy(Ad, Ah, MATRIX_MEMSIZE, cudaMemcpyHostToDevice)) Fatal("Cannot copy A from host to device\n");
	if (cudaMemcpy(Bd, Bh, MATRIX_MEMSIZE, cudaMemcpyHostToDevice)) Fatal("Cannot copy B from host to device\n");

	// Compute R = AB on host
	Elapsed();
	host_matmul(Rh, Ah, Bh);
	double Th = Elapsed();

	// Compute C = AB on device
	Elapsed();
	device_matmul(Cd, Ad, Bd);
	double Td = Elapsed();

	if (cudaMemcpy(Ch, Cd, MATRIX_MEMSIZE, cudaMemcpyDeviceToHost)) Fatal("Cannot copy C from device to host\n");

	// Compute difference between R and C
	double r2 = 0;
	for (int i = 0; i < MATRIX_COUNT; i++)
		r2 += fabs(Ch[i] - Rh[i]);
	r2 /= MATRIX_COUNT;

	// Print results
	printf("  Host   Time = %6.4f s\n", Th);
	printf("  Device Time = %6.4f s\n", Td);
	printf("  Speedup = %.2f\n", Th / Td);
	printf("  Difference = %.2e\n", r2);
}

void test_matmin(float *Ah, float *Ad)
{
	RandomInit(Ah);
	Ah[10 * MATRIX_SIZE + 10] = -100.0; // Make an artifical minimum at (10, 10)

	if (cudaMemcpy(Ad, Ah, MATRIX_MEMSIZE, cudaMemcpyHostToDevice)) Fatal("Cannot copy A from host to device\n");

	// Compute mat_min on host
	float hval = 0;
	int hx = 0, hy = 0;
	Elapsed();
	host_matmin(&hval, &hx, &hy, Ah);
	double Th = Elapsed();

	// Compute mat_min on device
	float dval = 0;
	int dx = 0, dy = 0;
	Elapsed();
	device_matmin(&dval, &dx, &dy, Ad);
	double Td = Elapsed();

	// Print results
	printf("  Host   Time = %6.4f s\n", Th);
	printf("  Device Time = %6.4f s\n", Td);
	printf("  Speedup = %.2f\n", Th / Td);
	printf("  Value: val = %.2f, x = %d, y = %d\n", dval, dx, dy);
	printf("  Difference: val = %.2e, x = %d, y = %d\n", fabs(dval - hval), abs(dx - hx), abs(dy - hy));
}

void test_matmax(float *Ah, float *Ad)
{
	RandomInit(Ah);
	Ah[10 * MATRIX_SIZE + 10] = 100.0; // Make an artifical maximum at (10, 10)

	if (cudaMemcpy(Ad, Ah, MATRIX_MEMSIZE, cudaMemcpyHostToDevice)) Fatal("Cannot copy A from host to device\n");

	// Compute mat_max on host
	float hval = 0;
	int hx = 0, hy = 0;
	Elapsed();
	host_matmax(&hval, &hx, &hy, Ah);
	double Th = Elapsed();

	// Compute mat_max on device
	float dval = 0;
	int dx = 0, dy = 0;
	Elapsed();
	device_matmax(&dval, &dx, &dy, Ad);
	double Td = Elapsed();

	// Print results
	printf("  Host   Time = %6.4f s\n", Th);
	printf("  Device Time = %6.4f s\n", Td);
	printf("  Speedup = %.2f\n", Th / Td);
	printf("  Value: val = %.2f, x = %d, y = %d\n", dval, dx, dy);
	printf("  Difference: val = %.2e, x = %d, y = %d\n", fabs(dval - hval), abs(dx - hx), abs(dy - hy));
}

void test_mattrans(float *Rh, float *Bh, float *Ah, float *Rd, float *Ad)
{
	RandomInit(Ah);

	if (cudaMemcpy(Ad, Ah, MATRIX_MEMSIZE, cudaMemcpyHostToDevice)) Fatal("Cannot copy A from host to device\n");

	// Compute A^T on host
	Elapsed();
	host_mattrans(Bh, Ah);
	double Th = Elapsed();

	// Compute A^T on device
	Elapsed();
	device_mattrans(Rd, Ad);
	double Td = Elapsed();

	if (cudaMemcpy(Rh, Rd, MATRIX_MEMSIZE, cudaMemcpyDeviceToHost)) Fatal("Cannot copy B from device to host\n");

	// Compute difference between R and C
	double r2 = 0;
	for (int i = 0; i < MATRIX_COUNT; i++)
		r2 += fabs(Bh[i] - Rh[i]);
	r2 /= MATRIX_COUNT;

	// Print results
	printf("  Host   Time = %6.5f s\n", Th);
	printf("  Device Time = %6.5f s\n", Td);
	printf("  Speedup = %.2f\n", Th / Td);
	printf("  Difference = %.2e\n", r2);
}

void test_matnorm(float *Ah, float *Ad)
{
	RandomInit(Ah);

	if (cudaMemcpy(Ad, Ah, MATRIX_MEMSIZE, cudaMemcpyHostToDevice)) Fatal("Cannot copy A from host to device\n");

	// Compute mat_max on host
	float hval = 0;
	Elapsed();
	host_matnorm(&hval, Ah);
	double Th = Elapsed();

	// Compute mat_max on device
	float dval = 0;
	Elapsed();
	device_matnorm(&dval, Ad);
	double Td = Elapsed();

	printf("  Host   Time = %6.5f s\n", Th);
	printf("  Device Time = %6.5f s\n", Td);
	printf("  Speedup = %.2f\n", Th / Td);
	printf("  Difference = %.3e\n", fabs(sqrt(dval) - sqrt(hval)));
}

/*
 *  main
 */
int main(int argc, char* argv[])
{
	// Initialize GPU
	InitGPU();

	// Allocate host matrices A/B/C/R
	float* Ah = (float*)malloc(MATRIX_MEMSIZE);
	float* Bh = (float*)malloc(MATRIX_MEMSIZE);
	float* Ch = (float*)malloc(MATRIX_MEMSIZE);
	float* Rh = (float*)malloc(MATRIX_MEMSIZE);
	if (!Ah || !Bh || !Ch || !Rh) Fatal("Cannot allocate host memory\n");

	// Allocate device matrices A/B/C/R
	float *Ad, *Bd, *Cd, *Rd;
	if (cudaMalloc((void**)&Ad, MATRIX_MEMSIZE)) Fatal("Cannot allocate device memory Ad\n");
	if (cudaMalloc((void**)&Bd, MATRIX_MEMSIZE)) Fatal("Cannot allocate device memory Bd\n");
	if (cudaMalloc((void**)&Cd, MATRIX_MEMSIZE)) Fatal("Cannot allocate device memory Cd\n");
	if (cudaMalloc((void**)&Rd, MATRIX_MEMSIZE)) Fatal("Cannot allocate device memory Rd\n");

	srand(9999);

	// Run all of the tests
	printf("Running Matrix multiplication test (1 / 5).\n");
	test_matmul(Ch, Ah, Bh, Rh, Cd, Ad, Bd);
	printf("Running Matrix minimum locator test (2 / 5).\n");
	test_matmin(Ah, Ad);
	printf("Running Matrix maximum locator test (3 / 5).\n");
	test_matmax(Ah, Ad);
	printf("Running Matrix transposition test (4 / 5).\n");
	test_mattrans(Rh, Bh, Ah, Rd, Ad);
	printf("Running Matrix Frobrenius norm test (5 / 5).\n");
	test_matnorm(Ah, Ad);

	// Free host memory
	free(Ah);
	free(Bh);
	free(Ch);
	free(Rh);

	// Free device memory
	cudaFree(Ad);
	cudaFree(Bd);
	cudaFree(Cd);
	cudaFree(Rd);

	// Done
	return 0;
}
