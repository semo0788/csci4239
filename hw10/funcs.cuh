// HW10 functions

#ifndef FUNCS_CUH_
#define FUNCS_CUH_

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <cuda.h>

#define MATRIX_SIZE (1024)
#define VECTOR_SIZE (1024)
#define MATRIX_COUNT (MATRIX_SIZE * MATRIX_SIZE)
#define VECTOR_COUNT VECTOR_SIZE
#define BLOCK_SIZE (32)
#define MATRIX_MEMSIZE (MATRIX_COUNT * sizeof(float))
#define VECTOR_MEMSIZE (VECTOR_COUNT * sizeof(float))

/*
 *  Print message to stderr and exit
 */
void Fatal(const char* format , ...);

// C = A*B
extern void host_matmul(float Ch[], const float Ah[], const float Bh[]);
extern void device_matmul(float Cd[], const float Ad[], const float Bd[]);

// val = min(A)
extern void host_matmin(float *val, int *x, int *y, const float Ah[]);
extern void device_matmin(float *val, int *x, int *y, const float Ad[]);

// val = max(A)
extern void host_matmax(float *val, int *x, int *y, const float Ah[]);
extern void device_matmax(float *val, int *x, int *y, const float Ad[]);

// B = A^T
extern void host_mattrans(float Bh[], const float Ah[]);
extern void device_mattrans(float Bd[], const float Ad[]);

// val = norm(A);
extern void host_matnorm(float *val, const float Ah[]);
extern void device_matnorm(float *val, const float Ad[]);

#endif // FUNCS_CUH_