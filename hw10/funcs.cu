#include "funcs.cuh"
#include <limits>
#include <float.h>


// ================================================================================================
void host_matmul(float Ch[], const float Ah[], const float Bh[])
{
	for(unsigned int i = 0 ; i < MATRIX_SIZE; i++) {
		for (unsigned int j = 0; j < MATRIX_SIZE; j++) {
			double sum = 0;
			for (unsigned int k = 0; k < MATRIX_SIZE; k++) {
				sum += (double)Ah[i * MATRIX_SIZE + k] * (double)Bh[k * MATRIX_SIZE + j];
			}
			Ch[i * MATRIX_SIZE + j] = (float)sum;
		}
	}
}

// ================================================================================================
__global__ void _device_matmul_impl(float Cd[], const float Ad[], const float Bd[])
{
	unsigned int j = blockIdx.x*blockDim.x+threadIdx.x;
	unsigned int i = blockIdx.y*blockDim.y+threadIdx.y;
	float sum = 0;
	for (int k = 0; k < MATRIX_SIZE; k++)
		sum += Ad[i * MATRIX_SIZE + k] * Bd[k * MATRIX_SIZE + j];
	Cd[i * MATRIX_SIZE + j] = sum;
}

// ================================================================================================
void device_matmul(float Cd[], const float Ad[], const float Bd[])
{
	dim3 threads(BLOCK_SIZE, BLOCK_SIZE);
	dim3 grid(BLOCK_SIZE, BLOCK_SIZE);
	_device_matmul_impl<<<grid, threads>>>(Cd, Ad, Bd);
	if (cudaGetLastError()) Fatal("Matrix multiplication failed\n");
}




// ================================================================================================
void host_matmin(float *val, int *x, int *y, const float Ah[])
{
	float min = FLT_MAX;
	int tx = -1, ty = -1;
	
	for(unsigned int i = 0 ; i < MATRIX_SIZE; i++) {
		for (unsigned int j = 0; j < MATRIX_SIZE; j++) {
			if (Ah[i * MATRIX_SIZE + j] < min) {
				min = Ah[i * MATRIX_SIZE + j];
				tx = j;
				ty = i;
			}
		}
	}

	*val = min;
	*x = tx;
	*y = ty;
}

// ================================================================================================
__global__ void _device_matmin_row_impl(float rmin[], int minpos[], const float Ad[])
{
	const unsigned int T_IDX = blockIdx.y * blockDim.y + threadIdx.y;

	float tmin = FLT_MAX;
	int tpos = -1;

	const int BASE_IDX = T_IDX * MATRIX_SIZE;
	for (int i = 0; i < MATRIX_SIZE; ++i) {
		if (Ad[BASE_IDX + i] < tmin) {
			tmin = Ad[BASE_IDX + i];
			tpos = i;
		}
	}

	rmin[T_IDX] = tmin;
	minpos[T_IDX] = tpos;
}

// ================================================================================================
// This launches on only a single thread on the GPU, but is kept there for fairness in testing
__global__ void _device_matmin_final_impl(float vals[], const float rmin[], const int minpos[])
{
	float tmin = FLT_MAX;
	int tx = -1, ty = -1;

	for (int i = 0; i < MATRIX_SIZE; ++i) {
		if (rmin[i] < tmin) {
			tmin = rmin[i];
			tx = minpos[i];
			ty = i;
		}
	}

	vals[0] = tmin;
	vals[1] = tx;
	vals[2] = ty;
}

// ================================================================================================
void device_matmin(float *val, int *x, int *y, const float Ad[])
{
	float *rmin;
	int *minpos;
	float *vals;
	if (cudaMalloc((void**)&rmin, VECTOR_SIZE * sizeof(float))) Fatal("Cannot allocate device memory rmin\n");
	if (cudaMalloc((void**)&minpos, VECTOR_SIZE * sizeof(int))) Fatal("Cannot allocate device memory minpos\n");
	if (cudaMalloc((void**)&vals, 3 * sizeof(float))) Fatal("Cannot allocate device memory vals\n");

	dim3 threads(1, BLOCK_SIZE);
	dim3 grid(1, BLOCK_SIZE);
	_device_matmin_row_impl<<<grid,threads>>>(rmin, minpos, Ad);
	if (cudaGetLastError()) Fatal("Matrix matmin_row failed\n");
	_device_matmin_final_impl<<<dim3(1, 1), dim3(1, 1)>>>(vals, rmin, minpos);
	if (cudaGetLastError()) Fatal("Matrix matmin_final failed\n");

	float hostvals[3];
	if (cudaMemcpy(hostvals, vals, 3 * sizeof(float), cudaMemcpyDeviceToHost)) Fatal("Cannot copy vals from device to host\n");

	cudaFree(rmin);
	cudaFree(minpos);
	cudaFree(vals);	

	*val = hostvals[0];
	*x = (int)hostvals[1];
	*y = (int)hostvals[2];
}




// ================================================================================================
void host_matmax(float *val, int *x, int *y, const float Ah[])
{
	float max = FLT_MIN;
	int tx = -1, ty = -1;
	
	for(unsigned int i = 0 ; i < MATRIX_SIZE; i++) {
		for (unsigned int j = 0; j < MATRIX_SIZE; j++) {
			if (Ah[i * MATRIX_SIZE + j] > max) {
				max = Ah[i * MATRIX_SIZE + j];
				tx = j;
				ty = i;
			}
		}
	}

	*val = max;
	*x = tx;
	*y = ty;
}

// ================================================================================================
__global__ void _device_matmax_row_impl(float rmax[], int maxpos[], const float Ad[])
{
	const unsigned int T_IDX = blockIdx.y * blockDim.y + threadIdx.y;

	float tmax = FLT_MIN;
	int tpos = -1;

	const int BASE_IDX = T_IDX * MATRIX_SIZE;
	for (int i = 0; i < MATRIX_SIZE; ++i) {
		if (Ad[BASE_IDX + i] > tmax) {
			tmax = Ad[BASE_IDX + i];
			tpos = i;
		}
	}

	rmax[T_IDX] = tmax;
	maxpos[T_IDX] = tpos;
}

// ================================================================================================
// This launches on only a single thread on the GPU, but is kept there for fairness in testing
__global__ void _device_matmax_final_impl(float vals[], const float rmax[], const int maxpos[])
{
	float tmax = FLT_MIN;
	int tx = -1, ty = -1;

	for (int i = 0; i < MATRIX_SIZE; ++i) {
		if (rmax[i] > tmax) {
			tmax = rmax[i];
			tx = maxpos[i];
			ty = i;
		}
	}

	vals[0] = tmax;
	vals[1] = tx;
	vals[2] = ty;
}

// ================================================================================================
void device_matmax(float *val, int *x, int *y, const float Ad[])
{
	float *rmax;
	int *maxpos;
	float *vals;
	if (cudaMalloc((void**)&rmax, VECTOR_SIZE * sizeof(float))) Fatal("Cannot allocate device memory rmax\n");
	if (cudaMalloc((void**)&maxpos, VECTOR_SIZE * sizeof(int))) Fatal("Cannot allocate device memory maxpos\n");
	if (cudaMalloc((void**)&vals, 3 * sizeof(float))) Fatal("Cannot allocate device memory vals\n");

	dim3 threads(1, BLOCK_SIZE);
	dim3 grid(1, BLOCK_SIZE);
	_device_matmax_row_impl<<<grid,threads>>>(rmax, maxpos, Ad);
	if (cudaGetLastError()) Fatal("Matrix matmax_row failed\n");
	_device_matmax_final_impl<<<dim3(1, 1), dim3(1, 1)>>>(vals, rmax, maxpos);
	if (cudaGetLastError()) Fatal("Matrix matmax_final failed\n");

	float hostvals[3];
	if (cudaMemcpy(hostvals, vals, 3 * sizeof(float), cudaMemcpyDeviceToHost)) Fatal("Cannot copy vals from device to host\n");

	cudaFree(rmax);
	cudaFree(maxpos);
	cudaFree(vals);	

	*val = hostvals[0];
	*x = (int)hostvals[1];
	*y = (int)hostvals[2];
}




// ================================================================================================
void host_mattrans(float Bh[], const float Ah[])
{
	for(unsigned int i = 0 ; i < MATRIX_SIZE; i++) {
		for (unsigned int j = 0; j < MATRIX_SIZE; j++) {
			Bh[j * MATRIX_SIZE + i] = Ah[i * MATRIX_SIZE + j];
		}
	}
}

// ================================================================================================
__global__ void _device_mattrans_impl(float Bh[], const float Ah[])
{
	unsigned int j = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int i = blockIdx.y * blockDim.y + threadIdx.y;
	Bh[j * MATRIX_SIZE + i] = Ah[i * MATRIX_SIZE + j];
}

// ================================================================================================
void device_mattrans(float Bd[], const float Ad[])
{
	dim3 threads(BLOCK_SIZE, BLOCK_SIZE);
	dim3 grid(BLOCK_SIZE, BLOCK_SIZE);
	_device_mattrans_impl<<<grid, threads>>>(Bd, Ad);
	if (cudaGetLastError()) Fatal("Matrix transposition failed\n");
}




// ================================================================================================
void host_matnorm(float *val, const float Ah[])
{
	float sum = 0;
	for(unsigned int i = 0 ; i < MATRIX_SIZE; i++) {
		for (unsigned int j = 0; j < MATRIX_SIZE; j++) {
			float val2 = Ah[i * MATRIX_SIZE + j];
			sum += (val2 * val2);
		}
	}
	*val = sum;
}
// ================================================================================================
__global__ void _device_matnorm_row_impl(float rnorm[], const float Ad[])
{
	const unsigned int T_IDX = blockIdx.y * blockDim.y + threadIdx.y;

	float sum = 0;
	const int BASE_IDX = T_IDX * MATRIX_SIZE;
	for (int i = 0; i < MATRIX_SIZE; ++i) {
		float val2 = Ad[BASE_IDX + i];
		sum += (val2 * val2);
	}

	rnorm[T_IDX] = sum;
}

// ================================================================================================
// This launches on only a single thread on the GPU, but is kept there for fairness in testing
__global__ void _device_matnorm_final_impl(float *val, const float rnorm[])
{
	float sum = 0;
	for (int i = 0; i < MATRIX_SIZE; ++i) {
		sum += rnorm[i];
	}
	val[0] = sum;
}

// ================================================================================================
void device_matnorm(float *val, const float Ad[])
{
	float *rnorm;
	float *dval;
	if (cudaMalloc((void**)&rnorm, VECTOR_SIZE * sizeof(float))) Fatal("Cannot allocate device memory rnorm\n");
	if (cudaMalloc((void**)&dval, sizeof(float))) Fatal("Cannot allocate device memory val\n");

	dim3 threads(1, BLOCK_SIZE);
	dim3 grid(1, BLOCK_SIZE);
	_device_matnorm_row_impl<<<grid, threads>>>(rnorm, Ad);
	if (cudaGetLastError()) Fatal("Matrix matmax_row failed\n");
	_device_matnorm_final_impl<<<dim3(1, 1), dim3(1, 1)>>>(dval, rnorm);
	if (cudaGetLastError()) Fatal("Matrix matmax_final failed\n");

	if (cudaMemcpy(val, dval, sizeof(float), cudaMemcpyDeviceToHost)) Fatal("Cannot copy val from device to host\n");

	cudaFree(rnorm);
	cudaFree(dval);	
}