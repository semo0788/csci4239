## HW10

To run this program, simply run `make` in the directory. The resulting executable is called "hw10". It was derived from ex23.

This program offers a wide variety of matrix operation functions, listed below:
* Matrix Multiplication
* Matrix Minimum Value
* Matrix Maximum Value (Max norm)
* Matrix Transposition
* Matrix Norm Finding (Frobrenius norm)

Running the program itself times all of these functions on both the cpu and gpu, and finds the speedup. All matrices in the program are 1024x1024.

To more properly measure the timing difference, I also initialized global device matrices, instead of creating and destroying device memory for each test.

### Time
This assignment took me about 4 hours to complete.
