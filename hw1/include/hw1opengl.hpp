/*
 *	HW1 Header File for Sean Moss.
 */

#ifndef hw1opengl_hpp_
#define hw1opengl_hpp_

#include <QtOpenGL>
#include <QString>
#include <QVector>
#include <QTimer>
#include <QElapsedTimer>
#include "Object.h"

class HW1OpenGL :
	public QGLWidget
{
	Q_OBJECT

private:
	bool    light;     //  Lighting
	int     mode;      //  Display mode
	int     th,ph;     //  Display angles
	bool    mouse;     //  Mouse pressed
	QPoint  pos;       //  Mouse position
	int     fov;       //  Field of view
	double  dim;       //  Display size
	double  asp;       //  Sceen aspect ratio
	double  sw, sh;
	Object* obj;       //  Object
	QTimer           timer;   //  Timer for animations
	QElapsedTimer    time;    //  Track elapsed time
	QGLShaderProgram shaderVert;  // Vertex-only Shader
	QGLShaderProgram shaderFrag; // Fragment-only Shader
	QVector<Object*> objects; //  Objects

public:
	HW1OpenGL(QWidget *parent);

	QSize sizeHint() const override { return QSize(400, 400); }

public slots:
	void setShader(int on);                //  Slot to set shader
	void setPerspective(int on);           //  Slot to set projection type
	void setObject(int type);              //  Slot to set displayed object
	void setLighting(int on);              //  Slot to set lighting
signals:
	void angles(QString text);             //  Signal for view angles
protected:
	void initializeGL();                   //  Initialize widget
	void resizeGL(int width, int height);  //  Resize widget
	void paintGL();                        //  Draw widget
	void mousePressEvent(QMouseEvent*);    //  Mouse pressed
	void mouseReleaseEvent(QMouseEvent*);  //  Mouse released
	void mouseMoveEvent(QMouseEvent*);     //  Mouse moved
	void wheelEvent(QWheelEvent*);         //  Mouse wheel
private:
	void Fatal(QString message);            //  Error handler
	void Projection();                      //  Update projection
};

#endif // hw1opengl_hpp_