/*
 *	HW1 Header File for Sean Moss.
 */

#ifndef hw1view_hpp_
#define hw1view_hpp_

#include <QWidget>

class HW1View : 
	public QWidget
{
	Q_OBJECT
	
public:
	HW1View();

private:
	void prepareControls();
};

#endif // hw1view_hpp_