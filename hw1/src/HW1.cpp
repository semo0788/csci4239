/*
 *	HW1 Source File for Sean Moss.
 */

#include <QApplication>
#include "hw1view.hpp"

int main(int argc, char **argv)
{
	QApplication app(argc, argv);

	// Create and display the assignment widget
	HW1View view;
	view.show();

	return app.exec();
}