/*
 *	HW1 Source File for Sean Moss.
 */

#include <QApplication>
#include <QComboBox>
#include <QGridLayout>
#include <QLabel>
#include <QCheckBox>
#include <QPushButton>
#include "hw1view.hpp"
#include "hw1opengl.hpp"

// ================================================================================================
HW1View::HW1View()
{
	// Set the window title
	setWindowTitle(tr("Assignment 1: Sean Moss"));

	// Setup the non OpenGL controls in the window
	prepareControls();
}

// ================================================================================================
void HW1View::prepareControls()
{
	// Shader selection box
	QComboBox *shader = new QComboBox();
	shader->addItem("None");
	shader->addItem("NDC -> RGB (Vert Only)");
	shader->addItem("NDC -> RGB (Frag Only)");

	// Select projection type
	QComboBox *projection = new QComboBox();
	projection->addItem("Othogonal");
	projection->addItem("Perspective");

	// Select the 3D object being shown
	QComboBox *object = new QComboBox();
	object->addItem("Cube");
	object->addItem("Teapot");
	object->addItem("Suzanne");

	// Lighting checkbox
	//QCheckBox *lighting = new QCheckBox("");

	//  View angle
	QLabel *angles = new QLabel();

	//  Quit
	QPushButton *quit = new QPushButton("Quit");

	// The OpenGL view for this homework
	HW1OpenGL *ogl = new HW1OpenGL(nullptr);

	//  Set layout of child widgets
	QGridLayout *layout = new QGridLayout();
	layout->addWidget(ogl, 0, 0, 7, 1);
	layout->addWidget(new QLabel("Shader"), 0, 1);
	layout->addWidget(shader, 0, 2);
	layout->addWidget(new QLabel("Projection"), 1, 1);
	layout->addWidget(projection, 1, 2);
	layout->addWidget(new QLabel("Object"), 2, 1);
	layout->addWidget(object, 2, 2);
	//layout->addWidget(new QLabel("Lighting"), 3, 1);
	//layout->addWidget(lighting, 3, 2);
	layout->addWidget(new QLabel("Angles"), 4, 1);
	layout->addWidget(angles, 4, 2);
	layout->addWidget(quit, 6, 2);
	//  Manage resizing
	layout->setColumnStretch(0,100);
	layout->setColumnMinimumWidth(0, 100);
	layout->setRowStretch(5, 100);
	setLayout(layout);

	//  Connect valueChanged() signals to ogl
	connect(shader,SIGNAL(currentIndexChanged(int))     , ogl,SLOT(setShader(int)));
	connect(object,SIGNAL(currentIndexChanged(int))     , ogl,SLOT(setObject(int)));
	connect(projection,SIGNAL(currentIndexChanged(int)) , ogl,SLOT(setPerspective(int)));
   	//connect(lighting,SIGNAL(stateChanged(int))          , ogl,SLOT(setLighting(int)));
	//  Connect angles() signal to label
	connect(ogl,SIGNAL(angles(QString)) , angles,SLOT(setText(QString)));
	//  Connect quit() signal to qApp::quit()
	connect(quit, SIGNAL(pressed()) , qApp, SLOT(quit()));
}