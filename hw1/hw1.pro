HEADERS = include/hw1view.hpp \
          include/hw1opengl.hpp \
          include/Cube.h \
          include/Object.h \
          include/Sphere.h \
          include/Teapot.h \
          include/Type.h \
          include/WaveOBJ.h
SOURCES = src/HW1.cpp \
          src/hw1view.cpp \
          src/hw1opengl.cpp \
          src/Cube.cpp \
          src/Object.cpp \
          src/Sphere.cpp \
          src/Teapot.cpp \
          src/WaveOBJ.cpp
QT += opengl
RESOURCES = res/hw1.qrc
INCLUDEPATH += $$PWD/include
QMAKE_CXXFLAGS += -std=c++11