// Secondary implementation of the NDC -> RGB shader, using only a fragment shader

void main()
{
    gl_FragColor = gl_FragCoord / gl_FragCoord.w;
}