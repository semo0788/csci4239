// Secondary implementation of the NDC -> RGB shader, using only a fragment shader

uniform vec2 screenSize;

void main()
{
    gl_FragColor = vec4(((gl_FragCoord.xyz / vec3(screenSize, 1.0)) + 1.0) / 2.0, 1.0);
}