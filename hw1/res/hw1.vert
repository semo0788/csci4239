// Primary implementation of the NDC -> RGB shader, using only the vertex shader

void main()
{
    // Translate to the output coordinate
    gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;

    // Simply map [-1, 1] to [0, 1] by adding 1 to the value, and then dividing by 2
    // Need to divide by the w component to ensure [-1, 1] as starting range
    gl_FrontColor = (vec4(gl_Position.xyz / gl_Position.w, 1.0) + 1.0) / 2.0;
}