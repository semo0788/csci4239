## HW1

To run this program, simply run `qmake` and then `make` in the directory. The resulting executable is called "hw1".

The interface is adapted from the ex01 code, but changed to only demonstrate the NDC -> RGB shaders, which means that lighting was removed.

The fragment only shader produces the correct colors, but they are washed out. I was unable to fix this.

#### Time
This assignment took me about 4 hours to complete, including the environment setup, adapting code from ex01, and writing the new shaders.

*Disclaimer: this homework made heavy use of the helper classes provided by Willem in the "ex01" code set. That code belongs to him.*