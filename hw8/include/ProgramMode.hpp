//
// Base class for the various modes that the program can be in
//

#ifndef PROGRAMMODE_HPP_
#define PROGRAMMODE_HPP_

#include <QObject>
#include <QLayout>
#include <QGLWidget>
#include <QGLShaderProgram>
#include <QMouseEvent>
#include <QWheelEvent>
#include "BaseOpengl.hpp"

class ProgramMode :
	public QObject
{
	Q_OBJECT

private:
	QString m_name;

public:
	ProgramMode(QString name);

	virtual QLayout* initialize() = 0; // Called when initializing the state, return the populated layout
	virtual void initializeGL() = 0; // Initialize the opengl content
	virtual void update(float dt) = 0; // Called each frame to update internals
	virtual void preRender(BaseOpenGL *gl) = 0; // Called to allow preparation of the gl state before drawing
	virtual void render(BaseOpenGL *gl) = 0; // Called each frame to do rendering
	virtual void shutdown() = 0; // Called when the mode is no longer active

	virtual void mousePressEvent(QMouseEvent*) { }
	virtual void mouseReleaseEvent(QMouseEvent*) { }
	virtual void mouseMoveEvent(QMouseEvent*) { }
	virtual void wheelEvent(QWheelEvent*) { }

protected:
	static QGLShaderProgram* LoadShader(QString vert, QString frag);
	static unsigned int LoadTexture(QString file);
	static unsigned int LoadImage(const QImage& img);

	static void Fatal(QString message);
};

#endif // PROGRAMMODE_HPP_