//
// Mode for demonstrating a heightmap (with noise)
//

#ifndef HEIGHTMAPNOISE_HPP_
#define HEIGHTMAPNOISE_HPP_

#include "ProgramMode.hpp"
#include <QImage>
#include <QLabel>
#include <QLineEdit>

class HeightmapNoise :
	public ProgramMode
{
	Q_OBJECT

private:
	static const int TEX_SIZE = 512;
	static const QImage::Format TEX_FMT = QImage::Format_RGBA8888;

	QImage *m_noiseImage;
	unsigned int m_noiseTexHandle;
	struct {
		unsigned int seed;
		int octaves;
		double freq;
	} m_noiseSettings;
	QGLShaderProgram *m_shader;

	QLabel *m_freqLabel;
	QLabel *m_octaveLabel;
	QLineEdit *m_seedLineEdit;

	float m_distance;
	bool m_mouse;
	QPoint m_mousePos;
	int m_ph;
	int m_th;

public:
	HeightmapNoise();

	virtual QLayout* initialize() override;
	virtual void initializeGL() override;
	virtual void update(float dt) override;
	virtual void preRender(BaseOpenGL *gl) override;
	virtual void render(BaseOpenGL *gl) override;
	virtual void shutdown() override;

	virtual void mousePressEvent(QMouseEvent*) override;
	virtual void mouseReleaseEvent(QMouseEvent*) override;
	virtual void mouseMoveEvent(QMouseEvent*) override;
	virtual void wheelEvent(QWheelEvent*) override;

	void updateNoiseImageGL();

private slots:
	void generateNoise();
	void setFreq(int f);
	void setOct(int o);
};

#endif // HEIGHTMAPNOISE_HPP_