
uniform sampler2D NoiseImage;

void main()
{
	vec4 imc = texture2D(NoiseImage, gl_TexCoord[0].xy);

	gl_FragColor = vec4(imc.rgb, 1.0);
}