
uniform sampler2D NoiseImage;

void main()
{
	// Calculate the offset value (map from [0.0, 1.0] to [-0.25, 0.25])
	// First map the vertexes from [-1.0, 1.0] to [0.0, 1.0]
	vec2 mapcoord = gl_Vertex.xz * 0.5 + 0.5;
	float height = (texture2D(NoiseImage, mapcoord).a * 0.5) - 0.25;

	gl_TexCoord[0] = gl_MultiTexCoord0;
	gl_Position = gl_ModelViewProjectionMatrix * (gl_Vertex + vec4(0.0, height, 0.0, 0.0));
}