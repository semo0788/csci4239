//
// Base opewngl widget for the window, modes draw into this object
//

#include "BaseOpengl.hpp"
#include "ProgramMode.hpp"

// ================================================================================================
BaseOpenGL::BaseOpenGL() :
	QGLWidget(static_cast<QWidget*>(nullptr)),
	m_newMode(false),
	m_activeMode(nullptr),
	m_fov(0)
{
	m_frameTimer.setInterval(10); // 100 FPS
	connect(&m_frameTimer, SIGNAL(timeout()), this, SLOT(frame()));
	m_frameTimer.start();
	m_elapsedTimer.start();
}

// ================================================================================================
void BaseOpenGL::initializeGL()
{
	
}

// ================================================================================================
void BaseOpenGL::resizeGL(int, int)
{
	doProjection();
}

// ================================================================================================
void BaseOpenGL::paintGL()
{
	static float lastTime = 0.0f;
	float dt = getElapsedTime() - lastTime;

	if (m_activeMode) {

		if (m_newMode) {
			m_activeMode->initializeGL(); // Ensures an active GL context
			m_newMode = false;
		}

		glEnable(GL_TEXTURE_2D);
		glEnable(GL_DEPTH_TEST);
		
		m_activeMode->preRender(this);

		doProjection();

		m_activeMode->update(dt);
		m_activeMode->render(this);
	}

	lastTime += dt;
}

// ================================================================================================
void BaseOpenGL::doView()
{

}

// ================================================================================================
void BaseOpenGL::doProjection()
{
	//  Window aspect ration
	float asp = width() / (float)height();
	//  Viewport is whole screen
	glViewport(0, 0, width(), height());
	//  Set Projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (m_fov != 0.0) {
		double zmin = 1.0/16;
		double zmax = 16;
		double ydim = zmin*tan(m_fov*M_PI/360);
		double xdim = ydim*asp;
		glFrustum(-xdim,+xdim,-ydim,+ydim,zmin,zmax);
	}
	else
		glOrtho(-1*asp, +1*asp, -1, +1, -1, +1);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

// ================================================================================================
void BaseOpenGL::setActiveMode(ProgramMode *mode)
{
	m_activeMode = mode;
	m_newMode = true;
}

// ================================================================================================
void BaseOpenGL::mousePressEvent(QMouseEvent *event)
{
	if (m_activeMode)
		m_activeMode->mousePressEvent(event);
}

// ================================================================================================
void BaseOpenGL::mouseReleaseEvent(QMouseEvent *event)
{
	if (m_activeMode)
		m_activeMode->mouseReleaseEvent(event);
}

// ================================================================================================
void BaseOpenGL::mouseMoveEvent(QMouseEvent *event)
{
	if (m_activeMode)
		m_activeMode->mouseMoveEvent(event);
}

// ================================================================================================
void BaseOpenGL::wheelEvent(QWheelEvent *event)
{
	if (m_activeMode)
		m_activeMode->wheelEvent(event);
}

// ================================================================================================
void BaseOpenGL::frame()
{
	doProjection();
	update();
}