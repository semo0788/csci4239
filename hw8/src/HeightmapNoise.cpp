//
// Mode for demonstrating a heightmap (with noise)
//

#include "HeightmapNoise.hpp"
#include "PerlinNoise.hpp"
#include <QFormLayout>
#include <QSlider>
#include <QPushButton>
#include <QPalette>
#include <iostream>

// ================================================================================================
HeightmapNoise::HeightmapNoise() :
	ProgramMode("HeightmapNoise"),
	m_noiseImage(nullptr),
	m_noiseTexHandle(0),
	m_noiseSettings{0, 1, 1},
	m_shader(nullptr),
	m_freqLabel(nullptr),
	m_octaveLabel(nullptr),
	m_seedLineEdit(nullptr),
	m_distance(4),
	m_mouse(false),
	m_mousePos{},
	m_ph(45),
	m_th(45)
{

}

// ================================================================================================
QLayout* HeightmapNoise::initialize()
{
	// Frequency slider
	m_freqLabel = new QLabel("Frequency (1):");
	QSlider *freq = new QSlider(Qt::Orientation::Horizontal);
	freq->setSingleStep(1);
	freq->setMinimum(1);
	freq->setMaximum(8);
	freq->setValue(1);

	// Octaves slider
	m_octaveLabel = new QLabel("Octaves (1):");
	QSlider *octave = new QSlider(Qt::Orientation::Horizontal);
	octave->setSingleStep(1);
	octave->setMinimum(1);
	octave->setMaximum(8);
	octave->setValue(1);

	// Seed entry line
	m_seedLineEdit = new QLineEdit("0");

	// Generate button
	QPushButton *generate = new QPushButton("Generate Noise");

	// Setup layout
	QFormLayout *layout = new QFormLayout;
	layout->addRow(m_freqLabel, freq);
	layout->addRow(m_octaveLabel, octave);
	layout->addRow("Seed", m_seedLineEdit);
	layout->addRow(generate);

	// Attach slots
	connect(freq, SIGNAL(valueChanged(int)), this, SLOT(setFreq(int)));
	connect(octave, SIGNAL(valueChanged(int)), this, SLOT(setOct(int)));
	connect(generate, SIGNAL(pressed()), this, SLOT(generateNoise()));

	return layout;
}

// ================================================================================================
void HeightmapNoise::initializeGL()
{
	m_noiseImage = new QImage(TEX_SIZE, TEX_SIZE, TEX_FMT);
	m_noiseImage->fill(Qt::GlobalColor::black);

	glActiveTexture(GL_TEXTURE0);
	m_noiseTexHandle = LoadImage(*m_noiseImage);

	m_shader = LoadShader(":/heightmap.vert", ":/heightmap.frag");
}

// ================================================================================================
void HeightmapNoise::update(float dt)
{

}

// ================================================================================================
void HeightmapNoise::preRender(BaseOpenGL *gl)
{
	gl->setFov(45);
}

// ================================================================================================
void HeightmapNoise::render(BaseOpenGL *gl)
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslated(0, 0, -m_distance);
	glRotated(m_ph, 1, 0, 0);
	glRotated(m_th, 0, 1, 0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_noiseTexHandle);
	
	m_shader->bind();
	m_shader->setUniformValue("NoiseImage", 0);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glBegin(GL_QUADS);
	for (int x = 0; x < TEX_SIZE - 1; ++x) {
		for (int y = 0; y < TEX_SIZE - 1; ++y) {
			int xp1 = x + 1;
			int yp1 = y + 1;

			glTexCoord2f(x / float(TEX_SIZE), y / float(TEX_SIZE));
			glVertex3f(-1 + x * 2 / float(TEX_SIZE), 0, -1 + y * 2 / float(TEX_SIZE));
			glTexCoord2f(xp1 / float(TEX_SIZE), y / float(TEX_SIZE));
			glVertex3f(-1 + xp1 * 2 / float(TEX_SIZE), 0, -1 + y * 2 / float(TEX_SIZE));
			glTexCoord2f(xp1 / float(TEX_SIZE), yp1 / float(TEX_SIZE));
			glVertex3f(-1 + xp1 * 2 / float(TEX_SIZE), 0, -1 + yp1 * 2 / float(TEX_SIZE));
			glTexCoord2f(x / float(TEX_SIZE), yp1 / float(TEX_SIZE));
			glVertex3f(-1 + x * 2 / float(TEX_SIZE), 0, -1 + yp1 * 2 / float(TEX_SIZE));
		}
	}
	glEnd();

	m_shader->release();
}

// ================================================================================================
void HeightmapNoise::shutdown()
{

}

// ================================================================================================
void HeightmapNoise::mousePressEvent(QMouseEvent *event)
{
	m_mousePos = event->pos();
	m_mouse = true;
}

// ================================================================================================
void HeightmapNoise::mouseReleaseEvent(QMouseEvent *event)
{
	m_mouse = false;
}

// ================================================================================================
void HeightmapNoise::mouseMoveEvent(QMouseEvent *event)
{
	if (m_mouse)
	{
		QPoint d = event->pos() - m_mousePos;
		m_th = (m_th + d.x()) % 360;
		m_ph = (m_ph + d.y()) % 360;
		m_ph = (m_ph < -80) ? -80 : (m_ph > 80) ? 80 : m_ph;
		m_mousePos = event->pos();
	}
}

// ================================================================================================
void HeightmapNoise::wheelEvent(QWheelEvent *event)
{
	if (event->delta() < 0)
		m_distance += 0.1;
	else if (event->delta() > 0)
		m_distance -= 0.1;
	
	m_distance = (m_distance > 10) ? 10 : (m_distance < 2) ? 2 : m_distance;
}

// ================================================================================================
void HeightmapNoise::updateNoiseImageGL()
{
	glBindTexture(GL_TEXTURE_2D, m_noiseTexHandle);
	QImage rgba = QGLWidget::convertToGLFormat(*m_noiseImage);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, rgba.width(), rgba.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, rgba.bits());
}

// ================================================================================================
void HeightmapNoise::generateNoise()
{
	// Validate seed entry
	bool valid;
	unsigned int seed = static_cast<unsigned int>(m_seedLineEdit->text().toInt(&valid));
	if (!valid) {
		m_seedLineEdit->setStyleSheet("color: red");
		return;
	}
	else {
		m_seedLineEdit->setStyleSheet("");
		m_noiseSettings.seed = seed;
	}

	const double fc = TEX_SIZE / m_noiseSettings.freq;
	siv::PerlinNoise rperlin(m_noiseSettings.seed);
	siv::PerlinNoise gperlin(m_noiseSettings.seed >> 4);
	siv::PerlinNoise bperlin(m_noiseSettings.seed >> 8);
	siv::PerlinNoise aperlin(m_noiseSettings.seed >> 12);

	for (int x = 0; x < TEX_SIZE; ++x) {
		for (int y = 0; y < TEX_SIZE; ++y) {
			const QColor color(
				static_cast<unsigned char>(rperlin.octaveNoise0_1(x / fc / 2, y / fc / 2, m_noiseSettings.octaves) * 255),
				static_cast<unsigned char>(gperlin.octaveNoise0_1(x / fc / 4, y / fc / 4, m_noiseSettings.octaves) * 255),
				static_cast<unsigned char>(bperlin.octaveNoise0_1(x / fc / 8, y / fc / 8, m_noiseSettings.octaves) * 255),
				static_cast<unsigned char>(aperlin.octaveNoise0_1(x / fc, y / fc, m_noiseSettings.octaves) * 255)
			);
			m_noiseImage->setPixel(x, y, color.rgba());
		}
	}

	glActiveTexture(GL_TEXTURE0);
	updateNoiseImageGL();
	m_noiseImage->save("./noise.png");
}

// ================================================================================================
void HeightmapNoise::setFreq(int f)
{
	m_freqLabel->setText(QString::asprintf("Frequency (%d)", f));
	m_noiseSettings.freq = f;
}

// ================================================================================================
void HeightmapNoise::setOct(int o)
{
	m_octaveLabel->setText(QString::asprintf("Octaves (%d)", o));
	m_noiseSettings.octaves = o;
}