//
// Main file for Sean Moss HW8
//

#include <QApplication>
#include "BaseViewer.hpp"

//
//  Main function
//
int main(int argc, char *argv[])
{
   //  Create the application
   QApplication app(argc,argv);
   //  Create and show view widget
   BaseViewer view;
   view.setMode(0);
   view.show();
   //  Main loop for application
   return app.exec();
}
