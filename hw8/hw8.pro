HEADERS = include/BaseViewer.hpp \
		  include/BaseOpengl.hpp \
		  include/ProgramMode.hpp \
		  include/HeightmapNoise.hpp \
		  include/PerlinNoise.hpp
SOURCES = src/hw8.cpp \
		  src/BaseViewer.cpp \
		  src/BaseOpengl.cpp \
		  src/ProgramMode.cpp \
		  src/HeightmapNoise.cpp
QT += opengl
RESOURCES = res/hw8.qrc
INCLUDEPATH += $$PWD/include
QMAKE_CXXFLAGS += -std=c++11