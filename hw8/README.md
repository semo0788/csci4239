## HW8

To run this program, simply run `qmake` and then `make` in the directory. The resulting executable is called "hw8".

The header file PerlinNoise.hpp was taken from Github, and can be found [here](https://github.com/Reputeless/PerlinNoise).

This program uses a perlin noise texture to generate both mesh color and heightmaps. The RGB components of the texture were the color, and the A component
was the height. The height of the mesh was adjusted in the vertex shader at render time, instead of beforehand on the CPU.

### Time
This assignment took me about 7 hours to complete, mostly devoted to fixing bugs I created when coding late at night.
