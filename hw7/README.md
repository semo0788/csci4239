## HW4

To run this program, simply run `qmake` and then `make` in the directory. The resulting executable is called "hw7".

Most of the code is mine, except for a few small snippets that were taken from examples 11 through 13.

### Time
This assignment took me about 13 hours to complete, including writing my own interface, editing the images used, fixing shaders, and making the presentation.

The presentation is not included in the repo.
