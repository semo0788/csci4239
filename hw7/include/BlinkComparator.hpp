//
// Mode for demonstrating a blink comparator
//

#ifndef BLINKCOMPARATOR_HPP_
#define BLINKCOMPARATOR_HPP_

#include "ProgramMode.hpp"
#include <QCheckBox>
#include <QSlider>
#include <QLabel>

class BlinkComparator :
	public ProgramMode
{
	Q_OBJECT

private:
	float m_elapsed;
	unsigned int m_pluto1;
	unsigned int m_pluto2;
	QGLShaderProgram *m_shader;
	int m_mode;
	bool m_paused;
	float m_threshold;

	QCheckBox *m_pauseBlink;
	QSlider *m_threshSlider;
	QLabel *m_threshLabel;
	QLabel *m_slideLabel;

public:
	BlinkComparator();

	virtual QLayout* initialize() override;
	virtual void initializeGL() override;
	virtual void update(float dt) override;
	virtual void render(QGLWidget *gl) override;
	virtual void shutdown() override;

private slots:
	void setMode(int mode);
	void setPaused(int pause);
	void setThresh(int amt);
};

#endif // BLINKCOMPARATOR_HPP_