//
// Base opewngl widget for the window, modes draw into this object
//

#ifndef BASEOPENGL_HPP_
#define BASEOPENGL_HPP_

#include <QGLWidget>
#include <QTimer>
#include <QElapsedTimer>
#include "ProgramMode.hpp"

class BaseOpenGL :
	public QGLWidget
{
	Q_OBJECT

private:
	bool m_newMode;
	ProgramMode *m_activeMode;
	QTimer m_frameTimer;
	QElapsedTimer m_elapsedTimer;

public:
	BaseOpenGL();

	QSize sizeHint() const { return QSize(600, 600); }
	float getElapsedTime() const { return 0.001f * m_elapsedTimer.elapsed(); }

	void initializeGL() override;
	void resizeGL(int, int) override;
	void paintGL() override;
	void doView();
	void doProjection();

	void setActiveMode(ProgramMode *mode);

private slots:
	void frame();
};

#endif // BASEOPENGL_HPP_