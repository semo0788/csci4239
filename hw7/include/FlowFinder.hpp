//
// Mode for demonstrating image comparison
//

#ifndef FLOWFINDER_HPP_
#define FLOWFINDER_HPP_

#include "ProgramMode.hpp"
#include <QCheckBox>
#include <QSlider>
#include <QLabel>

class FlowFinder :
	public ProgramMode
{
	Q_OBJECT

private:
	float m_elapsed;
	unsigned int m_marsBefore;
	unsigned int m_marsAfter;
	QGLShaderProgram *m_shader;
	int m_mode;
	bool m_paused;
	float m_threshold;

	QCheckBox *m_pauseBlink;
	QSlider *m_threshSlider;
	QLabel *m_threshLabel;
	QLabel *m_slideLabel;

public:
	FlowFinder();

	virtual QLayout* initialize() override;
	virtual void initializeGL() override;
	virtual void update(float dt) override;
	virtual void render(QGLWidget *gl) override;
	virtual void shutdown() override;

private slots:
	void setMode(int mode);
	void setPaused(int pause);
	void setThresh(int amt);
};

#endif // FLOWFINDER_HPP_