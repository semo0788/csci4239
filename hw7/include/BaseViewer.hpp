//
// Base viewer for the window, manages the other views that appear
//

#ifndef BASEVIEWER_HPP_
#define BASEVIEWER_HPP_

#include <QWidget>
#include <QGroupBox>
#include "BaseOpengl.hpp"
#include "ProgramMode.hpp"

class BaseViewer :
	public QWidget
{
	Q_OBJECT

private:
	int m_mode;
	BaseOpenGL *m_gl;
	QGroupBox *m_controls;
	ProgramMode *m_activeMode;

public:
	BaseViewer();

private:
	void Fatal(QString message);

public slots:
	void setMode(int mode);
};

#endif // BASEVIEWER_HPP_