//
// Base class for the various modes that the program can be in
//

#ifndef PROGRAMMODE_HPP_
#define PROGRAMMODE_HPP_

#include <QObject>
#include <QLayout>
#include <QGLWidget>
#include <QGLShaderProgram>

class ProgramMode :
	public QObject
{
	Q_OBJECT

private:
	QString m_name;

public:
	ProgramMode(QString name);

	virtual QLayout* initialize() = 0; // Called when initializing the state, return the populated layout
	virtual void initializeGL() = 0; // Initialize the opengl content
	virtual void update(float dt) = 0; // Called each frame to update internals
	virtual void render(QGLWidget *gl) = 0; // Called each frame to do rendering
	virtual void shutdown() = 0; // Called when the mode is no longer active

protected:
	static QGLShaderProgram* LoadShader(QString vert, QString frag);
	static unsigned int LoadTexture(QString file);

	static void Fatal(QString message);
};

#endif // PROGRAMMODE_HPP_