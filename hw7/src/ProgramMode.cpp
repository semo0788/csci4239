//
// Base class for the various modes that the program can be in
//

#include "ProgramMode.hpp"
#include <QMessageBox>
#include <QApplication>

// ================================================================================================
ProgramMode::ProgramMode(QString name) :
	m_name(name)
{
	
}

// ================================================================================================
/* static */ QGLShaderProgram* ProgramMode::LoadShader(QString vert, QString frag)
{
	QGLShaderProgram* prog = new QGLShaderProgram;

	//  Vertex shader
	if (vert.length() && !prog->addShaderFromSourceFile(QGLShader::Vertex,vert))
		Fatal("Error compiling "+vert+"\n"+prog->log());
	//  Fragment shader
	if (frag.length() && !prog->addShaderFromSourceFile(QGLShader::Fragment,frag))
		Fatal("Error compiling "+frag+"\n"+prog->log());
	//  Link
	if (!prog->link())
		Fatal("Error linking shader\n"+prog->log());
	
	return prog;
}

// ================================================================================================
/* static */ unsigned int ProgramMode::LoadTexture(QString file)
{
	//  Load image
	QImage img(file);
	//  Bind texture
	unsigned int tex;
	glGenTextures(1,&tex);
	glBindTexture(GL_TEXTURE_2D,tex);
	//  Copy image to texture
	QImage rgba = QGLWidget::convertToGLFormat(img);
	glTexImage2D(GL_TEXTURE_2D,0,4,rgba.width(),rgba.height(),0,GL_RGBA,GL_UNSIGNED_BYTE,rgba.bits());
	//  Set pixel interpolation
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	return tex;
}

// ================================================================================================
/* static */ void ProgramMode::Fatal(QString message)
{
	QMessageBox::critical(nullptr, "ProgramMode", message);
	QApplication::quit();
}