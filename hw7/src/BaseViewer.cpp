//
// Base viewer for the window, manages the other views that appear
//

#include "BaseViewer.hpp"
#include "BlinkComparator.hpp"
#include "FlowFinder.hpp"
#include <QComboBox>
#include <QLabel>
#include <QGridLayout>
#include <QApplication>
#include <QPushButton>
#include <QMessageBox>
#include <QFrame>

// ================================================================================================
BaseViewer::BaseViewer() :
	m_mode(0),
	m_gl(nullptr),
	m_controls(nullptr),
	m_activeMode(nullptr)
{
	setWindowTitle(tr("Assignment 7 - Sean Moss"));

	// Combo Box for selecting mode
	QComboBox *modeBox = new QComboBox;
	modeBox->addItem("Blink Comparator");
	modeBox->addItem("Flow Finder");

	// Quit Button
	QPushButton *quit = new QPushButton("Quit");

	// Location that will contain the opengl scene
	m_gl = new BaseOpenGL;

	// Location that will contain the mode specific controls
	m_controls = new QGroupBox(tr("Mode Controls"));

	// Separator bar
	QFrame *hbar = new QFrame;
	hbar->setFrameShape(QFrame::HLine);

	// Set up the grid layout
	QGridLayout* layout = new QGridLayout;
	layout->addWidget(m_gl, 0, 0, 8, 1);
	layout->addWidget(new QLabel("Mode Select"), 0, 1, 1, 2);
	layout->addWidget(modeBox, 1, 1, 1, 2);
	layout->addWidget(hbar, 2, 1, 1, 2);
	layout->addWidget(m_controls, 3, 1, 1, 2);
	layout->addWidget(quit, 5, 2, 1, 1);
	layout->setColumnStretch(0, 100);
	layout->setColumnMinimumWidth(0, 200);
	layout->setRowStretch(4, 100);
	setLayout(layout);

	// Connect
	connect(modeBox, SIGNAL(currentIndexChanged(int)), this, SLOT(setMode(int)));
	connect(quit, SIGNAL(pressed()), qApp, SLOT(quit()));
}

// ================================================================================================
void BaseViewer::setMode(int mode)
{
	if (m_activeMode && mode == m_mode)
		return;

	if (m_activeMode) {
		m_activeMode->shutdown();
		delete m_activeMode;
	}

	switch (mode) {
		case 0: {
			m_activeMode = new BlinkComparator();
			break;
		}
		case 1: {
			m_activeMode = new FlowFinder();
			break;
		}
		default: Fatal("The value given for the mode was not understood.");
	}

	m_mode = mode;
	QLayout* layout = m_activeMode->initialize();
	delete m_controls->layout();
	m_controls->setLayout(layout);
	m_gl->setActiveMode(m_activeMode);
}

// ================================================================================================
void BaseViewer::Fatal(QString message)
{
	QMessageBox::critical(this, "BaseViewer", message);
	QApplication::quit();
}