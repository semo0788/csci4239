//
// Mode for demonstrating a blink comparator
//

#include "BlinkComparator.hpp"
#include <QFormLayout>
#include <QComboBox>

static float g_dx = 0.0f;
static float g_dy = 0.0f;

// ================================================================================================
BlinkComparator::BlinkComparator() :
	ProgramMode("BlinkComparator"),
	m_elapsed(0),
	m_pluto1(0),
	m_pluto2(0),
	m_mode(0),
	m_paused(false),
	m_threshold(0.95f),
	m_pauseBlink(nullptr),
	m_threshSlider(nullptr),
	m_threshLabel(nullptr)
{

}

// ================================================================================================
QLayout* BlinkComparator::initialize()
{
	// Mode box
	QComboBox *modeBox = new QComboBox;
	modeBox->addItem("Image 1 Only");
	modeBox->addItem("Image 2 Only");
	modeBox->addItem("Blink Images");
	modeBox->addItem("Fade Images");
	modeBox->addItem("Difference Frame");
	modeBox->addItem("Filtered Difference Frame");

	// Pause box
	m_pauseBlink = new QCheckBox("Pause Blinking");

	// Threshold slider
	m_threshSlider = new QSlider(Qt::Orientation::Horizontal);
	m_threshSlider->setSingleStep(1);
	m_threshSlider->setMinimum(0);
	m_threshSlider->setMaximum(100);
	m_threshSlider->setValue(95);

	// Slider label
	m_slideLabel = new QLabel("Similarity Limit:");

	// Threshold label
	m_threshLabel = new QLabel("0.95");

	// Prepare layout
	QFormLayout *layout = new QFormLayout;
	layout->addRow("Image Mode:", modeBox);
	layout->addRow(m_pauseBlink);
	layout->addRow(m_slideLabel, m_threshSlider);
	layout->addRow("", m_threshLabel);

	// Connect signals
	connect(modeBox, SIGNAL(currentIndexChanged(int)), this, SLOT(setMode(int)));
	connect(m_pauseBlink, SIGNAL(stateChanged(int)), this, SLOT(setPaused(int)));
	connect(m_threshSlider, SIGNAL(valueChanged(int)), this, SLOT(setThresh(int)));

	return layout;
}

// ================================================================================================
void BlinkComparator::initializeGL()
{
	glActiveTexture(GL_TEXTURE0);
	m_pluto1 = LoadTexture(":/pluto1small.png");
	glActiveTexture(GL_TEXTURE1);
	m_pluto2 = LoadTexture(":/pluto2small.png");

	QImage img(":/pluto1small.png");
	QImage rgba = QGLWidget::convertToGLFormat(img);
	g_dx = 1.0f / rgba.width();
	g_dy = 1.0f / rgba.height();

	m_shader = LoadShader("", ":/comparator.frag");

	setMode(0);
}

// ================================================================================================
void BlinkComparator::update(float dt)
{
	if (!m_paused) {
		m_elapsed += dt;
	}
}

// ================================================================================================
void BlinkComparator::render(QGLWidget *gl)
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	m_shader->bind();
	m_shader->setUniformValue("img0", 0);
	m_shader->setUniformValue("img1", 1);
	m_shader->setUniformValue("time", m_elapsed);
	m_shader->setUniformValue("mode", m_mode);
	m_shader->setUniformValue("thresh", m_threshold);
	m_shader->setUniformValue("dx", g_dx);
	m_shader->setUniformValue("dy", g_dy);

	glClear(GL_COLOR_BUFFER_BIT);
	glBegin(GL_QUADS);
	glTexCoord2f(0,0); glVertex2f(-1,-1);
	glTexCoord2f(1,0); glVertex2f(+1,-1);
	glTexCoord2f(1,1); glVertex2f(+1,+1);
	glTexCoord2f(0,1); glVertex2f(-1,+1);
	glEnd();

	m_shader->release();
}

// ================================================================================================
void BlinkComparator::shutdown()
{

}

// ================================================================================================
void BlinkComparator::setMode(int mode)
{
	m_mode = mode;

	if (mode == 2 || mode == 3) {
		m_pauseBlink->show();
	}
	else {
		m_pauseBlink->hide();
	}

	if (mode == 4 || mode == 5) {
		m_threshSlider->show();
		m_threshLabel->show();
		m_slideLabel->show();
	} else {
		m_threshSlider->hide();
		m_threshLabel->hide();
		m_slideLabel->hide();
	}
}

// ================================================================================================
void BlinkComparator::setPaused(int pause)
{
	m_paused = static_cast<bool>(pause);
}

// ================================================================================================
void BlinkComparator::setThresh(int amt)
{
	m_threshold = float(amt) / 100.0f;
	m_threshLabel->setText(QString::asprintf("%.2f", m_threshold));
}