//
// Base opewngl widget for the window, modes draw into this object
//

#include "BaseOpengl.hpp"

// ================================================================================================
BaseOpenGL::BaseOpenGL() :
	QGLWidget(static_cast<QWidget*>(nullptr)),
	m_newMode(false),
	m_activeMode(nullptr)
{
	m_frameTimer.setInterval(10); // 100 FPS
	connect(&m_frameTimer, SIGNAL(timeout()), this, SLOT(frame()));
	m_frameTimer.start();
	m_elapsedTimer.start();
}

// ================================================================================================
void BaseOpenGL::initializeGL()
{
	
}

// ================================================================================================
void BaseOpenGL::resizeGL(int, int)
{
	doProjection();
}

// ================================================================================================
void BaseOpenGL::paintGL()
{
	static float lastTime = 0.0f;
	float dt = getElapsedTime() - lastTime;

	if (m_activeMode) {

		if (m_newMode) {
			m_activeMode->initializeGL(); // Ensures an active GL context
			m_newMode = false;
		}

		m_activeMode->update(dt);
		m_activeMode->render(this);
	}

	lastTime += dt;
}

// ================================================================================================
void BaseOpenGL::doView()
{

}

// ================================================================================================
void BaseOpenGL::doProjection()
{
	//  Window aspect ration
	float asp = width() / (float)height();
	//  Viewport is whole screen
	glViewport(0, 0, width(), height());
	//  Set Projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-1*asp, +1*asp, -1, +1, -1, +1);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

// ================================================================================================
void BaseOpenGL::setActiveMode(ProgramMode *mode)
{
	m_activeMode = mode;
	m_newMode = true;
}

// ================================================================================================
void BaseOpenGL::frame()
{
	doProjection();
	update();
}