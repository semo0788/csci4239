HEADERS = include/BaseViewer.hpp \
		  include/BaseOpengl.hpp \
		  include/ProgramMode.hpp \
		  include/BlinkComparator.hpp \
		  include/FlowFinder.hpp
SOURCES = src/hw7.cpp \
		  src/BaseViewer.cpp \
		  src/BaseOpengl.cpp \
		  src/ProgramMode.cpp \
		  src/BlinkComparator.cpp \
		  src/FlowFinder.cpp
QT += opengl
RESOURCES = res/hw7.qrc
INCLUDEPATH += $$PWD/include
QMAKE_CXXFLAGS += -std=c++11