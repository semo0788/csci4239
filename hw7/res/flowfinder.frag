// Shader for emulating blink comparator
 
uniform sampler2D img0;
uniform sampler2D img1;
uniform float time;
uniform int mode;
uniform float thresh;
uniform float dx;
uniform float dy;

float sampleDiff(float delx, float dely)
{
	vec2 loc = gl_TexCoord[0].st + vec2(delx, dely);

	return length(texture2D(img0, loc)
				- texture2D(img1, loc));
}

void main()
{
	vec2 texcoord = gl_TexCoord[0].st;

	if (mode == 0) { // Image 1 only
		gl_FragColor = texture2D(img0, texcoord);
	}
	else if (mode == 1) { // Image 2 only
		gl_FragColor = texture2D(img1, texcoord);
	}
	else if (mode == 2) { // Blink Images
		vec4 c0 = texture2D(img0, texcoord);
		vec4 c1 = texture2D(img1, texcoord);

		float timef = floor(time * 2.0);
		if (mod(timef, 2.0) == 0.0) {
			gl_FragColor = c0;
		}
		else {
			gl_FragColor = c1;
		}
	}
	else if (mode == 3) { // Difference frame
		vec4 c0 = texture2D(img0, texcoord);
		vec4 c1 = texture2D(img1, texcoord);

		float diff = length(c0 - c1);

		if (diff > thresh) {
			gl_FragColor = vec4(diff, diff, diff, 1.0);
		}
		else {
			gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0);
		}
	}
	else {
		discard;
	}
}