// Shader for emulating blink comparator
 
uniform sampler2D img0;
uniform sampler2D img1;
uniform float time;
uniform int mode;
uniform float thresh;
uniform float dx;
uniform float dy;

float sampleDiff(float delx, float dely)
{
	vec2 loc = gl_TexCoord[0].st + vec2(delx, dely);

	return length(texture2D(img0, loc)
				- texture2D(img1, loc));
}

void main()
{
	vec2 texcoord = gl_TexCoord[0].st;

	if (mode == 0) { // Image 1 only
		gl_FragColor = texture2D(img0, texcoord);
	}
	else if (mode == 1) { // Image 2 only
		gl_FragColor = texture2D(img1, texcoord);
	}
	else if (mode == 2) { // Blink Images
		vec4 c0 = texture2D(img0, texcoord);
		vec4 c1 = texture2D(img1, texcoord);

		float timef = floor(time * 2.0);
		if (mod(timef, 2.0) == 0.0) {
			gl_FragColor = c0;
		}
		else {
			gl_FragColor = c1;
		}
	}
	else if (mode == 3) { // Fade Images
		vec4 c0 = texture2D(img0, texcoord);
		vec4 c1 = texture2D(img1, texcoord);

		gl_FragColor = mix(c0, c1, abs(sin(time)));
	}
	else if (mode == 4) { // Difference frame
		vec4 c0 = texture2D(img0, texcoord);
		vec4 c1 = texture2D(img1, texcoord);

		float diff = length(c0 - c1);

		if (diff > thresh) {
			gl_FragColor = vec4(diff, diff, diff, 1.0);
		}
		else {
			gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0);
		}
	}
	else if (mode == 5) { // Filtered Difference frame
		// float filter[9] = {
		// 	+1.0, +2.0, +1.0,
		// 	+2.0, +8.0, +2.0,
		// 	+1.0, +2.0, +1.0
		// };
		// float filterNorm = 20.0;
		// float filter[9] = {
		// 	+2.0, +3.0, +2.0,
		// 	+3.0, +1.0, +3.0,
		// 	+2.0, +3.0, +2.0
		// };
		// float filterNorm = 21.0;
		float filter[9] = {
			+2.0, +3.0, +2.0,
			+3.0, -8.0, +3.0,
			+2.0, +3.0, +2.0
		};
		float filterNorm = 12.0;

		float diffs[9] = {
			sampleDiff(-dx, +dy),
			sampleDiff(0.0, +dy),
			sampleDiff(+dx, +dy),
			sampleDiff(-dx, 0.0),
			sampleDiff(0.0, 0.0),
			sampleDiff(+dx, 0.0),
			sampleDiff(-dx, -dy),
			sampleDiff(0.0, -dy),
			sampleDiff(+dx, -dy)
		};

		float wDiff = filter[0] * diffs[0] + filter[1] * diffs[1] + filter[2] * diffs[2]
					+ filter[3] * diffs[3] + filter[4] * diffs[4] + filter[5] * diffs[5]
					+ filter[6] * diffs[6] + filter[7] * diffs[7] + filter[8] * diffs[8];
		wDiff /= filterNorm;
		
		if (wDiff > thresh) {
			gl_FragColor = vec4(wDiff, wDiff, wDiff, 1.0);
		}
		else {
			gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0);
		}
	}
	else {
		discard;
	}
}