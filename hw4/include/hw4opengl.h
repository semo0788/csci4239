// Opengl widget header file for hw4, adapted from ex06

#ifndef HW4OPENGL_H_
#define HW4OPENGL_H_

#include "CUgl.h"
#include <QMatrix4x4>

class HW4OpenGL : public CUgl
{
Q_OBJECT
private:
   unsigned int tex;       //  Textures
   QGLBuffer cube_buffer;  //  Vertex buffer
   QMatrix4x4       proj;  //  Projection matrix
public:
   HW4OpenGL(QWidget* parent=0);                  //  Constructor
   QSize sizeHint() const {return QSize(400,400);} //  Default size of widget
protected:
   void initializeGL();  //  Initialize widget
   void paintGL();       //  Draw widget
   void doProjection();  //  Projection
};

#endif // HW4OPENGL_H_
