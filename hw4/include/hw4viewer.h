// QT viewer header file for hw4, adapted from ex06

#ifndef HW4VIEWER_H_
#define HW4VIEWER_H_

#include <QWidget>
#include <QSlider>
#include <QDoubleSpinBox>
#include "hw4opengl.h"

class HW4Viewer : public QWidget
{
Q_OBJECT
private:
   QSlider*     Lpos;
   QSlider*     Zpos;
   QPushButton* light;
   HW4OpenGL*  ogl;
private slots:
   void lmove();        //  Light movement
public:
    HW4Viewer();
};

#endif // HW4VIEWER_H_
