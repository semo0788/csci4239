// Main file for hw4, taken from ex06 and adapted

#include <QApplication>
#include "hw4viewer.h"

//
//  Main function
//
int main(int argc, char *argv[])
{
   //  Create the application
   QApplication app(argc,argv);
   //  Create and show view widget
   HW4Viewer view;
   view.show();
   //  Main loop for application
   return app.exec();
}
