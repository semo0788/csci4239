HEADERS = include/hw4viewer.h \
		  include/hw4opengl.h \
		  include/CUgl.h \
		  include/Object.h \
		  include/Type.h
SOURCES = src/hw4.cpp \ 
          src/hw4viewer.cpp \
		  src/hw4opengl.cpp \
		  src/CUgl.cpp \
		  src/Object.cpp
QT += opengl
RESOURCES = res/hw4.qrc
INCLUDEPATH += $$PWD/include
QMAKE_CXXFLAGS += -std=c++11