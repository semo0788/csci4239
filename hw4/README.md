## HW4

To run this program, simply run `qmake` and then `make` in the directory. The resulting executable is called "hw4".

The interface is adapted from the ex06 code, as well as the code from previous homeworks.

### Time
This assignment took me about 2 hours to complete, including adapting the example code, and adding lighting and textures.

*Disclaimer: this homework made heavy use of the helper classes provided by Willem in the "ex06" code set. That code belongs to him.*