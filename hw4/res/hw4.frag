#version 400 core

//  Input from previous shader
in vec3 FrontColor;
in vec3 NormalOut;
in vec2 TexCoordOut;
in vec4 VertexOut;

// HW4 - texture sampler
uniform sampler2D Texture;

// HW4 - phong lighting uniforms
uniform vec3 Light;

//  Fragment color
layout (location = 0) out vec4 Fragcolor;

// HW4 - phong lighting function, taken from ex04 and adapted
//       The ambient, diffuse, and specular colors are hardcoded from the defaults in CUgl.cpp
vec4 phong()
{
   //  N is the object normal
   vec3 N = normalize(NormalOut);
   //  L is the light vector
   vec3 L = normalize(Light);

   //  Emission and ambient color
   vec4 color = vec4(0.3, 0.3, 0.3, 1.0);

   //  Diffuse light is cosine of light and normal vectors
   float Id = dot(L,N);
   if (Id>0.0)
   {
      //  Add diffuse
	  color += Id * vec4(1.0, 1.0, 1.0, 1.0);
      //  R is the reflected light vector R = 2(L.N)N - L
      vec3 R = reflect(-L,N);
      //  V is the view vector (eye vector)
      vec3 V = normalize(Light - VertexOut.xyz);
      //  Specular is cosine of reflected and view vectors
      float Is = dot(R,V);
	  if (Is>0.0) color += pow(Is,8)*vec4(1.0, 1.0, 1.0, 1.0);
   }

   //  Return sum of color components
   return color;
}

void main()
{
   vec4 outcolor = texture2D(Texture, TexCoordOut);
   outcolor *= phong();
   Fragcolor = outcolor * vec4(FrontColor, 1.0);
}
