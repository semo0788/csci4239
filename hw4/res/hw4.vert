#version 400 core

//  Transformation matrices
uniform mat4 ModelViewMatrix;
uniform mat4 ProjectionMatrix;

//  Vertex attributes (input)
layout (location = 0) in vec4 Vertex;
layout (location = 1) in vec3 Normal;
layout (location = 2) in vec3 Color;
layout (location = 3) in vec2 TexCoord;

//  Output to next shader
out vec3 FrontColor;
out vec3 NormalOut;
out vec2 TexCoordOut;
out vec4 VertexOut;

void main()
{	
   //  Pass colors to fragment shader (will be interpolated)
   FrontColor = Color;
   //  Set transformed vertex location
   VertexOut = ModelViewMatrix * Vertex;
   gl_Position =  ProjectionMatrix * ModelViewMatrix * Vertex;

   // HW4 - pass along to fragment shader
   NormalOut = Normal;
   TexCoordOut = TexCoord;
}
