HEADERS = include/config.hpp \
		  include/cuda_all.hpp \
		  include/gui/control_dockpanel.hpp \
		  include/gui/opengl_viewer.hpp \
		  include/gui/primary_menu.hpp \
		  include/gui/primary_window.hpp \
		  include/render/camera.hpp \
		  include/render/ogl_utils.hpp \
		  include/render/vertex_buffer.hpp \
		  include/render/vertex_format.hpp \
		  include/sim/memory_manager.hpp \
		  include/sim/nbody_solver.hpp \
		  include/sim/particle.hpp \
		  include/sim/sim_manager.hpp \
		  include/util/fast_mutex.h \
		  include/util/math.hpp \
		  include/util/tinythread.h

SOURCES = src/main.cpp \
		  src/cuda_all.cpp \
		  src/gui/control_dockpanel.cpp \
		  src/gui/opengl_viewer.cpp \
		  src/gui/primary_menu.cpp \
		  src/gui/primary_window.cpp \
		  src/render/camera.cpp \ 
		  src/render/ogl_utils.cpp \
		  src/render/vertex_buffer.cpp \
		  src/sim/memory_manager.cpp \
		  src/sim/nbody_solver.cpp \
		  src/sim/particle.cpp \
		  src/sim/sim_manager.cpp \
		  src/util/logging.cpp \
		  src/util/math.cpp \
		  src/util/tinythread.cpp

CUDA_SOURCES = src/sim/cuda_solvers.cu


QT += opengl widgets core
RESOURCES = res/final.qrc
INCLUDEPATH += $$PWD/include
LIBPATH += $$PWD/lib
LIBS += -lGLEW -lgomp -lOpenCL
QMAKE_CXXFLAGS += --std=c++11 -fopenmp
#QMAKE_LINK = nvcc


# auto-detect CUDA path
CUDA_DIR = $$system(which nvcc | sed 's,/bin/nvcc$,,')
INCLUDEPATH += $$CUDA_DIR/include
LIBPATH += $$CUDA_DIR/lib64
LIBS += -lcudart -lcuda
CUDA_INC = $$join(INCLUDEPATH,'" -I "','-I "','"') 
CUDA_CXXFLAGS = $$join(QMAKE_CXXFLAGS,",")

# Create CUDA compiler
cuda.input = CUDA_SOURCES
cuda.output = ${QMAKE_FILE_BASE}_cuda.o
cuda.commands = nvcc -c --std=c++11 $${CUDA_INC} ${QMAKE_FILE_NAME} -o ${QMAKE_FILE_OUT}
QMAKE_EXTRA_COMPILERS += cuda