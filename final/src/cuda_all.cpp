/**
 * Sean Moss (semo0788@colorado.edu)
 * Part of the final project for CSCI 4239, CU Boulder, Spring 2017.
 *
 * This file implements the code in "cuda_all.hpp"
 */

#include "cuda_all.hpp"

void cudaCheckError(cudaError_t code, const char* file, int line, bool abort)
{
	if (code != cudaSuccess) 
	{
		QtFatal(strfmt("CUDA error in file %s (%d): '%s'.", file, line, cudaGetErrorString(code)));
		if (abort) 
			exit(code);
	}
}