/**
 * Sean Moss (semo0788@colorado.edu)
 * Part of the final project for CSCI 4239, CU Boulder, Spring 2017.
 *
 * This file is the entry point for the program.
 */

#include "config.hpp"
#include "gui/primary_window.hpp"
#include "sim/memory_manager.hpp"
#include <QApplication>

int main(int argc, char **argv)
{
	QApplication app(argc, argv);

	PrimaryWindow window;
	//ParticleMemoryManager::InitializeCuda();
	window.show();

	return app.exec();
}