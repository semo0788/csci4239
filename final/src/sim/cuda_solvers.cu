/**
 * Sean Moss (semo0788@colorado.edu)
 * Part of the final project for CSCI 4239, CU Boulder, Spring 2017.
 *
 * This file implements the code found in sim/nbody_solver.hpp that is specific to the CUDA
 *     compiler.
 */

#include "sim/nbody_solver.hpp"
#include "sim/memory_manager.hpp"
#include <cuda_runtime_api.h>
#include <cuda.h>


// ================================================================================================
// __global__ void _leapfrog_solve(const float dt, const size_t count, Particle *src, Particle *dst)
// {
// 	const unsigned int INDEX = (blockIdx.x * blockDim.x) + threadIdx.x;

// 	Particle& srcp = src[INDEX];
// 	Particle& dstp = dst[INDEX];

// 	vec3 force;
// 	for (size_t j = 0; j < count; ++j) {
// 		if (INDEX == j)
// 			continue;
		
// 		Particle& opart = src[j];

// 		vec3 diff = (opart.pos - srcp.pos);
// 		float dist = diff.length() + 1; // With damping
// 		float scale = opart.m / (dist * dist * dist);
// 		force += (scale * diff);
// 	}

// 	dstp.m = srcp.m;
// 	dstp.acc = (force / srcp.m);
// 	dstp.vel = srcp.vel + (dstp.acc * dt);
// 	dstp.pos = srcp.pos + (dstp.vel * dt);
// }


// // ================================================================================================
// void cuda_leapfrog_solver::solveCuda()
// {
// 	const size_t nBlocks = 
// 			static_cast<size_t>((static_cast<float>(ParticleCount) / ParticleMemoryManager::CUDA_MAX_THREADS_PER_BLOCK)) + 1;
// 	dim3 threads(ParticleMemoryManager::CUDA_MAX_THREADS_PER_BLOCK, 1);
// 	dim3 grid(nBlocks, 1);
// 	_leapfrog_solve<<<grid, threads, 0, *m_deviceStream>>>(DeltaTime, ParticleCount, m_mappedSource, 
// 			m_mappedDestination);
// 	cudaDeviceSynchronize();
// }