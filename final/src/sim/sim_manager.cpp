/**
 * Sean Moss (semo0788@colorado.edu)
 * Part of the final project for CSCI 4239, CU Boulder, Spring 2017.
 *
 * This file implements the code found in the file sim/sim_manager.hpp.
 */

#include "sim/sim_manager.hpp"
#include "render/ogl_utils.hpp"


static const vertex_format_specifier_t AXES_VERTEX_FORMAT[2] = {
	{0, 3, GL_FLOAT, 7 * sizeof(GLfloat), 0}, // Position Data
	{1, 4, GL_FLOAT, 7 * sizeof(GLfloat), 3 * sizeof(GLfloat)}  // Color Data
};
static const float AXES_VERTEX_DATA[63] = {
	// x-axis
	0, 0, 0, 1, 0, 0, 0.5,
	1, 0, 0, 1, 0, 0, 0.5,
	0, 0, 0, 1, 0, 0, 0.5,
	// y-axis
	0, 0, 0, 0, 1, 0, 0.5,
	0, 1, 0, 0, 1, 0, 0.5,
	0, 0, 0, 0, 1, 0, 0.5,
	// z-axis
	0, 0, 0, 0, 0, 1, 0.5,
	0, 0, 1, 0, 0, 1, 0.5,
	0, 0, 0, 0, 0, 1, 0.5
};


// ================================================================================================
SimManager::SimManager() :
	m_axesBuffer{nullptr},
	m_axesShader{nullptr},
	m_particleShader{nullptr},
	m_vectorShader{nullptr},
	m_particleManager{nullptr},
	m_currentSimSize{ParticleMemoryManager::SIM_SIZE_SMALL},
	m_currentParticleCount{ParticleMemoryManager::SIM_PARTICLE_COUNTS[ParticleMemoryManager::SIM_SIZE_SMALL]},
	m_queuedSimSizeChange{nullptr},
	m_currentIntegType{0},
	m_currentIntegStyle{0},
	m_queuedIntegChange{nullptr},
	m_queuedRegen{nullptr},
	m_colorStyle{0},
	m_distType{0},
	m_paused{false},
	m_timestep{1e-3},
	m_showAxes{false},
	m_showVectors{false},
	m_meanVelocity{0},
	m_meanAcceleration{0},
	m_currentSolver{nullptr},
	m_currentThread{nullptr}
{
	m_upsTimer.start();
	m_meanValueTimer.start();
}

// ================================================================================================
SimManager::~SimManager()
{
	if (m_axesBuffer)
		delete m_axesBuffer;

	if (m_particleManager)
		delete m_particleManager;

	if (m_currentThread) {
		m_currentThread->detach();
		delete m_currentThread;
	}
}

// ================================================================================================
void SimManager::initializeGL()
{
	m_axesBuffer = new VertexBuffer(sizeof(AXES_VERTEX_DATA), GL_STATIC_DRAW);
	m_axesBuffer->setFormat(AXES_VERTEX_FORMAT, 2);
	m_axesBuffer->setData(AXES_VERTEX_DATA);

	m_axesShader = OGLUtils::LoadShader(":/shader/passthrough.vert", ":/shader/passthrough.frag");
	m_particleShader = OGLUtils::LoadShader(":/shader/particle.vert", ":/shader/particle.frag");
	m_vectorShader = OGLUtils::LoadGeomShader(":/shader/vector.vert", ":/shader/vector.geom", ":/shader/vector.frag");

	ParticleMemoryManager::InitializeGraphicsCompute();
	m_particleManager = new ParticleMemoryManager;

	// Create initial solver
	m_currentSolver = makeCurrentSolver();
	launchTimestep();
}

// ================================================================================================
void SimManager::setSimulationSize(size_t size)
{
	if (m_queuedSimSizeChange)
		delete m_queuedSimSizeChange;

	m_queuedSimSizeChange = new sim_size_change_queue_t { size };
}

// ================================================================================================
void SimManager::setIntegratorType(int type)
{
	int savedStyle = m_currentIntegStyle;
	if (m_queuedIntegChange) {
		savedStyle = m_queuedIntegChange->NewStyle;
		delete m_queuedIntegChange;
	}

	m_queuedIntegChange = new integ_change_queue_t { type, savedStyle };
}

// ================================================================================================
void SimManager::setIntegratorLocation(int loc)
{
	int savedType = m_currentIntegType;
	if (m_queuedIntegChange) {
		savedType = m_queuedIntegChange->NewType;
		delete m_queuedIntegChange;
	}

	m_queuedIntegChange = new integ_change_queue_t { savedType, loc };
}

// ================================================================================================
void SimManager::update()
{
	if (m_currentSolver && m_currentSolver->isDone()) { // A new timestep has finished, copy new particle data
		// Grab the elapsed time to calculate UPS, and emit event
		float elapsed = m_upsTimer.nsecsElapsed() / 1e9;
		emit updateUPS(1 / elapsed);

		m_currentSolver->postSolve();

		// Swap beforehand to get the correct array
		m_particleManager->swap(m_currentSimSize);

		// Copy the particle data to OpenGL for rendering (only if on CPU)
		if (m_currentIntegStyle <= 1)
			m_particleManager->triggerHostToDeviceCopy(m_currentSimSize);

		// Remove the current solver
		delete m_currentSolver;
		m_currentSolver = nullptr;

		size_t lastSimSize = m_currentSimSize;
		int lastIntegStyle = m_currentIntegStyle;

		// Do a queued sim size change, if necessary
		if (m_queuedSimSizeChange) {
			m_currentSimSize = m_queuedSimSizeChange->NewSize;
			m_currentParticleCount = ParticleMemoryManager::SIM_PARTICLE_COUNTS[m_currentSimSize];
			delete m_queuedSimSizeChange;
			m_queuedSimSizeChange = nullptr;
		}

		// Do a queued integrator change, if necessary
		if (m_queuedIntegChange) {
			m_currentIntegType = m_queuedIntegChange->NewType;
			m_currentIntegStyle = m_queuedIntegChange->NewStyle;
			delete m_queuedIntegChange;
			m_queuedIntegChange = nullptr;
		}

		// If there was a change, we need to make sure that any data copying gets done
		if ((lastSimSize != m_currentSimSize) || (lastIntegStyle != m_currentIntegStyle)) {
			bool lastSimOnCPU = (lastIntegStyle <= 1);
			bool currSimOnCPU = (m_currentIntegStyle <= 1);
			bool sizeChanged = (lastSimSize != m_currentSimSize);

			if (lastSimOnCPU && !currSimOnCPU) { // We have moved from CPU to GPU
				linfo(strfmt("CPU -> GPU (%d)", sizeChanged));
				if (sizeChanged) { // Need to copy the "new" size over
					m_particleManager->triggerHostToDeviceCopy(m_currentSimSize);
				}
				else { } // Data was already copied above, in order to render
				m_particleManager->acquireObjectsForSize(m_currentSimSize);
			}
			else if (!lastSimOnCPU && currSimOnCPU) { // We have moved from GPU to CPU
				linfo(strfmt("GPU -> CPU (%d)", sizeChanged));
				m_particleManager->releaseObjectsForSize(lastSimSize);
				// Whether or not there was a size change, this will copy the correct data over
				m_particleManager->triggerDeviceToHostCopy(lastSimSize);
			}
			else if (!lastSimOnCPU && !currSimOnCPU && sizeChanged) { // Just copy the new size over
				linfo("GPU -> GPU size change");
				m_particleManager->releaseObjectsForSize(lastSimSize);
				m_particleManager->triggerDeviceToHostCopy(lastSimSize);
				m_particleManager->triggerHostToDeviceCopy(m_currentSimSize);
				m_particleManager->acquireObjectsForSize(m_currentSimSize);
			}
		}
		else if (m_queuedRegen && m_queuedRegen->DoRegen) {
			m_distType = m_queuedRegen->NewType;
			m_particleManager->generateParticleArrayForSize(m_currentSimSize, m_distType);
			m_particleManager->triggerHostToDeviceCopy(m_currentSimSize);

			delete m_queuedRegen;
			m_queuedRegen = nullptr;
		}
		else if (m_meanValueTimer.elapsed() > 250) { // Only update every quarter second
			bool currSimOnCPU = (m_currentIntegStyle <= 1);
			if (currSimOnCPU) {
				Particle *src = m_particleManager->getHostSourceForSize(m_currentSimSize);
				updateMeanValues(src);
			}
			else {
				m_particleManager->releaseObjectsForSize(m_currentSimSize);
				VertexBuffer *srcbuf = m_particleManager->getSourceBufferForSize(m_currentSimSize);
				//Particle *src = static_cast<Particle*>(srcbuf->mapBufferRange(GL_READ_ONLY, 0, 50 * sizeof(Particle)));
				Particle *src = static_cast<Particle*>(srcbuf->mapBuffer(GL_READ_ONLY));
				updateMeanValues(src);
				srcbuf->unmapBuffer();
				m_particleManager->acquireObjectsForSize(m_currentSimSize);
			}
			m_meanValueTimer.restart();
		}
	}

	// Recreate a new solver for the new timestep, and launch it
	if (!m_paused && !m_currentSolver) {
		m_currentSolver = makeCurrentSolver();
		launchTimestep();
	}
}

// ================================================================================================
void SimManager::render(const Camera& camera)
{
	prepareRender();

	if (m_showAxes) {
		m_axesShader->bind();
		m_axesShader->setUniformValue("ViewMatrix", camera.getViewMatrix());
		m_axesShader->setUniformValue("ProjectionMatrix", camera.getProjectionMatrix());
		m_axesBuffer->drawBuffer(GL_LINES, 0, 8);
		m_axesShader->release();
	}

	m_particleShader->bind();
	m_particleShader->setUniformValue("ViewMatrix", camera.getViewMatrix());
	m_particleShader->setUniformValue("ProjectionMatrix", camera.getProjectionMatrix());
	m_particleShader->setUniformValue("ColorStyle", m_colorStyle);
	m_particleShader->setUniformValue("MeanVelocity", m_meanVelocity);
	m_particleShader->setUniformValue("MeanAcceleration", m_meanAcceleration);
	m_particleShader->setUniformValue("ParticleCount", (int)m_currentParticleCount);
	glPointSize(3.0f);
	m_particleManager->getSourceBufferForSize(m_currentSimSize)->drawBuffer(GL_POINTS, 0, m_currentParticleCount);
	m_particleShader->release();

	if (m_showVectors) {
		m_vectorShader->bind();
		m_vectorShader->setUniformValue("ViewMatrix", camera.getViewMatrix());
		m_vectorShader->setUniformValue("ProjectionMatrix", camera.getProjectionMatrix());
		m_vectorShader->setUniformValue("TotalParticles", static_cast<int>(m_currentSimSize));
		m_particleManager->getSourceBufferForSize(m_currentSimSize)->drawBuffer(GL_POINTS, 0, 50);
		m_vectorShader->release();
	}

	postRender();
}

// ================================================================================================
void SimManager::setDistType(int type)
{
	if (m_queuedRegen) {
		if (m_queuedRegen->DoRegen)
			return;
		else
			m_queuedRegen->NewType = type;
	}
	else {
		m_queuedRegen = new dist_regen_queue_t;
		m_queuedRegen->NewType = type;
		m_queuedRegen->DoRegen = false;
	}
}

// ================================================================================================
void SimManager::regenDist()
{
	if (m_queuedRegen) {
		m_queuedRegen->DoRegen = true;
	}
	else {
		m_queuedRegen = new dist_regen_queue_t;
		m_queuedRegen->NewType = m_distType;
		m_queuedRegen->DoRegen = true;
	}
}

// ================================================================================================
void SimManager::prepareRender()
{
	
}

// ================================================================================================
void SimManager::postRender()
{

}

// ================================================================================================
void SimManager::updateMeanValues(Particle* parts)
{
	m_meanVelocity = 0;
	m_meanAcceleration = 0;
	for (int i = 0; i < 50; ++i) {
		m_meanVelocity += parts[i].vel.length();
		m_meanAcceleration += parts[i].acc.length();
	}
	m_meanVelocity /= 50;
	m_meanAcceleration /= 50;
}

// ================================================================================================
base_nbody_solver* SimManager::makeCurrentSolver()
{
	if (m_currentIntegType == 0) { // Leapfrog
		if (m_currentIntegStyle == 0) { // Direct CPU
			return new cpu_leapfrog_solver(
				m_currentParticleCount,
				m_particleManager->getHostSourceForSize(m_currentSimSize),
				m_particleManager->getHostDestinationForSize(m_currentSimSize),
				m_timestep
			);
		}
		else if (m_currentIntegStyle == 1) { // OpenMP
			return new openmp_leapfrog_solver(
				m_currentParticleCount,
				m_particleManager->getHostSourceForSize(m_currentSimSize),
				m_particleManager->getHostDestinationForSize(m_currentSimSize),
				m_timestep
			);
		}
		else if (m_currentIntegStyle == 2) { // OpenCL
			return new opencl_leapfrog_solver(
				m_currentParticleCount,
				m_particleManager->getCLSourceForSize(m_currentSimSize),
				m_particleManager->getCLDestinationForSize(m_currentSimSize),
				m_timestep
			);
		}
		else
			QtFatal("The integrator style was set to an invalid value.");
	}
	else
		QtFatal("The integrator type was set to an invalid value.");

	return nullptr; // Will never be reached
}

// ================================================================================================
void SimManager::launchTimestep()
{
	m_upsTimer.restart();

	if (m_currentThread) {
		m_currentThread->join();
		delete m_currentThread;
	}

	m_currentSolver->prepare();
	m_currentThread = new tthread::thread([](void* arg) -> void { 
		static_cast<base_nbody_solver*>(arg)->solve(); 
	}, m_currentSolver);
}