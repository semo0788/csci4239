/**
 * Sean Moss (semo0788@colorado.edu)
 * Part of the final project for CSCI 4239, CU Boulder, Spring 2017.
 *
 * This file implements the code found in the file sim/memory_manager.hpp.
 */

#include <QString>
#include <QFile>
#include <QTextStream>
#include "sim/memory_manager.hpp"
#include <GL/glxew.h>


/* static */ const size_t ParticleMemoryManager::SIM_PARTICLE_COUNTS[4] = { 
	100, 1000, 10000, 100000
};

/* static */ size_t ParticleMemoryManager::OPENCL_MAX_WORK_GROUP_SIZE = 0;

/* static */ ParticleMemoryManager::OpenCLDeviceInfo ParticleMemoryManager::s_opencl = {};

/* static */ cl_program ParticleMemoryManager::OPENCL_LEAPFROG_PROGRAM = nullptr;
/* static */ cl_kernel ParticleMemoryManager::OPENCL_LEAPFROG_KERNEL = nullptr;


// ================================================================================================
ParticleMemoryManager::ParticleMemoryManager() :
	m_hostParticleLists{nullptr, nullptr, nullptr, nullptr},
	m_hostParticleLists2{nullptr, nullptr, nullptr, nullptr},
	m_deviceParticleLists{nullptr, nullptr, nullptr, nullptr},
	m_deviceParticleLists2{nullptr, nullptr, nullptr, nullptr},
	m_swapped{false, false, false, false},
	m_clDeviceMemory{nullptr, nullptr, nullptr, nullptr},
	m_clDeviceMemory2{nullptr, nullptr, nullptr, nullptr}
{
	for (size_t size = 0; size < 4; ++size) {
		initializeListsForSize(size);
		generateParticleArrayForSize(size, 0);
		triggerHostToDeviceCopy(size);
	}
}

// ================================================================================================
ParticleMemoryManager::~ParticleMemoryManager()
{
	for (int i = 0; i < 4; ++i) {
		delete [] m_hostParticleLists[i];
		delete [] m_hostParticleLists2[i];

		delete m_deviceParticleLists[i];
		delete m_deviceParticleLists2[i];
	}
}

// ================================================================================================
Particle* ParticleMemoryManager::getHostSourceForSize(size_t size)
{
	return m_swapped[size] ? m_hostParticleLists2[size] : m_hostParticleLists[size];
}

// ================================================================================================
Particle* ParticleMemoryManager::getHostDestinationForSize(size_t size)
{
	return m_swapped[size] ? m_hostParticleLists[size] : m_hostParticleLists2[size];
}

// ================================================================================================
VertexBuffer* ParticleMemoryManager::getSourceBufferForSize(size_t size)
{
	return m_swapped[size] ? m_deviceParticleLists2[size] : m_deviceParticleLists[size];
}

// ================================================================================================
VertexBuffer* ParticleMemoryManager::getDestinationBufferForSize(size_t size)
{
	return m_swapped[size] ? m_deviceParticleLists[size] : m_deviceParticleLists2[size];
}

// ================================================================================================
cl_mem ParticleMemoryManager::getCLSourceForSize(size_t size)
{
	return m_swapped[size] ? m_clDeviceMemory2[size] : m_clDeviceMemory[size];
}

// ================================================================================================
cl_mem ParticleMemoryManager::getCLDestinationForSize(size_t size)
{
	return m_swapped[size] ? m_clDeviceMemory[size] : m_clDeviceMemory2[size];
}

// ================================================================================================
void ParticleMemoryManager::acquireObjectsForSize(size_t size)
{
	cl_mem source = getCLSourceForSize(size);
	cl_mem dest = getCLDestinationForSize(size);

	glFinish();
	if (clEnqueueAcquireGLObjects(s_opencl.queue, 1, &source, 0, NULL, NULL) != CL_SUCCESS)
		QtFatal(strfmt("Unable to aquire OpenCL source for size %d.", size));
	if (clEnqueueAcquireGLObjects(s_opencl.queue, 1, &dest, 0, NULL, NULL) != CL_SUCCESS)
		QtFatal(strfmt("Unable to aquire OpenCL destination for size %d.", size));
	clFinish(s_opencl.queue);
}

// ================================================================================================
void ParticleMemoryManager::releaseObjectsForSize(size_t size)
{
	cl_mem source = getCLSourceForSize(size);
	cl_mem dest = getCLDestinationForSize(size);

	clFinish(s_opencl.queue);
	if (clEnqueueReleaseGLObjects(s_opencl.queue, 1, &source, 0, NULL, NULL) != CL_SUCCESS)
		QtFatal(strfmt("Unable to release OpenCL source for size %d.", size));
	if (clEnqueueReleaseGLObjects(s_opencl.queue, 1, &dest, 0, NULL, NULL) != CL_SUCCESS)
		QtFatal(strfmt("Unable to release OpenCL destination for size %d.", size));
	glFinish();
}

// ================================================================================================
void ParticleMemoryManager::triggerHostToDeviceCopy(size_t size)
{
	VertexBuffer *buffer = getSourceBufferForSize(size);
	Particle *parts = getHostSourceForSize(size);
	buffer->setData(parts);
}

// ================================================================================================
void ParticleMemoryManager::triggerDeviceToHostCopy(size_t size)
{
	VertexBuffer *buffer = getSourceBufferForSize(size);
	Particle *parts = getHostSourceForSize(size);
	const size_t PCOUNT = SIM_PARTICLE_COUNTS[size];
	Particle *mappedMem = static_cast<Particle*>(buffer->mapBuffer(GL_READ_ONLY));
	memcpy(parts, mappedMem, PCOUNT * sizeof(Particle));
	buffer->unmapBuffer();
}

// ================================================================================================
void ParticleMemoryManager::initializeListsForSize(size_t size)
{
	// Create the host arrays
	const size_t pcount = SIM_PARTICLE_COUNTS[size];
	m_hostParticleLists[size] = new Particle[pcount];
	m_hostParticleLists2[size] = new Particle[pcount];

	// Create the device buffers
	const size_t rawcount = pcount * sizeof(Particle);
	m_deviceParticleLists[size] = new VertexBuffer(rawcount, GL_STREAM_DRAW);
	m_deviceParticleLists[size]->setFormat(ParticleFormatSpecifiers, ParticleFormatSpecifierLength);
	m_deviceParticleLists2[size] = new VertexBuffer(rawcount, GL_STREAM_DRAW);
	m_deviceParticleLists2[size]->setFormat(ParticleFormatSpecifiers, ParticleFormatSpecifierLength);

	// Link the opengl buffers to opencl buffers
	cl_int clerr = 0;
	m_clDeviceMemory[size] = clCreateFromGLBuffer(s_opencl.context, CL_MEM_READ_WRITE, 
			m_deviceParticleLists[size]->getVboName(), &clerr);
	if (!m_clDeviceMemory[size] || clerr != CL_SUCCESS)
		QtFatal(strfmt("Unable to share OpenGL buffer with OpenCL (Error: %d).", clerr));
	m_clDeviceMemory2[size] = clCreateFromGLBuffer(s_opencl.context, CL_MEM_READ_WRITE,
			m_deviceParticleLists2[size]->getVboName(), &clerr);
	if (!m_clDeviceMemory2[size] || clerr != CL_SUCCESS)
		QtFatal(strfmt("Unable to share OpenGL buffer with OpenCL (Error: %d).", clerr));
}

// ================================================================================================
void ParticleMemoryManager::generateParticleArrayForSize(size_t size, int type)
{
	Particle *list = m_hostParticleLists[size];

	static const auto randflt = [](float low = 0.0f, float high = 1.0f) -> float {
		float flt = rand() / (float)RAND_MAX;
		return (flt * (high - low)) + low;
	};

	const size_t pcount = SIM_PARTICLE_COUNTS[size];
	if (type == 0) {
		for (unsigned int i = 0; i < pcount; ++i) {
			Particle& p = list[i];
			p.m = randflt();
			p.x = randflt(-5, 5);
			p.y = randflt(-5, 5);
			p.z = randflt(-5, 5);
			p.vx = randflt(-0.5, 0.5);
			p.vy = randflt(-0.5, 0.5);
			p.vz = randflt(-0.5, 0.5);
			p.ax = randflt(-0.5, 0.5);
			p.ay = randflt(-0.5, 0.5);
			p.az = randflt(-0.5, 0.5);
		}
	}
	else if (type == 1) {
		for (unsigned int i = 0; i < pcount; ++i) {
			Particle& part = list[i];
			part.m = randflt();
			
			const float r = randflt();
			const float t = randflt();
			const float p = randflt();
			const float vr = randflt();
			const float vt = randflt();
			const float vp = randflt();
			const float ar = randflt();
			const float at = randflt();
			const float ap = randflt();

			// Precalculate commonly used values
			const float SP = sin(p);
			const float CP = cos(p);
			const float ST = sin(t);
			const float CT = cos(t);
			const float SPST = SP * ST;
			const float SPCT = SP * CT;
			const float CPST = CP * ST;
			const float CPCT = CP * CT;
			const float VRVT = vr * vt;
			const float VRVP = vr * vp;
			const float RAT = r * at;
			const float RAP = r * ap;
			const float RVP2 = r * vp * vp;
			const float RVT2 = r * vt * vt;

			// Position
			part.x = r * SPCT;
			part.y = r * SPST;
			part.z = r * CP;

			// Velocity
			part.vx = (SPCT * vr) + (r * SPST * vt) + (r * CPCT * vp);
			part.vy = (SPST * vr) - (r * SPCT * vt) + (r * CPST * vp);
			part.vz = (CP * vr) - (r * SP * vp);

			// Acceleration
			part.ax = (ar * SPCT)   - (VRVP * CPCT) - (VRVT * SPST)
					- (VRVP * CPCT) - (RVP2 * SPCT) - (RAP * CPCT)
					- (VRVT * SPST) - (RVT2 * SPCT) - (RAT * SPST)
					+ (2 * r * vp * vt * CPST);
			part.ay = (ar * SPST)   - (VRVP * CPST) + (VRVT * SPCT)
					- (VRVP * CPST) - (RVP2 * SPST) - (RAP * CPST)
					+ (VRVT * SPCT) - (RVT2 * SPST) + (RAT * SPCT)
					- (2 * r * vp * vt * CPCT);
			part.az = (ar * CP) + (2 * vp * vt * SP) - (RVP2 * CP) + (RAP * SP);
		}
	}
}

// ================================================================================================
// Note: Code from this function is largely taken from examples provided by Willem Schreuder
/* static */ void ParticleMemoryManager::InitializeGraphicsCompute()
{
	cl_int clerr;
	char clname[1024];
	int clmaxflops = -1;
	cl_platform_id clfastestplatid = 0;
	cl_device_id clfastestid = 0;

	// Get the OpenCL platforms
	cl_uint numPlat;
	cl_platform_id clplatforms[1024];
	if (clGetPlatformIDs(1024, clplatforms, &numPlat))
		QtFatal("Could not retrieve number of OpenCL platforms.");
	else if (numPlat < 1)
		QtFatal("No available OpenCL platforms.");

	// Loop over each platform to retrieve devices
	for (unsigned int pIndex = 0; pIndex < numPlat; ++pIndex) {
		// Get the name of the platform
		if (clGetPlatformInfo(clplatforms[pIndex], CL_PLATFORM_NAME, 1024, clname, NULL))
			QtFatal("Could not retrieve name of OpenCL platform.");

		// Get the devices on the platform
		cl_uint numDev;
		cl_device_id cldevices[1024];
		if (clGetDeviceIDs(clplatforms[pIndex], CL_DEVICE_TYPE_GPU, 1024, cldevices, &numDev))
			QtFatal(strfmt("Could not retrieve number of OpenCL devices for platform '%s'.", clname));
		else if (numDev < 1)
			QtFatal(strfmt("No valid OpenCL devices were found on platform '%s'.", clname));

		// Loop over the devices on this platform to try to find the fastest one
		for (unsigned int dIndex = 0; dIndex < numDev; ++dIndex) {
			// Extract the device name, compute unit count, and processor speed
			cl_uint dUnits, dFreq;
			if (clGetDeviceInfo(cldevices[dIndex], CL_DEVICE_NAME, 1024, clname, NULL))
				QtFatal("Unable to retrieve name of OpenCL device.");
			if (clGetDeviceInfo(cldevices[dIndex], CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(dUnits), &dUnits, NULL))
				QtFatal(strfmt("Could not get the number of compute units for device '%s'.", clname));
			if (clGetDeviceInfo(cldevices[dIndex], CL_DEVICE_MAX_CLOCK_FREQUENCY, sizeof(dFreq), &dFreq, NULL))
				QtFatal(strfmt("Could not get the clock frequency for device '%s'.", clname));

			// Calculate the speed, and check if it is the fastest
			int flops = dUnits * dFreq;
			if (flops > clmaxflops) {
				clfastestplatid = clplatforms[pIndex];
				clfastestid = cldevices[dIndex];
				clmaxflops = flops;
			}
		}
	}

	// Print info for fastest device
	if (clGetDeviceInfo(clfastestid, CL_DEVICE_NAME, 1024, clname, NULL))
		QtFatal("Could not retrieve name of fastest OpenCL device.");
	if (clGetDeviceInfo(clfastestid, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(size_t), &OPENCL_MAX_WORK_GROUP_SIZE, NULL))
		QtFatal(strfmt("Could not get the max work group size of device '%s'.", clname));
	linfo(strfmt("Fastest OpenCL Device: '%s' (%f Gflops) (MWGS: %d)", clname, clmaxflops * 1e-3, OPENCL_MAX_WORK_GROUP_SIZE));

	// Extract opengl context information
	void *glxContext = glXGetCurrentContext();
	if (!glxContext)
		QtFatal("Could not get current GLX context handle.");
	void *glxDisplay = glXGetCurrentDisplay();
	if (!glxDisplay)
		QtFatal("Could not get current GLX display handle.");

	// Set up the OpenGL context information
	cl_context_properties glprops[7] = {
		CL_CONTEXT_PLATFORM, (cl_context_properties) clfastestplatid,
		CL_GL_CONTEXT_KHR, (cl_context_properties) glxContext,
		CL_GLX_DISPLAY_KHR, (cl_context_properties) glxDisplay,
		0
	};

	// OpenCL error callback for next few steps
	const auto err_callback = [](const char *errinfo, const void *, size_t, void *) -> void {
		lerr(strfmt("OpenCL Error: '%s'.", errinfo));
	};

	// Create the context and command queue on the fastest device
	s_opencl.deviceId = clfastestid;
	s_opencl.context = clCreateContext(glprops, 1, &clfastestid, err_callback, NULL, &clerr);
	if (!s_opencl.context || clerr)
		QtFatal(strfmt("Could not create OpenCL shared context on device '%s'.", clname));
	s_opencl.queue = clCreateCommandQueue(s_opencl.context, clfastestid, 0, &clerr);
	if (!s_opencl.queue || clerr)
		QtFatal(strfmt("Could not create OpenCL command queue on device '%s'.", clname));

	linfo(strfmt("OpenCL initialized on device '%s'.", clname));


	// ======== Compile kernels ========
	// Read in kernel source
	QFile lffile(":/compute/leapfrog.kern");
	String leapfrogsrc;
	if (lffile.open(QIODevice::ReadOnly | QIODevice::Text)) {
		QTextStream stream(&lffile);
		while (!stream.atEnd()) {
			String line = stream.readLine().toStdString();
			leapfrogsrc += line + "\n";
		}
	}
	lffile.close();

	// Compile kernel
	const char * lfsrc = leapfrogsrc.c_str();
	OPENCL_LEAPFROG_PROGRAM = clCreateProgramWithSource(s_opencl.context, 1, &lfsrc, 0, &clerr);
	if (clerr != CL_SUCCESS)
		QtFatal(strfmt("Could not create leapfrog program (Error: %d).", clerr));
	if (clBuildProgram(OPENCL_LEAPFROG_PROGRAM, 0, NULL, NULL, NULL, NULL) != CL_SUCCESS) {
		char cllog[1024];
		if (clGetProgramBuildInfo(OPENCL_LEAPFROG_PROGRAM, s_opencl.deviceId, CL_PROGRAM_BUILD_LOG, 1024, cllog, NULL) != CL_SUCCESS)
			QtFatal("Could not get leapfrog program build log.");
		else
			QtFatal(strfmt("Could not build leapfrog program. Reason: '%s'.", cllog));
	}
	OPENCL_LEAPFROG_KERNEL = clCreateKernel(OPENCL_LEAPFROG_PROGRAM, "Solve", &clerr);
	if (clerr != CL_SUCCESS)
		QtFatal(strfmt("Could not create leapfrog kernel (Error: %d).", clerr));
	else
		linfo("Compiled Leapfrog kernel successfully.");
}