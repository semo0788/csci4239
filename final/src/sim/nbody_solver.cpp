/**
 * Sean Moss (semo0788@colorado.edu)
 * Part of the final project for CSCI 4239, CU Boulder, Spring 2017.
 *
 * This file implements the code found in sim/nbody_solver.hpp.
 */

#include "sim/nbody_solver.hpp"
#include "sim/memory_manager.hpp"


// ================================================================================================
bool base_nbody_solver::isDone()
{
	tthread::lock_guard<tthread::fast_mutex> lock(m_completeMutex);
	return m_complete;
}

// ================================================================================================
void base_nbody_solver::toggleComplete()
{
	tthread::lock_guard<tthread::fast_mutex> lock(m_completeMutex);
	m_complete = true;
}


// ================================================================================================
void cpu_leapfrog_solver::solve()
{
	Particle * const src = static_cast<Particle * const>(SourceArray);
	Particle * const dst = static_cast<Particle * const>(DestinationArray);

	for (size_t i = 0; i < ParticleCount; ++i) {
		Particle& srcp = src[i];
		Particle& dstp = dst[i];

		vec3 force;
		for (size_t j = 0; j < ParticleCount; ++j) {
			if (i == j)
				continue;
			
			Particle& opart = src[j];

			vec3 diff = (opart.pos - srcp.pos);
			float dist = diff.length() + 1; // With damping
			float scale = opart.m / (dist * dist * dist);
			force += (scale * diff);
		}

		dstp.m = srcp.m;
		dstp.acc = (force / srcp.m);
		dstp.vel = srcp.vel + (dstp.acc * DeltaTime);
		dstp.pos = srcp.pos + (dstp.vel * DeltaTime);
	}

	toggleComplete();
}


// ================================================================================================
void openmp_leapfrog_solver::solve()
{
	Particle * const src = static_cast<Particle * const>(SourceArray);
	Particle * const dst = static_cast<Particle * const>(DestinationArray);

	#pragma omp parallel for
	for (size_t i = 0; i < ParticleCount; ++i) {
		Particle& srcp = src[i];
		Particle& dstp = dst[i];

		vec3 force;
		for (size_t j = 0; j < ParticleCount; ++j) {
			if (i == j)
				continue;
			
			Particle& opart = src[j];

			vec3 diff = (opart.pos - srcp.pos);
			float dist = diff.length() + 1; // With damping
			float scale = opart.m / (dist * dist * dist);
			force += (scale * diff);
		}

		dstp.m = srcp.m;
		dstp.acc = (force / srcp.m);
		dstp.vel = srcp.vel + (dstp.acc * DeltaTime);
		dstp.pos = srcp.pos + (dstp.vel * DeltaTime);
	}

	toggleComplete();
}


// ================================================================================================
void opencl_leapfrog_solver::solve()
{
	cl_command_queue queue = ParticleMemoryManager::GetCLQueue();
	cl_kernel kernel = ParticleMemoryManager::OPENCL_LEAPFROG_KERNEL;

	int pcount = static_cast<int>(ParticleCount);
	size_t Global[1] = { ParticleCount };

	cl_int clerr;
	if ((clerr = clSetKernelArg(kernel, 0, sizeof(m_source), &m_source))) {
		lerr(strfmt("Could not set the source array for the leapfrog kernel (error: %d).", clerr));
		goto end;
	}
	if ((clerr = clSetKernelArg(kernel, 1, sizeof(m_dest), &m_dest))) {
		lerr(strfmt("Could not set the destination array for the leapfrog kernel (error: %d).", clerr));
		goto end;
	}
	if ((clerr = clSetKernelArg(kernel, 2, sizeof(float), &DeltaTime))) {
		lerr(strfmt("Could not set the delta time for the leapfrog kernel (error: %d).", clerr));
		goto end;
	}
	if ((clerr = clSetKernelArg(kernel, 3, sizeof(int), &pcount))) {
		lerr(strfmt("Could not set the particle count for the leapfrog kernel (error: %d).", clerr));
		goto end;
	}

	if ((clerr = clEnqueueNDRangeKernel(queue, kernel, 1, NULL, Global, NULL, 0, NULL, NULL))) {
		lerr(strfmt("Could not run OpenCL leapfrog kernel (error: %d).", clerr));
		goto end;
	}
	if ((clerr = clFinish(queue))) {
		lerr(strfmt("Could not wait for leapfrog kernel to finish (error: %d).", clerr));
		goto end;
	}

end:
	toggleComplete();
}