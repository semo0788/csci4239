/**
 * Sean Moss (semo0788@colorado.edu)
 * Part of the final project for CSCI 4239, CU Boulder, Spring 2017.
 *
 * This file implements the code found in the file sim/particle.hpp.
 */

#include "sim/particle.hpp"


const size_t ParticleFormatSpecifierLength = 4;

const vertex_format_specifier_t ParticleFormatSpecifiers[4] = {
	{0, 1, GL_FLOAT, 10 * sizeof(GLfloat), 0},					 // Mass
	{1, 3, GL_FLOAT, 10 * sizeof(GLfloat), 1 * sizeof(GLfloat)}, // Position
	{2, 3, GL_FLOAT, 10 * sizeof(GLfloat), 4 * sizeof(GLfloat)}, // Velocity
	{3, 3, GL_FLOAT, 10 * sizeof(GLfloat), 7 * sizeof(GLfloat)}  // Acceleration
};