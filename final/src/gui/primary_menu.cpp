/**
 * Sean Moss (semo0788@colorado.edu)
 * Part of the final project for CSCI 4239, CU Boulder, Spring 2017.
 *
 * This file implements the code in gui/primary_menu.hpp.
 */

#include "gui/primary_window.hpp"


// ================================================================================================
PrimaryMenu::PrimaryMenu()
{
	addAction("Test Action");
}

// ================================================================================================
PrimaryMenu::~PrimaryMenu()
{

}