/**
 * Sean Moss (semo0788@colorado.edu)
 * Part of the final project for CSCI 4239, CU Boulder, Spring 2017.
 *
 * This file implements the code in the gui/opengl_viewer.hpp file.
 */

#include "render/ogl_utils.hpp"
#include "gui/opengl_viewer.hpp"
#include <QMouseEvent>
#include <QWheelEvent>



// ================================================================================================
OpenGLViewer::OpenGLViewer() :
	m_simulation{nullptr},
	m_camera{},
	m_mouse{false},
	m_mousePos{0, 0}
{
	setMinimumSize(QSize(1200, 900)); // The smallest "useable" 4:3 ratio

	m_frameTimer.setInterval(10); // Try for 100 fps maximum
	m_frameTimer.start();
	connect(&m_frameTimer, SIGNAL(timeout()), this, SLOT(doFrame()));

	m_updateTimer.setInterval(2); // Check 500 times a second for timestep updates
	connect(&m_updateTimer, SIGNAL(timeout()), this, SLOT(doUpdate()));

	m_elapsedTimer.start();
	m_fpsTimer.start();

	m_camera.setTheta(M_PI / 4);
	m_camera.setPhi(M_PI / 4);
	m_camera.setDistance(10);

	m_simulation = new SimManager;
}

// ================================================================================================
OpenGLViewer::~OpenGLViewer()
{
	delete m_simulation;
}

// ================================================================================================
void OpenGLViewer::initializeGL()
{
	OGLUtils::InitializeGL();

	m_simulation->initializeGL();
	m_updateTimer.start();

	update();
}

// ================================================================================================
void OpenGLViewer::resizeGL(int, int)
{
	float asp = width() / (float)height();
	glViewport(0, 0, width(), height());
	m_camera.setAspect(asp);
	update();
}

// ================================================================================================
void OpenGLViewer::paintGL()
{
	// Update FPS
	static uint64 frameCounter = 0;
	++frameCounter;
	if (m_fpsTimer.elapsed() >= 500) {
		uint64 totalTime = m_fpsTimer.restart();
		float fps = float(frameCounter) * 1000 / totalTime;
		frameCounter = 0;
		emit updateFPS(fps);
	}

	// Do rendering
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_BLEND);

	m_simulation->render(m_camera);
}

// ================================================================================================
void OpenGLViewer::mousePressEvent(QMouseEvent *event)
{
	m_mousePos = event->pos();
	m_mouse = true;
}

// ================================================================================================
void OpenGLViewer::mouseReleaseEvent(QMouseEvent *event)
{
	(void)event;
	m_mouse = false;
}

// ================================================================================================
void OpenGLViewer::mouseMoveEvent(QMouseEvent *event)
{
	static const float PHI_LIMIT = M_PI * 0.98 / 2;
	if (m_mouse)
	{
		QPoint d = event->pos() - m_mousePos;
		float nt = fmod(m_camera.getTheta() + (d.x() * 0.003), M_PI * 2);
		float np = fmod(m_camera.getPhi() + (d.y() * 0.004), M_PI * 2);
		np = (np < -PHI_LIMIT) ? -PHI_LIMIT : (np > PHI_LIMIT) ? PHI_LIMIT : np;
		m_mousePos = event->pos();
		m_camera.setTheta(nt);
		m_camera.setPhi(np);
		emit updateAngles(nt, np);
	}
}

// ================================================================================================
void OpenGLViewer::wheelEvent(QWheelEvent *event)
{
	float nd = m_camera.getDistance();
	if (event->delta() < 0)
		nd += 0.1;
	else if (event->delta() > 0)
		nd -= 0.1;
	
	nd = (nd > 20) ? 20 : (nd < 2) ? 2 : nd;
	m_camera.setDistance(nd);
	emit updateDistance(nd);
}

// ================================================================================================
void OpenGLViewer::doFrame()
{
	update();
}

// ================================================================================================
void OpenGLViewer::doUpdate()
{
	// Check for updates to the simulation
	m_simulation->update();
}