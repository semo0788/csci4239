/**
 * Sean Moss (semo0788@colorado.edu)
 * Part of the final project for CSCI 4239, CU Boulder, Spring 2017.
 *
 * This file implements code found in the gui/control_dockpanel.hpp file.
 */

#include "gui/control_dockpanel.hpp"
#include "sim/memory_manager.hpp"
#include <QGroupBox>
#include <QGridLayout>
#include <QVBoxLayout>


static const QString GROUPBOX_STYLESHEET = R"(
	QGroupBox {
		border: 1px solid gray;
		border-radius: 5px;
		margin-top: 0.5em;
	}

	QGroupBox::title {
		subcontrol-origin: margin;
		left: 10px;
		padding: 0 3px 0 3px;
	}
)";

static const QString INTEG_TYPE_DESC[1] = {
	"  Fast, Inaccurate" // Leapfrog
};

static const QString INTEG_LOC_DESC[3] = {
	"  Sequential (CPU)", // Direct CPU
	"  Parallel (CPU)", // OpenMP
	"  Parallel (GPU)" // CUDA (Parallel)
};

static const QString SPEED_DESC[3] = {
	"Current Timestep: 1e-2",
	"Current Timestep: 1e-3",
	"Current Timestep: 1e-4"
};


// ================================================================================================
ControlDockPanel::ControlDockPanel(SimManager *sim) :
	QDockWidget("Simulation Control"),
	m_simManager{sim},
	m_simSizeSelector{nullptr},
	m_simSizeLabel{nullptr},
	m_integTypeSelector{nullptr},
	m_integLocSelector{nullptr},
	m_integTypeLabel{nullptr},
	m_integLocLabel{nullptr},
	m_distTypeControl{nullptr},
	m_distRegenerateButton{nullptr},
	m_axesShowControl{nullptr},
	m_vectorShowControl{nullptr},
	m_colorControlSelector{nullptr},
	m_speedSelectorSlider{nullptr},
	m_speedLabel{nullptr},
	m_pausePlayButton{nullptr}
{
	setAllowedAreas(Qt::LeftDockWidgetArea);
	setFeatures(QDockWidget::NoDockWidgetFeatures);
	setMinimumWidth(275);

	// Overall widget to put in group box, and its layout
	QWidget *topLevelContainer = new QWidget;
	QVBoxLayout *topLevelLayout = new QVBoxLayout;
	topLevelLayout->setContentsMargins(5, 5, 5, 5);

	// Create the individual components for the dock window
	topLevelLayout->addWidget(prepareSimulationSizeSelector());
	topLevelLayout->addWidget(prepareIntegratorTypeSelector());
	topLevelLayout->addWidget(prepareDistributionControl());
	topLevelLayout->addWidget(prepareRenderControl());
	topLevelLayout->addStretch();
	topLevelLayout->addWidget(prepareSpeedControl());

	// Save the layout and commit the widget
	topLevelContainer->setLayout(topLevelLayout);
	setWidget(topLevelContainer);

	// Connect the various controls
	connect(m_simSizeSelector, SIGNAL(currentIndexChanged(int)), this, SLOT(setSimSize(int)));
	connect(m_integTypeSelector, SIGNAL(currentIndexChanged(int)), this, SLOT(setIntegType(int)));
	connect(m_integLocSelector, SIGNAL(currentIndexChanged(int)), this, SLOT(setIntegLoc(int)));
	connect(m_speedSelectorSlider, SIGNAL(sliderMoved(int)), this, SLOT(setSpeedValue(int)));
	connect(m_pausePlayButton, SIGNAL(pressed()), this, SLOT(setPaused()));
	connect(m_axesShowControl, SIGNAL(clicked(bool)), this, SLOT(setShowAxes(bool)));
	connect(m_vectorShowControl, SIGNAL(clicked(bool)), this, SLOT(setShowVectors(bool)));
	connect(m_colorControlSelector, SIGNAL(currentIndexChanged(int)), this, SLOT(setColorStyle(int)));
	connect(m_distTypeControl, SIGNAL(currentIndexChanged(int)), this, SLOT(setDistType(int)));
	connect(m_distRegenerateButton, SIGNAL(pressed()), this, SLOT(regenerateDist()));
}

// ================================================================================================
QWidget* ControlDockPanel::prepareSimulationSizeSelector()
{
	QGroupBox *groupBox = new QGroupBox(tr("Size Control"));
	groupBox->setContentsMargins(3, 11, 3, 3);
	groupBox->setStyleSheet(GROUPBOX_STYLESHEET);
	QGridLayout *layout = new QGridLayout;

	m_simSizeSelector = new QComboBox;
	m_simSizeSelector->addItem(tr("Small"));
	m_simSizeSelector->addItem(tr("Medium"));
	m_simSizeSelector->addItem(tr("Large"));
	m_simSizeSelector->addItem(tr("Huge"));

	m_simSizeLabel = new QLabel(tr("Small (100 particles)"));

	layout->addWidget(new QLabel(tr("Simulation Size")), 0, 0, 1, 1);
	layout->addWidget(m_simSizeSelector, 0, 1, 1, 1);
	layout->addWidget(m_simSizeLabel, 1, 0, 1, 2);

	groupBox->setLayout(layout);
	return groupBox;
}

// ================================================================================================
QWidget* ControlDockPanel::prepareIntegratorTypeSelector()
{
	QGroupBox *groupBox = new QGroupBox(tr("Integrator Control"));
	groupBox->setContentsMargins(3, 11, 3, 3);
	groupBox->setStyleSheet(GROUPBOX_STYLESHEET);
	QGridLayout *layout = new QGridLayout;

	m_integTypeSelector = new QComboBox;
	m_integTypeSelector->addItem(tr("Leapfrog"));

	m_integTypeLabel = new QLabel(INTEG_TYPE_DESC[0]);

	m_integLocSelector = new QComboBox;
	m_integLocSelector->addItem(tr("Direct CPU"));
	m_integLocSelector->addItem(tr("OpenMP"));
	m_integLocSelector->addItem(tr("OpenCL"));

	m_integLocLabel = new QLabel(INTEG_LOC_DESC[0]);

	layout->addWidget(new QLabel(tr("Integ. Type")), 0, 0, 1, 1);
	layout->addWidget(m_integTypeSelector, 0, 1, 1, 1);
	layout->addWidget(m_integTypeLabel, 1, 0, 1, 2);
	layout->addWidget(new QLabel(tr("Integ. Style")), 2, 0, 1, 1);
	layout->addWidget(m_integLocSelector, 2, 1, 1, 1);
	layout->addWidget(m_integLocLabel, 3, 0, 1, 2);

	groupBox->setLayout(layout);
	return groupBox;
}

// ================================================================================================
QWidget* ControlDockPanel::prepareDistributionControl()
{
	QGroupBox *groupBox = new QGroupBox(tr("Distribution Control"));
	groupBox->setContentsMargins(3, 11, 3, 3);
	groupBox->setStyleSheet(GROUPBOX_STYLESHEET);
	QGridLayout *layout = new QGridLayout;

	m_distTypeControl = new QComboBox;
	m_distTypeControl->addItem(tr("Square"));
	m_distTypeControl->addItem(tr("Sphere"));
	m_distTypeControl->addItem(tr("Random Groups"));
	m_distRegenerateButton = new QPushButton("Regenerate");

	layout->addWidget(new QLabel(tr("Dist. Type:")), 0, 0, 1, 1);
	layout->addWidget(m_distTypeControl, 0, 1, 1, 1);
	layout->addWidget(m_distRegenerateButton, 1, 1, 1, 1);

	groupBox->setLayout(layout);
	return groupBox;
}

// ================================================================================================
QWidget* ControlDockPanel::prepareRenderControl()
{
	QGroupBox *groupBox = new QGroupBox(tr("Render Control"));
	groupBox->setContentsMargins(3, 11, 3, 3);
	groupBox->setStyleSheet(GROUPBOX_STYLESHEET);
	QGridLayout *layout = new QGridLayout;

	m_axesShowControl = new QCheckBox(tr("Show Axes"));
	m_vectorShowControl = new QCheckBox(tr("Show Particle Vectors"));
	m_colorControlSelector = new QComboBox;
	m_colorControlSelector->addItem(tr("None"));
	m_colorControlSelector->addItem(tr("Mass"));
	m_colorControlSelector->addItem(tr("Position"));
	m_colorControlSelector->addItem(tr("Velocity"));
	m_colorControlSelector->addItem(tr("Acceleration"));
	m_colorControlSelector->addItem(tr("Index"));

	layout->addWidget(m_axesShowControl, 0, 0, 1, 2);
	layout->addWidget(m_vectorShowControl, 1, 0, 1, 2);
	layout->addWidget(new QLabel(tr("\tGreen Vector = Normalized Velocity\n\tBlue Vector = Normalized Acceleration")), 2, 0, 1, 2);
	layout->addWidget(new QLabel(tr("Color Coding:")), 3, 0, 1, 1);
	layout->addWidget(m_colorControlSelector, 3, 1, 1, 1);
	layout->addWidget(new QLabel(tr("\tColor Legend:\tBlue -> Lowest Values\n\t\t\tRed -> Highest Values")), 4, 0, 1, 2);

	groupBox->setLayout(layout);
	return groupBox;
}

// ================================================================================================
QWidget* ControlDockPanel::prepareSpeedControl()
{
	QGroupBox *groupBox = new QGroupBox(tr("Speed Control"));
	groupBox->setContentsMargins(3, 11, 3, 3);
	groupBox->setStyleSheet(GROUPBOX_STYLESHEET);
	QGridLayout *layout = new QGridLayout;

	m_speedSelectorSlider = new QSlider(Qt::Horizontal);
	m_speedSelectorSlider->setTickInterval(1);
	m_speedSelectorSlider->setMaximum(2);
	m_speedSelectorSlider->setMinimum(0);
	m_speedSelectorSlider->setTickPosition(QSlider::TicksBelow);
	m_speedSelectorSlider->setValue(1);
	m_pausePlayButton = new QPushButton(tr("Pause"));
	m_speedLabel = new QLabel(SPEED_DESC[1]);
	m_speedLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

	layout->addWidget(new QLabel(tr("Sim Speed:")), 0, 0, 1, 1);
	layout->addWidget(m_speedSelectorSlider, 0, 1, 1, 2);
	layout->addWidget(m_pausePlayButton, 1, 0, 1, 1);
	layout->addWidget(m_speedLabel, 1, 1, 1, 2);

	groupBox->setLayout(layout);
	return groupBox;
}

// ================================================================================================
void ControlDockPanel::setSimSize(int size)
{
	unsigned int sz = static_cast<unsigned int>(ParticleMemoryManager::SIM_PARTICLE_COUNTS[size]);
	String szText = m_simSizeSelector->itemText(size).toStdString();

	m_simSizeLabel->setText(QString().sprintf("%s (%d particles)", szText.c_str(), sz));
	m_simManager->setSimulationSize(size);
}

// ================================================================================================
void ControlDockPanel::setIntegType(int type)
{
	m_integTypeLabel->setText(INTEG_TYPE_DESC[type]);
	m_simManager->setIntegratorType(type);
}

// ================================================================================================
void ControlDockPanel::setIntegLoc(int loc)
{
	m_integLocLabel->setText(INTEG_LOC_DESC[loc]);
	m_simManager->setIntegratorLocation(loc);
}

// ================================================================================================
void ControlDockPanel::setPaused()
{
	if (m_pausePlayButton->text() == "Pause") {
		m_pausePlayButton->setText(tr("Resume"));
		m_simManager->setPaused(true);
	}
	else {
		m_pausePlayButton->setText(tr("Pause"));
		m_simManager->setPaused(false);
	}
}

// ================================================================================================
void ControlDockPanel::setSpeedValue(int speed)
{
	m_speedLabel->setText(SPEED_DESC[speed]);
	m_simManager->setTimestep(speed);
}

// ================================================================================================
void ControlDockPanel::setShowAxes(bool checked)
{
	m_simManager->setShowAxes(checked);
}

// ================================================================================================
void ControlDockPanel::setShowVectors(bool checked)
{
	m_simManager->setShowVectors(checked);
}

// ================================================================================================
void ControlDockPanel::setColorStyle(int style)
{
	m_simManager->setColorStyle(style);
}

// ================================================================================================
void ControlDockPanel::setDistType(int type)
{
	m_simManager->setDistType(type);
}

// ================================================================================================
void ControlDockPanel::regenerateDist()
{
	m_simManager->regenDist();
}