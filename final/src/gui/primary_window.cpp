/**
 * Sean Moss (semo0788@colorado.edu)
 * Part of the final project for CSCI 4239, CU Boulder, Spring 2017.
 *
 * This file implements the code in the gui/primary_window.hpp file.
 */

#include "gui/primary_window.hpp"


// ================================================================================================
PrimaryWindow::PrimaryWindow() :
	m_oglView{nullptr},
	m_topMenu{nullptr},
	m_statusBar{nullptr},
	m_controlPanel{nullptr},
	m_sbFpsLabel{nullptr},
	m_sbUpsLabel{nullptr},
	m_sbAngleLabel{nullptr},
	m_sbDistLabel{nullptr}
{
	setWindowTitle(tr("Final Project - NBody Simulation - Sean Moss"));

	// Prepare top menu
	m_topMenu = new PrimaryMenu;
	//setMenuBar(m_topMenu);

	// Prepare central opengl widget
	m_oglView = new OpenGLViewer;
	setCentralWidget(m_oglView);

	// Prepare status bar
	m_statusBar = new QStatusBar;
	m_sbFpsLabel = new QLabel("FPS:");
	m_sbFpsLabel->setMinimumWidth(70);
	m_statusBar->addPermanentWidget(m_sbFpsLabel);
	m_sbUpsLabel = new QLabel("UPS:");
	m_sbUpsLabel->setMinimumWidth(70);
	m_statusBar->addPermanentWidget(m_sbUpsLabel);
	m_sbDistLabel = new QLabel(QString().sprintf("Distance: %.2f", 10.0f));
	m_sbDistLabel->setMinimumWidth(90);
	m_statusBar->addPermanentWidget(m_sbDistLabel);
	m_sbAngleLabel = new QLabel(QString().sprintf("Angles: %.2f, %.2f", M_PI / 4, M_PI / 4));
	m_sbAngleLabel->setMinimumWidth(110);
	m_statusBar->addPermanentWidget(m_sbAngleLabel);
	setStatusBar(m_statusBar);

	// Prepare the docked control panel
	m_controlPanel = new ControlDockPanel(m_oglView->getSimManager());
	addDockWidget(Qt::LeftDockWidgetArea, m_controlPanel);

	// Connect the signals from the opengl widget
	connect(m_oglView, SIGNAL(updateAngles(float, float)), this, SLOT(setAngles(float, float)));
	connect(m_oglView, SIGNAL(updateDistance(float)), this, SLOT(setDistance(float)));
	connect(m_oglView, SIGNAL(updateFPS(float)), this, SLOT(setFPS(float)));
	connect(m_oglView->getSimManager(), SIGNAL(updateUPS(float)), this, SLOT(setUPS(float)));

	m_upsTimer.start();
}

// ================================================================================================
PrimaryWindow::~PrimaryWindow()
{

}

// ================================================================================================
void PrimaryWindow::setAngles(float nt, float np)
{
	m_sbAngleLabel->setText(QString().sprintf("Angles: %.2f, %.2f", nt, np));
}

// ================================================================================================
void PrimaryWindow::setDistance(float dist)
{
	m_sbDistLabel->setText(QString().sprintf("Distance: %.2f", dist));
}

// ================================================================================================
void PrimaryWindow::setFPS(float fps)
{
	m_sbFpsLabel->setText(QString().sprintf("FPS: %.3f", fps));
}

// ================================================================================================
void PrimaryWindow::setUPS(float ups)
{
	// Rate limit the UPS timer updating
	if (m_upsTimer.elapsed() > 500) {
		m_sbUpsLabel->setText(QString().sprintf("UPS: %.3f", ups));
		m_upsTimer.restart();
	}
}