/**
 * Sean Moss (semo0788@colorado.edu)
 * Part of the final project for CSCI 4239, CU Boulder, Spring 2017.
 *
 * This file implements the code in the file render/camera.hpp.
 */

#include "render/camera.hpp"


// ================================================================================================
Camera::Camera(float dist) :
	m_theta{0},
	m_phi{0},
	m_dist{dist},
	m_aspect{1},
	m_dirty{true},
	m_view{},
	m_proj{}
{ }

// ================================================================================================
QMatrix4x4 Camera::getViewMatrix() const
{
	if (m_dirty)
		updateMatrix();

	return m_view;
}

// ================================================================================================
QMatrix4x4 Camera::getProjectionMatrix() const
{
	if (m_dirty)
		updateMatrix();

	return m_proj;
}

// ================================================================================================
QMatrix4x4 Camera::getVPMatrix() const
{
	if (m_dirty)
		updateMatrix();

	return m_proj * m_view;
}

// ================================================================================================
void Camera::updateMatrix() const
{
	m_proj.setToIdentity();
	m_proj.perspective(45, m_aspect, 0.001f, 1000000.0f);

	m_view.setToIdentity();
	QVector3D eyepos(
		m_dist * sin(M_PI / 2 - m_phi) * cos(m_theta),
		m_dist * cos(M_PI / 2 - m_phi),
		m_dist * sin(M_PI / 2 - m_phi) * sin(m_theta)
	);
	m_view.lookAt(eyepos, QVector3D(0, 0, 0), QVector3D(0, 1, 0));

	m_dirty = false;
}