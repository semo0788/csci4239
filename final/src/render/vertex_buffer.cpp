/**
 * Sean Moss (semo0788@colorado.edu)
 * Part of the final project for CSCI 4239, CU Boulder, Spring 2017.
 *
 * This file implements the code defined in the render/vertex_buffer.hpp file.
 */

#include "render/vertex_buffer.hpp"


// ================================================================================================
VertexBuffer::VertexBuffer(size_t size, GLenum usage) :
	m_vao{0},
	m_vbo{0},
	m_isMapped{false},
	m_size{size},
	m_usage{usage}
{
	glGenVertexArrays(1, &m_vao);
	if (!m_vao)
		QtFatal("Could not generate a Vertex Array Object");
	
	glGenBuffers(1, &m_vbo);
	if (!m_vbo)
		QtFatal("Could not generate a Vertex Buffer Object");

	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferData(GL_ARRAY_BUFFER, size, nullptr, usage);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

// ================================================================================================
VertexBuffer::~VertexBuffer()
{
	if (m_isMapped)
		unmapBuffer();

	if (m_vao)
		glDeleteVertexArrays(1, &m_vao);
	if (m_vbo)
		glDeleteBuffers(1, &m_vbo);
}

// ================================================================================================
void VertexBuffer::setFormat(const vertex_format_specifier_t *fmt, size_t count)
{
	if (m_isMapped)
		QtFatal("Cannot set the format of a buffer that it mapped to host memory.");
	
	glBindVertexArray(m_vao);
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);

	for (size_t i = 0; i < count; ++i) {
		const vertex_format_specifier_t& cfmt = fmt[i];
		glEnableVertexAttribArray(cfmt.location);
		glVertexAttribPointer(cfmt.location, cfmt.size, cfmt.type, GL_FALSE, cfmt.stride, 
				(GLvoid*)cfmt.offset);
	}

	glBindVertexArray(0);
}

// ================================================================================================
void VertexBuffer::setData(const void * const data)
{
	if (m_isMapped)
		QtFatal("Cannot set the contents of a buffer that is mapped to host memory.");
	
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferSubData(GL_ARRAY_BUFFER, 0, m_size, data);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

// ================================================================================================
void* VertexBuffer::mapBuffer(GLenum flag)
{
	if (m_isMapped)
		QtFatal("Attempting to map a buffer that is already mapped.");

	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	void* mapped = glMapBuffer(GL_ARRAY_BUFFER, flag);
	if (!mapped)
		QtFatal("Could not map vertex buffer.");
	m_isMapped = true;
	return mapped;
}

// ================================================================================================
void* VertexBuffer::mapBufferRange(GLenum flag, size_t offset, size_t length)
{
	if (m_isMapped)
		QtFatal("Attempting to map a buffer that is already mapped.");

	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	void* mapped = glMapBufferRange(GL_ARRAY_BUFFER, (GLintptr)offset, (GLsizeiptr)length, flag);
	if (!mapped)
		QtFatal("Could not map vertex buffer.");
	m_isMapped = true;
	return mapped;
}

// ================================================================================================
void VertexBuffer::unmapBuffer()
{
	if (!m_isMapped)
		QtFatal("Attempting to unmap a buffer that is not mapped.");

	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	if (glUnmapBuffer(GL_ARRAY_BUFFER) == GL_FALSE)
		QtFatal("Could not unmap buffer.");
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	m_isMapped = false;
}

// ================================================================================================
void VertexBuffer::drawBuffer(GLenum primitiveType, size_t start, size_t count)
{
	if (m_isMapped)
		QtFatal("Cannot draw a buffer that is mapped to host memory.");
	
	glBindVertexArray(m_vao);
	glDrawArrays(primitiveType, start, count);
	glBindVertexArray(0);
}