/**
 * Sean Moss (semo0788@colorado.edu)
 * Part of the final project for CSCI 4239, CU Boulder, Spring 2017.
 *
 * This file implements the code in the file render/ogl_utils.hpp.
 */

#include "render/ogl_utils.hpp"


// ================================================================================================
/* static */ void OGLUtils::InitializeGL()
{
	glewExperimental = GL_TRUE;
	GLenum err;
	if ((err = glewInit()) != GLEW_OK) {
		std::string errstr = strfmt("Could not initialize GLEW, error: \"%s\".", glewGetErrorString(err));
		QtFatal(errstr);
	}
}

// ================================================================================================
/* static */ QGLShaderProgram* OGLUtils::LoadShader(const QString& vert, const QString& frag)
{
	QGLShaderProgram* prog = new QGLShaderProgram;

	if (vert.length() && !prog->addShaderFromSourceFile(QGLShader::Vertex, vert))
		QtFatal("Error compiling " + vert.toStdString() + "\n" + prog->log().toStdString());
	if (frag.length() && !prog->addShaderFromSourceFile(QGLShader::Fragment, frag))
		QtFatal("Error compiling " + frag.toStdString() + "\n" + prog->log().toStdString());
	if (!prog->link())
		QtFatal("Error linking shader\n" + prog->log().toStdString());
	
	return prog;
}

// ================================================================================================
/* static */ QGLShaderProgram* OGLUtils::LoadGeomShader(const QString& vert, const QString& geom, const QString& frag)
{
	QGLShaderProgram* prog = new QGLShaderProgram;

	if (vert.length() && !prog->addShaderFromSourceFile(QGLShader::Vertex, vert))
		QtFatal("Error compiling " + vert.toStdString() + "\n" + prog->log().toStdString());
	if (geom.length() && !prog->addShaderFromSourceFile(QGLShader::Geometry, geom))
		QtFatal("Error compiling " + geom.toStdString() + "\n" + prog->log().toStdString());
	if (frag.length() && !prog->addShaderFromSourceFile(QGLShader::Fragment, frag))
		QtFatal("Error compiling " + frag.toStdString() + "\n" + prog->log().toStdString());
	if (!prog->link())
		QtFatal("Error linking shader\n" + prog->log().toStdString());
	
	return prog;
}

// ================================================================================================
/* static */ unsigned int OGLUtils::LoadTextureFromFile(const QString& path)
{
	QImage img(path);
	return LoadTextureFromImage(img);
}

// ================================================================================================
/* static */ unsigned int OGLUtils::LoadTextureFromImage(const QImage& image)
{
	unsigned int tex;
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);

	QImage rgba = QGLWidget::convertToGLFormat(image);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, rgba.width(), rgba.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, 
			rgba.bits());

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	return tex;
}