# Final Project for Sean Moss (CSCI 4239 Spring 2017)

To run the program, first run `qmake`, then `make`, then the executable is called `final`. No further steps necessary.

The interface has many controls to it, which are:
* Size Control - This controls the size of the simulation, choose among the 4 available
* Integrator Control - Allows you to chose how the integrator is run: Direct Sequential, OpenMP, or OpenCL
* Distribution Control - Was going to allow different particle distributions, never fully implmented.
* Render Control - This controls what is rendered in the simulation, as well as how the particles are color coded.
* Speed Control - This controls the timestep size, and allows pausing of the simulation.

Furthermore, the bottom right of the interface shows information:
* FPS - The number of rendered frames per second.
* UPS - The number of updates per second, or how many timesteps the integrator goes through per second.
* Distance and Angles - Information about the camera placement.

Control of the camera is given by clicking and dragging to change angles, and using the scroll wheel to change zoom.

Because the updating of the simulation and the rendering take place on different threads, the simulation can be freely
and smoothly looked around and manipulated even when it is paused, or running very slowly.

Graphics Related Notes (things implemented relating to this class):
* Use of Vertex Buffer Objects to provide vertex data to the GPU.
* Sharing of Vertex Buffers between OpenGL and OpenCL, which eliminates the need for expensive copying between device and
	host, everything just lives on the GPU until needed again. (I am particularly proud of this, and it took me a long time
	to get right).
* Use of Geometry Shaders to add the Velocity and Acceleration vectors on the particles in the GPU.
* Fragment shaders that can color code the particles based on different parameters, all calculated on the GPU.
* Implementation of the leapfrog n-body integrator in an OpenCL kernel, in a way that provides a ~60x speedup on my system.

Non-Graphics Notes (non-graphics things I am proud of, if that matters...):
* Bug-free (as far as I can tell) synchronization of rendering and physics threads to prevent data races.
* Nice looking (in my opinion at least) GUI with good feedback on the simulation state.

This project includes the source of the TinyThread++ library, found [here](http://tinythreadpp.bitsnbites.eu/).