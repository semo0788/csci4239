#version 330 core

in vec4 gfColor;

out vec4 outColor;

void main()
{
	outColor = gfColor;
}