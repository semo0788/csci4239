#version 330 core

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec4 inColor;

uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;

out vec4 vpColor;

void main()
{
	gl_Position = ProjectionMatrix * ViewMatrix * vec4(inPos, 1.0f);
	vpColor = inColor;
}