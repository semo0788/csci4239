#version 330 core

layout(location = 0) in float inMass;
layout(location = 1) in vec3 inPos;
layout(location = 2) in vec3 inVel;
layout(location = 3) in vec3 inAcc;

out vec4 vgVel;
out vec4 vgAcc;

uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;
uniform int TotalParticles;
uniform float MeanVelocity;
uniform float MeanAcceleration;

void main()
{
	mat4 PVMAT = ProjectionMatrix * ViewMatrix;

	gl_Position = PVMAT * vec4(inPos, 1.0);

	float cmod = 0.25 / length(inVel);
	float amod = 0.25 / length(inAcc);

	vec3 vel = inVel * cmod;
	vec3 acc = inAcc * amod;

	vgVel = PVMAT * vec4(inPos + vel, 1.0);
	vgAcc = PVMAT * vec4(inPos + acc, 1.0);
}