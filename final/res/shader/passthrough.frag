#version 330 core

in vec4 vpColor;

out vec4 outColor;

void main()
{
	outColor = vpColor;
}