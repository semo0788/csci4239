#version 330 core

#define M_PI 3.1415926535

in vec4 vfColor;
in float vfMass;
in vec3 vfPos;
in float vfVel;
in float vfAcc;
in float vfIndex;

out vec4 outColor;

uniform int ColorStyle;
uniform float MeanVelocity;
uniform float MeanAcceleration;
uniform int ParticleCount;

vec4 colorLookupTable(float val) 
{
	float x = clamp(val, 0, 1);
	float b = max(0, cos(x * M_PI * 3 / 4));
	float g = max(0, sin(x * M_PI));
	float r = max(0, sin((val - 0.333) * 3 * M_PI / 4));
	return vec4(r, g, b, 1.0);
}

void main()
{
	if (ColorStyle == 0) {
		outColor = vfColor;
	}
	else if (ColorStyle == 1) {
		outColor = colorLookupTable(vfMass);
	}
	else if (ColorStyle == 2) { // Position
		outColor = vec4(vfPos, 1.0);
	}
	else if (ColorStyle == 3) { // Velocity
		outColor = colorLookupTable(vfVel / MeanVelocity);
	}
	else if (ColorStyle == 4) { // Acceleration
		outColor = colorLookupTable(vfAcc / MeanAcceleration);
	}
	else if (ColorStyle == 5) { // Index
		outColor = colorLookupTable(vfIndex / ParticleCount);
	}
}