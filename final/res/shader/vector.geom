#version 330 core

layout(points) in;
layout(line_strip, max_vertices = 5) out;

in vec4 vgVel[];
in vec4 vgAcc[];

out vec4 gfColor;

uniform vec4 VelocityColor = vec4(0.0, 1.0, 0.0, 1.0);
uniform vec4 AccelerationColor = vec4(0.0, 0.0, 1.0, 1.0);

void main()
{
	vec4 PLOC = gl_in[0].gl_Position;
	vec4 VLOC = vgVel[0];
	vec4 ALOC = vgAcc[0];

	// Base of velocity vector
	gfColor = VelocityColor;
	gl_Position = PLOC;
	EmitVertex();

	// Tip of velocity vector
	gfColor = VelocityColor;
	gl_Position = VLOC;
	EmitVertex();

	// Base of velocity vector
	gfColor = VelocityColor;
	gl_Position = PLOC;
	EmitVertex();

	// Base of acceleration vector
	gfColor = AccelerationColor;
	gl_Position = PLOC;
	EmitVertex();

	// Tip of acceleration vector
	gfColor = AccelerationColor;
	gl_Position = ALOC;
	EmitVertex();

	EndPrimitive();
}