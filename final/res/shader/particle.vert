#version 330 core

layout(location = 0) in float inMass;
layout(location = 1) in vec3 inPos;
layout(location = 2) in vec3 inVel;
layout(location = 3) in vec3 inAcc;

out vec4 vfColor;
out float vfMass;
out vec3 vfPos;
out float vfVel;
out float vfAcc;
out float vfIndex;

uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;
uniform int ColorStyle;

void main()
{
	gl_Position = ProjectionMatrix * ViewMatrix * vec4(inPos, 1.0);
	
	if (ColorStyle == 0) { // None
		vfColor = vec4(1.0, 0.0, 0.0, 1.0);
	}
	else if (ColorStyle == 1) { // Mass
		vfMass = inMass;
	}
	else if (ColorStyle == 2) { // Position
		vfPos = inPos;
	}
	else if (ColorStyle == 3) { // Velocity
		vfVel = length(inVel);
	}
	else if (ColorStyle == 4) { // Acceleration
		vfAcc = length(inAcc);
	}
	else if (ColorStyle == 5) { // Index
		vfIndex = gl_VertexID;
	}
}