/**
 * Sean Moss (semo0788@colorado.edu)
 * Part of the final project for CSCI 4239, CU Boulder, Spring 2017.
 *
 * This file defines the PrimaryMenu class, which provides the MenuBar used in PrimaryWindow.
 */

#pragma once

#include "config.hpp"
#include <QMenuBar>


class PrimaryMenu :
	public QMenuBar
{
	Q_OBJECT

public:
	PrimaryMenu();
	~PrimaryMenu();
};