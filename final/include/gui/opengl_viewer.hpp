/**
 * Sean Moss (semo0788@colorado.edu)
 * Part of the final project for CSCI 4239, CU Boulder, Spring 2017.
 *
 * This file defines the OpenGLViewer class, which provides the primary OpenGL widget to render the
 *     application into. There is only ever one created, and it is the central widget for the
 *     PrimaryWindow class.
 */

#pragma once

#include "config.hpp"
#include "render/camera.hpp"
#include "sim/sim_manager.hpp"
#include <QApplication>
#include <QGLWidget>
#include <QTimer>
#include <QElapsedTimer>
#include <QGLShaderProgram>


class OpenGLViewer :
	public QGLWidget
{
	Q_OBJECT

private:
	QTimer m_frameTimer;
	QTimer m_updateTimer;
	QElapsedTimer m_elapsedTimer;
	QElapsedTimer m_fpsTimer;

	SimManager *m_simulation;

	Camera m_camera;
	bool m_mouse;
	QPoint m_mousePos;

public:
	OpenGLViewer();
	~OpenGLViewer();

	SimManager* getSimManager() const { return m_simulation; }

	void initializeGL() override;
	void resizeGL(int, int) override;
	void paintGL() override;

protected:
	void mousePressEvent(QMouseEvent *event) override;
	void mouseReleaseEvent(QMouseEvent *event) override;
	void mouseMoveEvent(QMouseEvent *event) override;
	void wheelEvent(QWheelEvent *event) override;

signals:
	void updateAngles(float, float);
	void updateFPS(float);
	void updateDistance(float);

private slots:
	void doFrame();
	void doUpdate();
};