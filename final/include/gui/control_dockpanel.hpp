/**
 * Sean Moss (semo0788@colorado.edu)
 * Part of the final project for CSCI 4239, CU Boulder, Spring 2017.
 *
 * This file defines the ControlDockPanel class, which appears on the left side of the main window,
 *     and controls settings for the simulation.
 */

#pragma once

#include "config.hpp"
#include "sim/sim_manager.hpp"
#include <QDockWidget>
#include <QComboBox>
#include <QLabel>
#include <QPushButton>
#include <QSlider>
#include <QCheckBox>


class ControlDockPanel :
	public QDockWidget
{
	Q_OBJECT

private:
	SimManager *m_simManager;
	
	// Simulation Size Selection
	QComboBox *m_simSizeSelector;
	QLabel    *m_simSizeLabel;

	// Integrator Type Selection
	QComboBox *m_integTypeSelector;
	QComboBox *m_integLocSelector;
	QLabel    *m_integTypeLabel;
	QLabel    *m_integLocLabel;

	// Distribution Control
	QComboBox *m_distTypeControl;
	QPushButton *m_distRegenerateButton;

	// Render control
	QCheckBox *m_axesShowControl;
	QCheckBox *m_vectorShowControl;
	QComboBox *m_colorControlSelector;

	// Speed Control
	QSlider *m_speedSelectorSlider;
	QLabel *m_speedLabel;
	QPushButton *m_pausePlayButton;

public:
	ControlDockPanel(SimManager *sim);

private:
	QWidget* prepareSimulationSizeSelector();
	QWidget* prepareIntegratorTypeSelector();
	QWidget* prepareDistributionControl();
	QWidget* prepareRenderControl();
	QWidget* prepareSpeedControl();

public slots:
	void setSimSize(int size);
	void setIntegType(int type);
	void setIntegLoc(int loc);
	void setPaused();
	void setSpeedValue(int speed);
	void setShowAxes(bool checked);
	void setShowVectors(bool checked);
	void setColorStyle(int style);
	void setDistType(int type);
	void regenerateDist();
};