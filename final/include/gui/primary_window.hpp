/**
 * Sean Moss (semo0788@colorado.edu)
 * Part of the final project for CSCI 4239, CU Boulder, Spring 2017.
 *
 * This file defines the PrimaryWindow class, which is the base viewer widget for the application.
 */

#pragma once

#include "config.hpp"
#include "gui/opengl_viewer.hpp"
#include "gui/primary_menu.hpp"
#include "gui/control_dockpanel.hpp"
#include <QApplication>
#include <QMainWindow>
#include <QStatusBar>
#include <QLabel>
#include <QElapsedTimer>


class PrimaryWindow :
	public QMainWindow
{
	Q_OBJECT

private:
	// Primary widgets
	OpenGLViewer *m_oglView;
	PrimaryMenu *m_topMenu;
	QStatusBar *m_statusBar;
	ControlDockPanel *m_controlPanel;

	// Status Bar widgets
	QLabel *m_sbFpsLabel;
	QLabel *m_sbUpsLabel;
	QLabel *m_sbAngleLabel;
	QLabel *m_sbDistLabel;

	// Extra
	QElapsedTimer m_upsTimer;

public:
	PrimaryWindow();
	~PrimaryWindow();

public slots:
	void setAngles(float nt, float np);
	void setDistance(float dist);
	void setFPS(float fps);
	void setUPS(float ups);
};