/**
 * Sean Moss (semo0788@colorado.edu)
 * Part of the final project for CSCI 4239, CU Boulder, Spring 2017.
 *
 * This file defines various nbody solver functions.
 */

#pragma once

#include "config.hpp"
#include "sim/particle.hpp"
#include "util/tinythread.h"
#include "util/fast_mutex.h"
#include <CL/opencl.h>


struct base_nbody_solver
{
public:
	const size_t ParticleCount;
	void * const SourceArray;
	void * const DestinationArray;
	const float DeltaTime;

private:
	bool m_complete;
	tthread::fast_mutex m_completeMutex;

public:
	base_nbody_solver(size_t count, void * const source, void * const dest, const float dt) :
		ParticleCount{count},
		SourceArray{source},
		DestinationArray{dest},
		DeltaTime{dt},
		m_complete{false},
		m_completeMutex{}
	{ }
	virtual ~base_nbody_solver() { }

	virtual void prepare() { }
	virtual void postSolve() { }
	virtual void solve() = 0;

	bool isDone();

protected:
	void toggleComplete(); // Should be called by solve() when the timestep is complete.
};


struct cpu_leapfrog_solver :
	public base_nbody_solver
{
public:
	cpu_leapfrog_solver(size_t count, void * const source, void * const dest, const float dt) :
		base_nbody_solver(count, source, dest, dt)
	{ }

	void solve() override;
};


struct openmp_leapfrog_solver :
	public base_nbody_solver
{
public:
	openmp_leapfrog_solver(size_t count, void * const source, void * const dest, const float dt) :
		base_nbody_solver(count, source, dest, dt)
	{ }

	void solve() override;
};


struct opencl_leapfrog_solver :
	public base_nbody_solver
{
private:
	cl_mem m_source;
	cl_mem m_dest;

public:
	opencl_leapfrog_solver(size_t count, cl_mem source, cl_mem dest, const float dt) :
		base_nbody_solver(count, nullptr, nullptr, dt),
		m_source{source},
		m_dest{dest}
	{ }

	void solve() override;
};