/**
 * Sean Moss (semo0788@colorado.edu)
 * Part of the final project for CSCI 4239, CU Boulder, Spring 2017.
 *
 * This file defines the ParticleMemoryManager class, which allocates and manages the Particle
 *     arrays used by the cpu and cuda.
 */

#pragma once

#include "config.hpp"
#include "sim/particle.hpp"
#include "render/vertex_buffer.hpp"
#include <CL/opencl.h>


class ParticleMemoryManager
{
public:
	static const unsigned int SIM_SIZE_SMALL = 0;
	static const unsigned int SIM_SIZE_MEDIUM = 1;
	static const unsigned int SIM_SIZE_LARGE = 2;
	static const unsigned int SIM_SIZE_HUGE = 3;

	static const size_t SIM_PARTICLE_COUNTS[4];

	static size_t OPENCL_MAX_WORK_GROUP_SIZE;

	static cl_program OPENCL_LEAPFROG_PROGRAM;
	static cl_kernel OPENCL_LEAPFROG_KERNEL;

	static cl_device_id GetCLDeviceId() { return s_opencl.deviceId; }
	static cl_context GetCLContext() { return s_opencl.context; }
	static cl_command_queue GetCLQueue() { return s_opencl.queue; }

private:
	Particle* m_hostParticleLists[4];		 // The primary particle array on host
	Particle* m_hostParticleLists2[4];		 // The secondary particle array on host
	VertexBuffer* m_deviceParticleLists[4];  // The primary particle array on device
	VertexBuffer* m_deviceParticleLists2[4]; // The secondary particle array on device
	bool m_swapped[4];				   // If true, then list2 -> list1, opposite if false
	cl_mem m_clDeviceMemory[4];  // The OpenCL links to the OpenGL vertex buffer
	cl_mem m_clDeviceMemory2[4]; // Same as above but for Lists2

	static struct OpenCLDeviceInfo
	{
		cl_device_id deviceId;
		cl_context context;
		cl_command_queue queue;
	} s_opencl;

public:
	ParticleMemoryManager();
	~ParticleMemoryManager();

	NBDY_DECLARE_CLASS_NONCOPYABLE(ParticleMemoryManager)

	Particle* getHostSourceForSize(size_t size);
	Particle* getHostDestinationForSize(size_t size);
	VertexBuffer* getSourceBufferForSize(size_t size);
	VertexBuffer* getDestinationBufferForSize(size_t size);
	cl_mem getCLSourceForSize(size_t size);
	cl_mem getCLDestinationForSize(size_t size);
	void acquireObjectsForSize(size_t size);
	void releaseObjectsForSize(size_t size);
	void swap(size_t size) { m_swapped[size] = !m_swapped[size]; }

	void triggerHostToDeviceCopy(size_t size); // Copies the host source array to device source array
	void triggerDeviceToHostCopy(size_t size); // Copies the device source array to host source array

	void generateParticleArrayForSize(size_t size, int type);

	static void InitializeGraphicsCompute();

private:
	void initializeListsForSize(size_t size);
};