/**
 * Sean Moss (semo0788@colorado.edu)
 * Part of the final project for CSCI 4239, CU Boulder, Spring 2017.
 *
 * This file defines the Particle class, which contains all of the information needed to run nbody
 *     simulations on a group of particles.
 */

#pragma once

#include "config.hpp"
#include "render/vertex_buffer.hpp"
#include "util/math.hpp"


#pragma pack(push, 1)
struct Particle
{
public:
	float m;
	union
	{
		struct
		{
			float x;
			float y;
			float z;
		};
		vec3 pos;
	};
	union
	{
		struct
		{
			float vx;
			float vy;
			float vz;
		};
		vec3 vel;
	};
	union
	{
		struct
		{
			float ax;
			float ay;
			float az;
		};
		vec3 acc;
	};

public:
	Particle() :
		m{0}, pos{}, vel{}, acc{}
	{ }
};
#pragma pack(pop)


// The OpenGL VBO attribute format specifier
extern const size_t ParticleFormatSpecifierLength;
extern const vertex_format_specifier_t ParticleFormatSpecifiers[4];