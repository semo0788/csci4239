/**
 * Sean Moss (semo0788@colorado.edu)
 * Part of the final project for CSCI 4239, CU Boulder, Spring 2017.
 *
 * This file defines the SimManager class, which manages the rendering of particles, as well as
 *     dispactching the update functions properly, and coordinating the data sharing between the
 *     simulation and rendering systems.
 */

#pragma once

#include "config.hpp"
#include "render/vertex_buffer.hpp"
#include "render/camera.hpp"
#include "sim/particle.hpp"
#include "sim/memory_manager.hpp"
#include "sim/nbody_solver.hpp"
#include <QObject>
#include <QElapsedTimer>
#include <QGLShaderProgram>


// Object for signaling to the simulation manager that a change in simulation size is needed,
//     this is done to prevent client side changes while the solver thread is still running.
struct sim_size_change_queue_t
{
public:
	const size_t NewSize;
};

// Object for signaling to the simulation manager that a change in integrator type or style
//     is needed.
struct integ_change_queue_t
{
public:
	const int NewType;
	const int NewStyle;
};

// Object for queueing a distribution regeneration
struct dist_regen_queue_t
{
public:
	int NewType;
	bool DoRegen;
};


class SimManager :
	public QObject
{
	Q_OBJECT

private:
	QElapsedTimer m_upsTimer;
	QElapsedTimer m_meanValueTimer;

	VertexBuffer *m_axesBuffer;
	QGLShaderProgram *m_axesShader;
	QGLShaderProgram *m_particleShader;
	QGLShaderProgram *m_vectorShader;

	ParticleMemoryManager *m_particleManager;
	size_t m_currentSimSize;
	size_t m_currentParticleCount;
	sim_size_change_queue_t *m_queuedSimSizeChange;
	int m_currentIntegType;
	int m_currentIntegStyle;
	integ_change_queue_t *m_queuedIntegChange;
	dist_regen_queue_t *m_queuedRegen;
	int m_colorStyle;
	int m_distType;

	bool m_paused;
	float m_timestep;
	bool m_showAxes;
	bool m_showVectors;

	float m_meanVelocity;
	float m_meanAcceleration;

	base_nbody_solver *m_currentSolver;
	tthread::thread *m_currentThread;

public:
	SimManager();
	~SimManager();

	void initializeGL();

	void setSimulationSize(size_t size);
	void setIntegratorType(int type);
	void setIntegratorLocation(int loc);

	void update();
	void render(const Camera& camera);

	inline void setPaused(bool paused) { m_paused = paused; }
	inline void setTimestep(int timestep) 
			{ m_timestep = ((timestep == 0) ? 1e-2 : (timestep == 1) ? 1e-3 : 1e-4); }
	inline void setShowAxes(bool axes) { m_showAxes = axes; }
	inline void setShowVectors(bool vecs) { m_showVectors = vecs; }
	inline void setColorStyle(int style) { m_colorStyle = style; }

	void setDistType(int type);
	void regenDist();

private:
	void prepareRender();
	void postRender();

	void updateMeanValues(Particle* parts);
	
	base_nbody_solver* makeCurrentSolver();
	void launchTimestep();

signals:
	void updateUPS(float);
};