/**
 * Sean Moss (semo0788@colorado.edu)
 * Part of the final project for CSCI 4239, CU Boulder, Spring 2017.
 *
 * This file includes all relavent cuda headers, as well as some helper functions.
 */

#pragma once

#include "config.hpp"
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <cuda_gl_interop.h>

#define CUDA_CHECK(stmt) do { cudaCheckError((stmt), __FILE__, __LINE__); } while (false)
extern void cudaCheckError(cudaError_t code, const char* file, int line, bool abort = true);