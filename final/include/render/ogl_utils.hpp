/**
 * Sean Moss (semo0788@colorado.edu)
 * Part of the final project for CSCI 4239, CU Boulder, Spring 2017.
 *
 * This file defines the OGLUtils class, which provides some OpenGL object creation functions.
 */

#pragma once

#include "config.hpp"
#include "GL/glew.h"
#include <QGLShaderProgram>


class OGLUtils
{
private:
	OGLUtils() { }

public:
	static void InitializeGL();

	static QGLShaderProgram* LoadShader(const QString& vert, const QString& frag);
	static QGLShaderProgram* LoadGeomShader(const QString& vert, const QString& geom, const QString& frag);
	static unsigned int LoadTextureFromFile(const QString& path);
	static unsigned int LoadTextureFromImage(const QImage& image);
};