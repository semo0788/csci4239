/**
 * Sean Moss (semo0788@colorado.edu)
 * Part of the final project for CSCI 4239, CU Boulder, Spring 2017.
 *
 * This file defines the Camera class, which generates view and projection matricies, and is
 *     designed to work like an actual camera in 3-space.
 */

#pragma once

#include "config.hpp"
#include <QMatrix4x4>


class Camera
{
private:
	float m_theta;
	float m_phi;
	float m_dist;
	float m_aspect;
	mutable bool m_dirty;
	mutable QMatrix4x4 m_view;
	mutable QMatrix4x4 m_proj;

public:
	Camera(float dist = 1.0f);
	~Camera() { }

	inline float getTheta() const { return m_theta; }
	inline float getPhi() const { return m_phi; }
	inline float getDistance() const { return m_dist; }
	inline float getAspect() const { return m_aspect; }

	inline float setTheta(float theta) 
			{ float tmp = m_theta; m_theta = theta; m_dirty = true; return tmp; }
	inline float setPhi(float phi) 
			{ float tmp = m_phi; m_phi = phi; m_dirty = true; return tmp; }
	inline float setDistance(float dist) 
			{ float tmp = m_dist; m_dist = dist; m_dirty = true; return tmp; }
	inline float setAspect(float asp)
			{ float tmp = m_aspect; m_aspect = asp; m_dirty = true; return tmp; }

	QMatrix4x4 getViewMatrix() const;
	QMatrix4x4 getProjectionMatrix() const;
	QMatrix4x4 getVPMatrix() const;

private:
	void updateMatrix() const;
};