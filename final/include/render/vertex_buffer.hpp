/**
 * Sean Moss (semo0788@colorado.edu)
 * Part of the final project for CSCI 4239, CU Boulder, Spring 2017.
 *
 * This file defines the VertexBuffer class, which is a wrapper around an OpenGL VAO and VBO,
 *     and the data stored in the vertex buffer.
 */

#pragma once

#include "config.hpp"
#include "render/vertex_format.hpp"
#include "GL/glew.h"


class VertexBuffer
{
private:
	unsigned int m_vao;
	unsigned int m_vbo;
	bool m_isMapped;
	const size_t m_size;
	const GLenum m_usage;

public:
	VertexBuffer(size_t size, GLenum usage);
	~VertexBuffer();

	inline unsigned int getVboName() const { return m_vbo; }
	inline size_t getSize() const { return m_size; }
	inline GLenum getUsage() const { return m_usage; }

	void setFormat(const vertex_format_specifier_t *fmt, size_t count);
	void setData(const void * const data);

	void* mapBuffer(GLenum flag);
	void* mapBufferRange(GLenum flag, size_t offset, size_t length);
	void unmapBuffer();
	inline bool isMapped() const { return m_isMapped; }

	void drawBuffer(GLenum primitiveType, size_t start, size_t count);
};