/**
 * Sean Moss (semo0788@colorado.edu)
 * Part of the final project for CSCI 4239, CU Boulder, Spring 2017.
 *
 * This file defines the vertex_format_specifier_t struct, to specify vertex formats for
 *    VertexBuffers.
 */

#pragma once

#include "config.hpp"


struct vertex_format_specifier_t
{
	size_t location;
	size_t size;
	unsigned int type;
	size_t stride;
	size_t offset;
};