/**
 * Sean Moss (semo0788@colorado.edu)
 * Part of the final project for CSCI 4239, CU Boulder, Spring 2017.
 *
 * This file provides common definitions, includes, and functions shared by the entire program.
 */

#pragma once

// Include some standard stl headers
#include <cfloat>
#include <cmath>
#include <cstdarg>
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <exception>
#include <fstream>
#include <functional>
#include <initializer_list>
#include <iostream>
#include <memory>
#include <random>
#include <sstream>
#include <string>

// Include some standard stl containers
#include <array>
#include <deque>
#include <list>
#include <queue>
#include <stack>
#include <unordered_map>
#include <vector>

// Expose the C++ string
using String = std::string;
using StringStream = std::stringstream;

// Expose the standard size stl types
using uint8 = std::uint8_t;
using uint16 = std::uint16_t;
using uint32 = std::uint32_t;
using uint64 = std::uint64_t;
using int8 = std::int8_t;
using int16 = std::int16_t;
using int32 = std::int32_t;
using int64 = std::int64_t;

// Expose the stl container classes
template <
	class T,
	std::size_t N
> using StlArray = std::array<T, N>;
template <
	class T,
	class Allocator = std::allocator<T>
> using StlDeque = std::deque<T, Allocator>;
template <
	class T,
	class Allocator = std::allocator<T>
> using StlList = std::list<T, Allocator>;
template <
	class T,
	class Container = std::deque<T>
> using StlQueue = std::queue<T, Container>;
template <
	class T,
	class Container = std::deque<T>
> using StlStack = std::stack<T, Container>;
template <
	class Key,
	class T,
	class Hash = std::hash<Key>,
	class KeyEqual = std::equal_to<Key>,
	class Allocator = std::allocator<std::pair<const Key, T>>
> using StlHashMap = std::unordered_map<Key, T, Hash, KeyEqual, Allocator>;
template <
	class T,
	class Allocator = std::allocator<T>
> using StlVector = std::vector<T, Allocator>;
template <
	class T
> using StlInitializerList = std::initializer_list<T>;

// Expose the stl smart pointer classes
template <
	class T,
	class Deleter = std::default_delete<T>
> using StlUniquePtr = std::unique_ptr<T, Deleter>;
template <
	class T
> using StlSharedPtr = std::shared_ptr<T>;
template <
	class T
> using StlWeakPtr = std::weak_ptr<T>;

// Macro definition for declaring classes as non-copyable and non-moveable
#define NBDY_DECLARE_CLASS_NONCOPYABLE(className) \
	public: \
		className (const className&) = delete; \
		className& operator = (const className&) = delete;

// Define the internal logging functionality
String strfmt(const String& fmt, ...);
void linfo(const String& msg);
void lwarn(const String& msg);
void lerr(const String& msg);
void lfatal(const String& msg);
void lsetPrefix(const String& pre);

void QtFatal(const String& message);

// Stringification macros
#define xstrify(a) __strify(a)
#define __strify(a) #a