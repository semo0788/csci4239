/**
 * Sean Moss (semo0788@colorado.edu)
 * Part of the final project for CSCI 4239, CU Boulder, Spring 2017.
 *
 * This file defines some lightweight math types.
 */

#pragma once

#include "config.hpp"


#pragma pack(push, 1)
struct vec3
{
public:
	float x;
	float y;
	float z;

public:
#ifdef __CUDACC__
	__host__ __device__
#endif // __CUDACC__
	vec3() :
		x{0}, y{0}, z{0}
	{ }
#ifdef __CUDACC__
	__host__ __device__
#endif // __CUDACC__
	vec3(float f) :
		x{f}, y{f}, z{f}
	{ }
#ifdef __CUDACC__
	__host__ __device__
#endif // __CUDACC__
	vec3(float _x, float _y, float _z) :
		x{_x}, y{_y}, z{_z}
	{ }

#ifdef __CUDACC__
	__host__ __device__
#endif // __CUDACC__
	inline vec3& operator += (const vec3& v) { x += v.x; y += v.y; z += v.z; return *this; }

#ifdef __CUDACC__
	__host__ __device__
#endif // __CUDACC__
	inline vec3& operator -= (const vec3& v) { x -= v.x; y -= v.y; z -= v.z; return *this; }

#ifdef __CUDACC__
	__host__ __device__
#endif // __CUDACC__
	inline vec3& operator *= (float f) { x *= f; y *= f; z *= f; return *this; }

#ifdef __CUDACC__
	__host__ __device__
#endif // __CUDACC__
	inline vec3& operator /= (float f) { x /= f; y /= f; z /= f; return *this; }

#ifdef __CUDACC__
	__host__ __device__
#endif // __CUDACC__
	inline float dot(const vec3& v) { return x * v.x + y * v.y + z * v.z; }

#ifdef __CUDACC__
	__host__ __device__
#endif // __CUDACC__
	inline float length() { return sqrt(x * x + y * y + z * z); }
};
#pragma pack(pop)

#ifdef __CUDACC__
	__host__ __device__
#endif // __CUDACC__
inline vec3 operator + (const vec3& v1, const vec3& v2) { return vec3(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z); }

#ifdef __CUDACC__
	__host__ __device__
#endif // __CUDACC__
inline vec3 operator - (const vec3& v1, const vec3& v2) { return vec3(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z); }

#ifdef __CUDACC__
	__host__ __device__
#endif // __CUDACC__
inline vec3 operator * (const vec3& v1, float f) { return vec3(v1.x * f, v1.y * f, v1.z * f); }

#ifdef __CUDACC__
	__host__ __device__
#endif // __CUDACC__
inline vec3 operator * (float f, const vec3& v1) { return vec3(v1.x * f, v1.y * f, v1.z * f); }

#ifdef __CUDACC__
	__host__ __device__
#endif // __CUDACC__
inline vec3 operator / (const vec3& v1, float f) { return vec3(v1.x / f, v1.y / f, v1.z / f); }

#ifdef __CUDACC__
	__host__ __device__
#endif // __CUDACC__
inline vec3 operator / (float f, const vec3& v1) { return vec3(f / v1.x, f / v1.y, f / v1.z); }